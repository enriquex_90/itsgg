# -*- coding: utf-8 -*-
{
    'name': "Educational Matriculación",

    'summary': """
        Matriculación estudiantil""",

    'description': """
        Módulo que tienen como objetivo la matriculación de los estudiantes, en la cual se asignan materias, paralelos y profesores.
    """,

    'author': "Ing. Cristhian Carreño",
    'website': "http://www.webhosting.ml",

    'category': 'Education',
    'version': '1.0',

    'depends': ['base','educational_core'],

    'data': [
        # 'security/ir.model.access.csv',
        'views/view_matricula.xml',
        'views/view_enrolment.xml',
        'views/view_enrolment_teachers.xml',
        'views/view_menuitem.xml',
    ],
    # only loaded in demonstration mode
    'demo': [

    ],
}