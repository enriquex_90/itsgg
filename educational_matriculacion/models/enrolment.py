
# -*- coding: utf-8 -*-

import logging
from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class MateriasLine(models.Model):
    _name = 'educational.enrolment_materias'

    enrolment_id = fields.Many2one(
        'educational.enrolment',
        string='Proceso',
    )

    materias = fields.Many2one(
        'educational.materias',
        string='Materias',
        required=True,
        readonly=True
    )

    repeticiones = fields.Integer(string='Repeticiones', default=0)


class EnrolmentPeriod(models.Model):
    _name = 'educational.enrolment'

    def _set_default_proceso(self):
        proceso_id = self.env['educacional.proceso_matriculacion'].search([('activar', '=', True)], limit=1).id
        return proceso_id

    def _set_default_records_nivel(self):
        if self.alumno_id:
            nivel = self.env['res.partner'].browse(self.alumno_id).id.nivel
            if nivel == 0:
                nivel += 1
            self.nivel = nivel

    @api.onchange('proceso', 'alumno_id')
    def _onchange_fill_alumnos_id(self):
        res = {}
        if self.proceso:
            res['domain'] = {'alumno_id': [('carrera', '=', self.proceso.carreras.id)]}
            return res
        else:
            res['domain'] = {'alumno_id': [('id', '=', 0)]}
            return res

    proceso = fields.Many2one(
        'educacional.proceso_matriculacion',
        string='Proceso',
        domain=[('activar', '=', True)],
        required=True,
    )

    alumno_id = fields.Many2one(
        'res.partner',
        string='Alumno',
        required=True,
        domain=_onchange_fill_alumnos_id
    )

    nivel = fields.Integer(
        readonly=True,
        required=True,
        #compute=_set_default_records_nivel,
        store=False,
    )

    estatus_enrolment = fields.Selection([('A', 'Aprobado'),
                                        ('R', 'Reprobado'),
                                        ('S', 'Suspendido'),
                                        ('N', 'Ninguno')],
                                         default='A',
                                         readonly=True,
                                         required=True
                                         )

    materias_line = fields.One2many(
        'educational.enrolment_materias',
        'enrolment_id',
        string='Materias disponibles',
        #readonly=True,
        required=True
    )

    paralelos = fields.Many2one(
        'educational.matriculacion_line',
        string='Paralelos',
        required=True
    )

    @api.onchange('alumno_id')
    def onchange_alumno(self):
        if self.alumno_id and self.proceso:
            carrera_id = self.env['res.partner'].browse(self.alumno_id).id.carrera.id

            level = self.env['res.partner'].browse(self.alumno_id).id.nivel
            if level == 0:
                level = 1

            r = []

            materias = self.env['educational.materias'].search([
                ('niveles', '=', level),
                ('carrera', '=', carrera_id)]
            )

            for materia in materias:
                r.append({'materias': materia.id})

            return {'value': {'materias_line': r,
                              'nivel': level}
                    }