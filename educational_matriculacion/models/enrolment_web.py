
# -*- coding: utf-8 -*-

import logging
from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class MateriasLine(models.Model):
    _name = 'educational.enrolment_materias'

    '''
        Registro de Estudiantes
    '''

    alumno_id = fields.Many2one(
        'res.partner',
        string='Alumno',
    )

    materias = fields.Many2one(
        'educational.materias',
        string='Materias',
        required=True,
        readonly=True
    )

    repeticiones = fields.Integer(string='Repeticiones', default=0)


class EnrolmentPeriod(models.Model):
    _name = 'educational.enrolment'

    def _get_level(self):
        level = self.env['res.partner'].browse(self.env.user.partner_id).id.nivel
        if level == 0:
            level = 1
        return level

    def _get_carrer(self):
        return self.env['res.partner'].browse(self.env.user.partner_id).id.carrera.id

    def _set_domain_carrer(self):
        domains = [('carreras.id', '=', self.env['res.partner'].browse(self.env.user.partner_id).id.carrera.id),
                   ('activar', '=', True)]
        return domains

    def _set_matriculaLine_domains(self):
        nivel = self.env['res.partner'].browse(self.env.user.partner_id).id.nivel

        if nivel == 0:
            nivel = 1
        domains = [('nivel', '=', nivel),
                   ('jornada', '=', self.env.user.partner_id.jornada.id)]
        return domains

    def _set_default_proceso(self):
        proceso_id = self.env['educacional.proceso_matriculacion'].search([('activar', '=', True)], limit=1).id
        return proceso_id

    proceso = fields.Many2one(
        'educacional.proceso_matriculacion',
        string='Proceso',
        domain=_set_domain_carrer,
        default=_set_default_proceso,
        required=True,
        readonly=True
    )

    def _fill_materias(self):

        nivel = self.env['res.partner'].browse(self.env.user.partner_id).id.nivel

        if nivel == 0:
            nivel = 1

        materias = self.env['educational.materias'].search([
            ('niveles', '=', nivel),
            ('carrera', '=', self.env['res.partner'].browse(self.env.user.partner_id).id.carrera.id)]
        )

        return [(0, 0, {'alumno_id': self.env.user.partner_id.id,
                        'proceso_matriculacion': self.proceso.id, #FIXME Proceso de Matriculación no puede ser 1 debe de ser automático
                        'materias': materia.id}) for materia in materias]

    alumno_id = fields.Many2one(
        'res.partner',
        string='Alumno',
        default=lambda self: self.env.user.partner_id,
        #readonly=True,
        required=True,
        domain=[('is_alumno', '=', True)]
    )

    carrera = fields.Many2one(
        'educational.carreras',
        string='Carrera',
        default=_get_carrer,
        readonly=True,
        required=True
    )

    nivel = fields.Integer(default=_get_level, readonly=True, required=True)

    estatus_enrolment = fields.Selection([('A','Aprobado'),
                                        ('R','Reprobado'),
                                        ('S','Suspendido'),
                                        ('N','Ninguno')],
                                         default='A',
                                         readonly=True,
                                         required=True
                                         )

    materias_line = fields.One2many(
        'educational.enrolment_materias',
        'alumno_id',
        string='Materias disponibles',
        #default=_fill_materias,
        readonly=True,
        required=True
    )

    paralelos = fields.Many2one(
        'educational.matriculacion_line',
        string='Paralelos',
        required=True,
        domain=_set_matriculaLine_domains
    )

    @api.onchange('alumno_id')
    def onchange_alumno(self):
        int_nivel = int(self.env['res.partner'].browse(self.alumno_id).id.nivel)

        if int_nivel == 0:
            int_nivel += 1

            self.nivel = int_nivel
            self.carrera = self.env['res.partner'].browse(self.alumno_id).id.carrera.id

            self._fill_materias()
