# -*- coding: utf-8 -*-

import logging
from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class Periodos(models.Model):
    _name = 'educational.periodo'

    def name_get(self):
        data = []
        for rec in self:
            name = rec.name + ' | ' + rec.tiempo + ' | ' + rec.fecha_inicio + ' | ' + rec.fecha_fin
            data.append((rec.id, name))
        return data

    name = fields.Char(string='Nombre de periodo',  required=True)
    tiempo = fields.Selection([('S', 'SEMESTRAL')], default='S',  required=True)
    fecha_inicio = fields.Date(required=True)
    fecha_fin = fields.Date(required=True)
    estado = fields.Boolean(string='Estado', default=False)


class MatriculaLine(models.Model):
    _name = 'educational.matriculacion_line'

    def name_get(self):
        data = []
        for rec in self:
            name = str(rec.nivel) + ' | ' + rec.paralelo.name + ' | ' + rec.aulas.name + ' | ' + rec.jornada.name
            data.append((rec.id, name))
        return data

    def _set_default_proceso(self):
        proceso_id = self.env['educacional.proceso_matriculacion'].search([('activar', '=', True)], limit=1).id
        return proceso_id

    periodo_id = fields.Many2one(
        'educacional.proceso_matriculacion',
        string='Periodo',
        default=_set_default_proceso,
        readonly=True
    )
    nivel = fields.Integer(string='Nivel', required=True)
    paralelo = fields.Many2one('educational.paralelos', string='Paralelo', required=True)
    aulas = fields.Many2one('educational.aulas', string='Aulas', required=True)
    jornada = fields.Selection([('M', 'MATUTINA'), ('V', 'VESPERTINA'), ('N', 'NOCTURNA')], string='Jornada', required=True)


class ProcesoMatriculacion(models.Model):
    _name = 'educacional.proceso_matriculacion'

    def name_get(self):
        data = []
        for rec in self:
            name = 'PERIODO No.: ' + rec.periodo_id.name + ' | ' + rec.carreras.name
            data.append((rec.id, name))
        return data

    periodo_id = fields.Many2one('educational.periodo', string='Seleccionar periodo',  required=True, domain=[('estado', '=', True)])
    fecha_inicio = fields.Date('Inicio de periodo de matriculación',  required=True)
    fecha_fin = fields.Date('Finalización de periodo de matriculación',  required=True)
    carreras = fields.Many2one('educational.carreras', string='Seleccione las carrera', required=True,
                               help='Seleccione las carreras que entraran en el proceso de matriculación actual')
    activar = fields.Boolean('Estado')

    matriculacion_line = fields.One2many(
        'educational.matriculacion_line',
        'periodo_id',
        string='Matriculación'
        )