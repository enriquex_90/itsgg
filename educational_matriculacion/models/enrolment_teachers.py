
# -*- coding: utf-8 -*-

import logging
from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class EnrolmentTeachersLine(models.Model):
    _name = 'educational.enrolment_teacher_line'

    enrolment_teacher_id = fields.Many2one(
        'educacional.enrolment_teacher',
        required=True
    )

    materias_id = fields.Many2one(
        'educational.materias',
        string='Materias',
        required=True,
        readonly=True
    )

    profesor_id = fields.Many2one(
        'res.partner',
        string='Profesor',
        # required=True,
        domain=[('is_profesor', '=', True)]
    )


class EnrolmentTeachers(models.Model):
    _name = 'educational.enrolment_teacher'

    def _set_default_proceso(self):
        proceso_id = self.env['educacional.proceso_matriculacion'].search([('activar', '=', True)], limit=1).id
        return proceso_id

    proceso_id = fields.Many2one(
        'educacional.proceso_matriculacion',
        string='Proceso',
        default=_set_default_proceso,
        required=True
    )
    matriculacion_line = fields.Many2one(
        'educational.matriculacion_line',
        string='Paralelos',
        required=True,
        #domain=_set_matriculaLine_domains
    )

    carga_horaria = fields.Float(string='Carga horaria (hrs.)', default=0, readonly=True)

    @api.onchange('matriculacion_line')
    def _fill_materias(self):

        if self.proceso_id.carreras.id and self.matriculacion_line.nivel:
            materias = self.env['educational.materias'].search([('niveles', '=', self.matriculacion_line.nivel),
                                                                ('carrera', '=', self.proceso_id.carreras.id)])
            r = []

            for mat in materias:
                r.append({'materias_id': mat.id})

            return {'value': {'enrolment_teacher_line': r}}

    enrolment_teacher_line = fields.One2many(
        'educational.enrolment_teacher_line',
        'enrolment_teacher_id',
        string='Asignaciones',
        #default=_fill_materias,
        # readonly=True,
        # required=True
    )