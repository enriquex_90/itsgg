from odoo import models, fields


class Biometric(models.Model):
    _name = 'attendance.biometric'
    _description = 'Biometrico Info'

    name = fields.Char('Nombre')
    location_id = fields.Many2one('edu.location', 'Ubicacion')
