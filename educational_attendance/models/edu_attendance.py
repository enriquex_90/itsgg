from odoo import models, fields


class Attendance(models.Model):
    _name = 'edu.attendance'
    _description = 'Asistencia Docente'
    _rec_name = 'faculty_id'

    date = fields.Datetime('Hora')
    state = fields.Selection([('I', 'Ingreso'), ('S', 'Salida')], 'Estado')
    faculty_id = fields.Many2one('edu.faculty', string='Docente',
                                 required=True)

    biometric_id = fields.Many2one('attendance.biometric', 'Biometrico')
