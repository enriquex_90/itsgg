from odoo import fields, models


class EduFaculty(models.Model):
    _inherit = 'edu.faculty'

    finger_print_id = fields.Integer('Id Huella')
    finger_print = fields.Text('Huella')
