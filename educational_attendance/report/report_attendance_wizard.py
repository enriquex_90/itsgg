from odoo import models, api


class ReportEduAttendanceXlsx(models.AbstractModel):
    _name = 'report.educational_attendance.report_attendance_xlsx'
    _description = 'Reporte Asistencia Docente'
    _inherit = 'report.report_xlsx.abstract'

    @api.model
    def generate_xlsx_report(self, workbook, data, obj):
        sheet = workbook.add_worksheet()

        sheet.set_column(0, 3, 10)
        sheet.set_column(1, 3, 10)
        sheet.set_column(2, 3, 10)
        sheet.set_column(3, 3, 10)

        sheet.set_column(4, 3, 10)
        sheet.set_column(5, 3, 10)
        sheet.set_column(6, 3, 10)
        sheet.set_column(7, 3, 10)

        format_title = workbook.add_format({'font_size': 12, 'bottom': False, 'right': False, 'left': False,
                                            'top': False, 'align': 'center', 'bold': True, 'border': 1})
        format_structure = workbook.add_format({'font_size': 6, 'bottom': False, 'right': False, 'left': False,
                                                'top': False, 'align': 'left', 'bold': True, 'border': 1})
        format_structure_center = workbook.add_format({'font_size': 6, 'bottom': False, 'right': False, 'left': False,
                                                       'top': False, 'align': 'center', 'bold': True, 'border': 1})
        format_content = workbook.add_format({'font_size': 6, 'bottom': False, 'right': False, 'left': False,
                                              'top': False, 'align': 'left', 'bold': False, 'border': 1})
        format_date = workbook.add_format(
            {'num_format': 'dd/mm/yy hh:mm:ss', 'font_size': 6, 'bottom': False, 'right': False, 'left': False,
             'top': False, 'align': 'center', 'bold': False, 'border': 1})

        sheet.merge_range('A2:F2', u'%s' % 'Reporte de Asistencia Docente', format_title)
        sheet.merge_range('A3:C3', u'%s' % 'Desde: ' + str(obj.start_date), format_structure)
        sheet.merge_range('D3:F3', u'%s' % 'Hasta: ' + str(obj.end_date), format_structure)
        sheet.merge_range('A4:C4', u'%s' % 'Docente', format_structure)
        sheet.write(3, 3, 'Estado', format_structure_center)
        sheet.write(3, 4, 'Hora', format_structure)
        sheet.write(3, 5, 'Biometrico', format_structure_center)

        index = 4
        for result_faculty in obj.faculty_ids:
            domain_attendance = [('faculty_id.id', '=', result_faculty.id),
                                 ('date', '>=', obj.start_date), ('date', '<=', obj.end_date)]
            attendance = self.env['edu.attendance'].search(domain_attendance, order='date')
            for result_attendance in attendance:
                #     faculty_name = result_attendance.faculty_id.name
                #     if result_attendance.state == 'I':
                #         start_date = result_attendance.date
                #         start_biometric_name = result_attendance.biometric_id.name
                #     if result_attendance.state == 'S':
                #         end_date = result_attendance.date
                #         end_biometric_name = result_attendance.biometric_id.name

                sheet.merge_range(index, 0, index, 2, result_attendance.faculty_id.name, format_content)
                if result_attendance.state == 'I':
                    sheet.write(index, 3, 'Ingreso', format_content)
                if result_attendance.state == 'S':
                    sheet.write(index, 3, 'Salida', format_content)
                sheet.write(index, 4, result_attendance.date, format_date)
                sheet.write(index, 5, result_attendance.biometric_id.name, format_content)
                index += 1
