# -*- encoding: utf-8 -*-

from odoo import models, api, fields


class WizardAttendanceReport(models.TransientModel):
    _name = 'wizard.attendance.report'
    _description = 'Asistente para generar reportes  de Asistencia'

    start_date = fields.Date('Fecha de Inicio')
    end_date = fields.Date('Fecha de Fin')
    faculty_ids = fields.Many2many('edu.faculty', string='Docente')

    @api.multi
    def action_get_report_pdf(self):
        result = []
        for faculty in self.faculty_ids:
            result.append(faculty.id)
        data = {
            'ids': self.ids,
            'model': self._name,
            'form': {
                'start_date': self.start_date,
                'end_date': self.end_date,
                'faculty_ids': result,
            },
        }
        return self.env.ref('educational_attendance.reports_attendance_pdf').report_action(self, data)

    @api.multi
    def action_get_report_xls(self):
        return self.env.ref('educational_attendance.reports_attendance_xlsx').report_action(self)


class ReportEduAttendancePdf(models.AbstractModel):
    _name = 'report.educational_attendance.report_attendance_pdf'
    _description = 'Reporte de asistencias PDF'

    @api.model
    def _get_report_values(self, docids, data=None):
        start_date = data['form']['start_date']
        end_date = data['form']['end_date']
        docs = []

        for result in data['form']['faculty_ids']:
            domain_attendance = [('faculty_id', '=', int(result)),
                                 ('date', '>=', start_date), ('date', '<=', end_date)]
            attendance = self.env['edu.attendance'].search(domain_attendance, order='date')

            docs.append(attendance)
        return {
            'doc_ids': data['ids'],
            'doc_model': data['model'],
            'start_date': start_date,
            'end_date': end_date,
            'docs': docs,
        }
