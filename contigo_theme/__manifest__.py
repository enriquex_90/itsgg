# -*- coding: utf-8 -*-
# Copyright 2016, 2019 Openworx
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    "name": "Tema Contigo",
    "summary": "Tema contigo",
    "version": "12.0.0.2",
    "category": "Theme/Backend",
    "website": "https://www.manexware.com",
	"description": """
		Tema para ERP Contigo
    """,
	'images':[
        'images/screen.png'
	],
    "author": "Manexware",
    "license": "LGPL-3",
    "installable": True,
    "depends": [
        'web',
        'web_responsive',

    ],
    "data": [
        'views/assets.xml',
		# 'views/res_company_view.xml',
		'views/users.xml',
        'views/sidebar.xml',
		#'views/web.xml',
    ],

}

