# -*- coding: utf-8 -*-

import logging
from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class ParametrosCalificaciones(models.Model):
    _name = 'educational.calificaciones_line'

    calificaciones_id = fields.Many2one(
        'educational.calificaciones',
        string='Proceso',
    )
    #proceso_id = fields.Many2one('educacional.proceso_matriculacion', string='Periodo', required=True)
    #parcial = fields.Selection([('P', '1er'), ('S', '2do')], string='Parcial', required=True)
    alumno_id = fields.Many2one('res.partner', string='Alumno', required=True, readonly=True)
    asistencias = fields.Float(string="Asistencias")
    gestion_formativa = fields.Float(string="Gestión Formativa")
    gestion_practica = fields.Float(string="Gestión Practica")
    examen = fields.Float(string="Examen")


class Calificaciones(models.Model):
    _name = 'educational.calificaciones'

    def _set_default_proceso(self):
        proceso_id = self.env['educacional.proceso_matriculacion'].search([('activar', '=', True)], limit=1).id
        return proceso_id

    proceso_id = fields.Many2one(
        'educacional.proceso_matriculacion',
        string='Proceso',
        default=_set_default_proceso,
        required=True
    )

    profesor_id = fields.Many2one(
        'res.partner',
        string='Profesor',
        default=lambda self: self.env.user.partner_id,
        domain=[('is_profesor', '=', True)],
        required=True
    )

    materias_id = fields.Many2one('educational.materias',
                               string='Materias Asignadas',
                               domain=[('id', '=', 0)],
                               required=True
                               )

    calificaciones_line = fields.One2many(
        'educational.calificaciones_line',
        'calificaciones_id',
        string='Calificaciones',
        #default=_fill_alumnos
        required=True
    )

    parcial = fields.Selection([('P', '1er'), ('S', '2do')],
                               string='Parcial',
                               required=True)

    matriculacion_line = fields.Many2one(
        'educational.matriculacion_line',
        string='Paralelos',
        required=True
    )

    @api.onchange('profesor_id')
    def _set_domain_materias(self):
        '''
            CARGAR LAS MATERIAS A TRAVES DEL PROCESO Y LA MATRICULACION_LINE (PARALELOS Y AULAS)
        '''

        if self.profesor_id:
            # Obtener proceso de mariculación activo
            proceso_id = self.env['educacional.proceso_matriculacion'].search([('activar', '=', True)], limit=1).id

            # Obtener los ids de la tabla enrolment teacher
            ids_enrolment_teacher = self.env['educational.enrolment_teacher'].search([('proceso_id', '=', proceso_id),
                                                                                      #('matriculacion_line', '=', self.matriculacion_line)
                                                                                       ])

            line = self.env['educational.enrolment_teacher_line'].search([('profesor_id', '=', self.profesor_id.id),
                                                                          ('enrolment_teacher_id', 'in', ids_enrolment_teacher.ids)])
            arr_id = []
            arr_name = []
            for mat in line:
                arr_id.append(mat.materias_id.id)
                arr_name.append(mat.materias_id.name)

            return {'domain': {'materias_id': [('id', 'in', arr_id), ('name', 'in', arr_name)]}}

    @api.onchange('materias_id', 'profesor_id')
    def _fill_alumnos_ids(self):
        '''
            CARGAR EL LISTADO DE ALUMNOS PARA LA CALIFICACIÓN
        '''

        if self.materias_id.niveles and self.proceso_id.id and self.proceso_id.carreras.id and self.parcial:

            #Registros de Profesores y Materias
            #teachers = self.env['educacional.enrolment_teacher']

            #Registros de Alumnos
            enrolments = self.env['educational.enrolment'].search([('nivel', '=', self.materias_id.niveles),
                                                                    #('paralelos', '=', paralelo_id),
                                                                    ('proceso', '=', self.proceso_id.id)])
            r = []

            for enrol in enrolments:
                r.append({'alumno_id': enrol.alumno_id.id})
                          #'proceso_id': self.proceso_id.id,
                          #'parcial': self.parcial})

            return {'value': {'calificaciones_line': r}}