# -*- coding: utf-8 -*-
{
    'name': "Educational Calificaciones",

    'summary': """
        Modulo de calificaciones""",

    'description': """
        Tiene como objetivo de que los profesores puedan emitir las calificaciones que obtienen los estudiantes,
        durante el periodo educativo.
    """,

    'author': "Ing. Cristhian Carreño",
    'website': "http://www.webhosting.ml",

    'category': 'Education',
    'version': '1.0',

    'depends': ['base','educational_core','educational_matriculacion'],

    'data': [
        # 'security/ir.model.access.csv',
        'views/view_calificaciones.xml',
        'views/view_menuitem.xml',
        #'views/view_menuitem.xml',
    ],
    # only loaded in demonstration mode
    'demo': [

    ],
}