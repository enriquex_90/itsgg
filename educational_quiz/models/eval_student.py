# -*- coding: utf-8 -*-

from odoo import models, fields, api


class EduEvalStudent(models.Model):
    _name = 'edu.eval.student'
    _description = "Evaluaciones Estudiantes"
    _rec_name = 'subject_id'

    subject_id = fields.Many2one('edu.subject', string='Materia', required=True)
    parallel_id = fields.Many2one('edu.parallel', string='Paralelo', domain="[('period_id.current','=',True)]",
                                  required=True)

    period_id = fields.Many2one('edu.period', related='parallel_id.period_id', store=True)
    student_ids = fields.Many2many('edu.student', string='Estudiante')
    template_ids = fields.Many2one('edu.eval.template', string='Eval Estudiante')
    current = fields.Boolean(related='parallel_id.period_id.current')

    user_id = fields.Many2one('res.users', string="Evaluador",
                              help="Usuario que realiza la evaluación",
                              ondelete="restrict")
    question_template_id = fields.Many2one('edu.eval.template', string='Plantilla de Evaluación', required=True)




