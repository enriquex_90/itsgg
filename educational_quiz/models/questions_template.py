from odoo import models, fields


class EduQuestionsTemplate(models.Model):
    _name = 'edu.questions.template'
    _description = 'Plantilla de Preguntas'
    _rec_name = 'name'

    def _get_default_period(self):
        period = self.env['edu.period'].search([('current', '=', True)])
        if period:
            return period[0].id
        return None

    def _get_default_faculty(self):
        faculty = self.env['edu.faculty'].search([('user_id', '=', self.env.user.id)])
        if faculty:
            return faculty[0].id
        return None

    name = fields.Char("Pregunta", required=True)
    period_id = fields.Many2one('edu.period', string='Periodo', default=_get_default_period)
    type = fields.Selection([('M', 'Multiple'), ('U', 'Unica')], "Tipo de Respuestas", default="U", required=True)
    answers_template_ids = fields.One2many('edu.answers.template', inverse_name='question_template_id',
                                           required=True, copy=True,
                                           string="Respuestas")
    subject_id = fields.Many2one('edu.subject', string='Materia', required=True)
    is_complexive = fields.Boolean('Examen complexivo?')
    faculty_id = fields.Many2one('edu.faculty', "Docente", default=_get_default_faculty)
    partial = fields.Selection([('1', '1er Parcial'), ('2', '2do Parcial')], "Parcial")
    topic = fields.Char('Tema')
    description = fields.Text('Descripción')
    image = fields.Binary('Imagen')
    special_needs = fields.Selection([('1','Grado 1'),('2','Grado 2'),('3','Grado 3')], string='Necesidades Especiales')
    # template_id = fields.Many2one('edu.eval.template', "Plantilla")

