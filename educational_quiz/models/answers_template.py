from odoo import models, fields


class EduAnswersTemplate(models.Model):
    _name = 'edu.answers.template'
    _description = 'Plantilla de Respuestas'
    _rec_name = 'name'

    name = fields.Char("Respuesta", required=True)
    score = fields.Float('Puntaje Máximo')
    state = fields.Boolean('Correcto', default=False)
    question_template_id = fields.Many2one('edu.questions.template', "Preguntas")
