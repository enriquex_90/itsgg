from odoo import models, fields, api
from odoo.exceptions import UserError
import random


class EduEvalTemplate(models.Model):
    _name = 'edu.eval.template'
    _description = 'Plantilla Evaluaciones'

    def _get_default_period(self):
        period = self.env['edu.period'].search([('current', '=', True)])
        if period:
            return period[0].id
        return None

    def _get_default_faculty(self):
        faculty = self.env['edu.faculty'].search([('user_id', '=', self.env.user.id)])
        if faculty:
            return faculty[0].id
        return None

    name = fields.Char("Nombre", compute='_compute_name')
    sequence = fields.Char("Número", readonly=True)
    start_date = fields.Datetime("Fecha de Inicio", states={'draft': [('readonly', False)]}, readonly=True,
                                 required=True)
    end_date = fields.Datetime("Fecha de Fin", states={'draft': [('readonly', False)]}, readonly=True, required=True)
    note = fields.Float("Nota")
    type_eval = fields.Selection([('A', 'Aporte'), ('E', 'Examen')], "Tipo", default="A",
                                 states={'draft': [('readonly', False)]}, readonly=True)
    state = fields.Selection([('draft', 'Borrador'), ('published', 'Publicado'), ('finished', 'Finalizado')],
                             "Estado", default="draft", readonly=True)
    faculty_id = fields.Many2one('edu.faculty', "Docente", default=_get_default_faculty,
                                 states={'draft': [('readonly', False)]}, readonly=True)
    subject_parallel_id = fields.Many2one('edu.subject.parallel', "Paralelo",
                                          domain="[('period_id','=',period_id),('faculty_id','=',faculty_id)]",
                                          states={'draft': [('readonly', False)]}, readonly=True
                                          )
    question_template_ids = fields.Many2many('edu.questions.template', string="Preguntas",
                                             states={'draft': [('readonly', False)]}, readonly=True
                                             )
    period_id = fields.Many2one('edu.period', string='Periodo', default=_get_default_period,
                                states={'draft': [('readonly', False)]}, readonly=True)

    eval_ids = fields.One2many('edu.eval', string='Evaluaciones Estudiantes', inverse_name='template_id',
                               states={'draft': [('readonly', False)]})
    question_number = fields.Integer('Número de Preguntas', states={'draft': [('readonly', False)]}, readonly=True)
    subject_id = fields.Many2one('edu.subject', string='Materia', related='subject_parallel_id.subject_id')
    student_ids = fields.Many2many('edu.student', string='Estudiantes')
    partial = fields.Selection([('1', '1er Parcial'), ('2', '2do Parcial'), ('3', 'Recuperación')], "Parcial",
                               states={'draft': [('readonly', False)]})

    # eval_id = fields.Many2one('edu.eval', 'Evaluacion')

    @api.multi
    @api.depends('subject_parallel_id', 'faculty_id', 'start_date')
    def _compute_name(self):
        for record in self:
            if record.subject_parallel_id and record.faculty_id and record.start_date:
                record.name = "{} - {} - {}".format(record.subject_parallel_id.name,
                                                    record.faculty_id.name,
                                                    record.start_date)

    @api.multi
    def action_to_draft(self):
        for record in self:
            record.state = 'draft'

    @api.multi
    def action_to_published(self):
        for record in self:
            record.state = 'published'
            for eval in record.eval_ids:
                eval.state = 'published'

    @api.multi
    def action_to_finished(self):
        for record in self:
            record.state = 'finished'
            for eval in record.eval_ids:
                eval.calculate_score()
                eval.state = 'finished'

    @api.multi
    def load_students(self):
        for record in self:
            if not record.subject_parallel_id:
                raise UserError('Debes primero seleccionar un paralelo')
            EVAL = self.env['edu.eval']
            student_ids = []
            for eval_id in record.eval_ids:
                student_ids.append(eval_id.student_id.id)
            for student in record.subject_parallel_id.student_ids:
                if student.id not in student_ids:
                    values = {'name': self.name, 'faculty_id': self.faculty_id.id, 'state': self.state,
                              'start_date': self.start_date, 'end_date': self.end_date, 'type_eval': self.type_eval,
                              'user_id': student.user_id.id, 'student_id': student.id, 'template_id': self.id,
                              'sequence': self.sequence, 'partial': self.partial, 'period_id': self.period_id.id,
                              'subject_parallel_id': self.subject_parallel_id.id, 'eval_question_ids': []}
                    EVAL.create(values)

    @api.multi
    def generate_questions(self):
        for record in self:
            for eval_id in record.eval_ids:
                values = {'eval_question_ids': []}
                if self.question_number > len(self.question_template_ids):
                    raise UserError("El número de preguntas debe ser menor o igual a las preguntas seleccionadas")
                if len(eval_id.eval_question_ids) < self.question_number:
                    questions = random.sample(self.question_template_ids, self.question_number)
                    for question in questions:
                        answers = []
                        answers_random = random.sample(question.answers_template_ids,
                                                       len(question.answers_template_ids))
                        for answer_template in answers_random:
                            answers.append((0, 0, {'name': answer_template.name, 'is_correct': answer_template.state}))
                        values['eval_question_ids'].append(
                            (0, 0, {'name': question.name,
                                    'description': question.description,
                                    'question_template_id': question.id,
                                    'answers_ids': answers}))
                    eval_id.write(values)

    @api.multi
    def recalculate(self):
        for record in self:
            for eval_id in record.eval_ids:
                for question in eval_id.eval_question_ids:
                    for answer in question.answers_ids:
                        for answer_template in question.question_template_id.answers_template_ids:
                            if answer.name == answer_template.name:
                                answer.is_correct = answer_template.state
                eval_id.calculate_score()

    @api.model
    def create(self, vals):
        vals['sequence'] = self.env['ir.sequence'].next_by_code('edu.eval')
        eval_template = super(EduEvalTemplate, self).create(vals)
        self.save_eval_params(eval_template)
        return eval_template

    @api.multi
    def write(self, vals):
        res = super(EduEvalTemplate, self).write(vals)
        self.save_eval_params(self)
        return res

    def save_eval_params(self, eval_template_ids):
        for eval_template in eval_template_ids:
            for eval_id in eval_template.eval_ids:
                values = {'name': eval_template.name, 'faculty_id': eval_template.faculty_id.id,
                          'state': eval_template.state,
                          'start_date': eval_template.start_date, 'end_date': eval_template.end_date,
                          'type_eval': eval_template.type_eval,
                          'sequence': eval_template.sequence, 'partial': eval_template.partial,
                          'period_id': eval_template.period_id.id,
                          'subject_parallel_id': eval_template.subject_parallel_id.id}
                eval_id.write(values)

