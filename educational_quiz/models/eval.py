from odoo import models, fields, api
from odoo.exceptions import UserError


class EduEval(models.Model):
    _name = 'edu.eval'
    _description = 'Evaluaciones'
    _rec_name = 'name'

    name = fields.Char("Nombre")
    sequence = fields.Char("Número", readonly=1)
    start_date = fields.Datetime("Fecha de Inicio")
    end_date = fields.Datetime("Fecha de Fin")
    type_eval = fields.Selection([('A', 'Aporte'), ('E', 'Examen')], "Tipo")
    state = fields.Selection([('draft', 'Borrador'), ('published', 'Publicado'), ('finished', 'Finalizado')],
                             "Estado", default="draft")
    faculty_id = fields.Many2one('edu.faculty', "Docente")
    student_id = fields.Many2one('edu.student', "Estudiante")
    subject_parallel_id = fields.Many2one('edu.subject.parallel', "Materia/Paralelo")
    subject_id = fields.Many2one('edu.subject', 'Materia', related='subject_parallel_id.subject_id', store=True)
    eval_question_ids = fields.One2many('edu.eval.question', string="Preguntas", inverse_name='eval_id')
    career_id = fields.Many2one('edu.career',
                                related='faculty_id.career_id',
                                store=True)
    user_id = fields.Many2one('res.users', string="Evaluador", related='student_id.user_id', store=True,
                              help="Usuario que realiza la evaluación",
                              ondelete="restrict")
    template_id = fields.Many2one('edu.eval.template', string='Plantilla')
    score = fields.Float("Nota", readonly=True)
    partial = fields.Selection([('1', '1er Parcial'), ('2', '2do Parcial'), ('3', 'Recuperación')], "Parcial",
                               readonly=True)
    period_id = fields.Many2one('edu.period', string='Periodo', readonly=True)

    @api.multi
    def calculate_score(self):
        for record in self:
            score = 0
            for question in record.eval_question_ids:
                for answer in question.answers_ids:
                    if answer.is_selected and answer.is_correct:
                        score += 1
            record.score = score

    @api.multi
    def action_to_draft(self):
        for record in self:
            record.state = 'draft'

    @api.multi
    def action_to_published(self):
        for record in self:
            record.state = 'published'

    @api.multi
    def action_to_finished(self):
        for record in self:
            record.state = 'finished'
            record.sudo().calculate_score()


class EduEvalQuestion(models.Model):
    _name = 'edu.eval.question'
    _description = 'Preguntas'
    _rec_name = 'name'

    name = fields.Char("Pregunta", required=True)
    question_template_id = fields.Many2one('edu.questions.template')
    description = fields.Text('Descripción')
    eval_id = fields.Many2one('edu.eval', string='Evaluación', ondelete='cascade')
    type = fields.Selection([('M', 'Multiple'), ('U', 'Unica')], "Tipo de Respuestas", default="U", required=True)
    answers_ids = fields.One2many('edu.eval.answer', inverse_name='eval_question_id',
                                  required=True,
                                  string="Respuestas")
    has_image = fields.Boolean(compute='_compute_has_image', store=True)

    @api.multi
    @api.depends('question_template_id.image')
    def _compute_has_image(self):
        for record in self:
            if record.question_template_id.image:
                record.has_image = True
            else:
                record.has_image = False

    @api.model
    def set_answers_false(self, id, answer_id):
        question = self.browse(id)
        if question:
            for answer in question.answers_ids:
                if answer.id == answer_id:
                    answer.is_selected = True
                else:
                    answer.is_selected = False


class EduEvalAnswer(models.Model):
    _name = 'edu.eval.answer'
    _description = 'Respuesta'

    name = fields.Char('Respuesta', required=True)
    eval_question_id = fields.Many2one('edu.eval.question', string='Pregunta', ondelete='cascade')
    is_selected = fields.Boolean('Marcar')
    is_correct = fields.Boolean('Correcta', groups="educational_core.group_edu_faculty")

    @api.constrains('is_selected')
    def check_state(self):
        for record in self:
            if record.eval_question_id.eval_id.state == 'finished':
                raise UserError('La prueba ha finalizado no puedes contestar mas preguntas')
