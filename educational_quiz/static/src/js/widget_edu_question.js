/* Copyright 2015 Holger Brunn <hbrunn@therp.nl>
 * Copyright 2016 Pedro M. Baeza <pedro.baeza@tecnativa.com>
 * Copyright 2018 Simone Orsi <simone.orsi@camptocamp.com>
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl). */

odoo.define('educational_quiz.widget', function (require) {
    "use strict";

    var field_registry = require('web.field_registry');
    var relational_fields = require('web.relational_fields');
    var EduQuestionRendered = require(
        'educational_quiz.EduQuestionRendeder'
    );

    var WidgetEduQuestion = relational_fields.FieldOne2Many.extend({
        className: 'o_field_one2many',
        supportedFieldTypes: ['one2many'],

        /**
         * @override
         */
        init: function () {
            this._super.apply(this, arguments);

            // boolean used to prevent concurrent record creation
            this.creatingRecord = false;
        },

        //--------------------------------------------------------------------------
        // Public
        //--------------------------------------------------------------------------
        /**
         * @override
         * @param {Object} record
         * @param {OdooEvent} [ev] an event that triggered the reset action
         * @returns {Deferred}
         */
        reset: function (record, ev) {
            var self = this;
            return this._super.apply(this, arguments).then(function () {
                if (ev && ev.target === self && ev.data.changes && self.view.arch.tag === 'tree') {
                    if (ev.data.changes[self.name].operation === 'CREATE') {
                        var index = 0;
                        if (self.editable !== 'top') {
                            index = self.value.data.length - 1;
                            // we consider that a new record, created in the bottom,
                            // does not count as a record worth mentioning in the
                            // pager, at least not until the line has been saved.
                            // We prevent the pager from increasing its size, which
                            // means that the pager is not displayed when we just
                            // reach the limit.  For example, if limit is 3, if we
                            // have 3 records, and we click on add, we will see the
                            // 4 records on the same page, but we do not want a
                            // pager.
                            self.pager.updateState({size: self.value.count - 1});
                        }
                        var newID = self.value.data[index].id;
                        self.renderer.editRecord(newID);
                    }
                }
            });
        },

        //--------------------------------------------------------------------------
        // Private
        //--------------------------------------------------------------------------

        /**
         * Overrides to only render the buttons if the 'create' action is available.
         *
         * @override
         * @private
         */
        _renderButtons: function () {
            if (this.activeActions.create) {
                this._super.apply(this, arguments);
            }
        },
        /**
         * @private
         * @param {Object} params
         * @param {Object} [params.context] We allow additional context, this is
         *   used for example to define default values when adding new lines to
         *   a one2many with control/create tags.
         */
        _openFormDialog: function (params) {
            var context = this.record.getContext(_.extend({},
                this.recordParams,
                {additionalContext: params.context}
            ));
            this.trigger_up('open_one2many_record', _.extend(params, {
                domain: this.record.getDomain(this.recordParams),
                context: context,
                field: this.field,
                fields_view: this.attrs.views && this.attrs.views.form,
                parentID: this.value.id,
                viewInfo: this.view,
                deletable: this.activeActions.delete,
            }));
        },

        _render: function () {
            if (!this.view) {
                return this._super();
            }
            // Ensure widget is re initiated when rendering
            // this.init_matrix();
            var arch = this.view.arch;
            // Update existing renderer
            // if (!_.isUndefined(this.renderer)) {
            //     return this.renderer.updateState(this.value, {
            //         matrix_data: this.matrix_data,
            //     });
            // }
            this.question_data = [{id:"1", name:'pregunta 1', answers:[{name:'Opcion 1' },{name:'Opcion 2' }]},
                {id:"2", name:'pregunta 2',  answers:[{name:'Opcion 1' },{name:'Opcion 2' }]}];
            // Create a new matrix renderer
            var is_finished = false;
            if (this.recordData.state == 'finished'){
                is_finished = true;
            }
            this.renderer = new EduQuestionRendered(this, this.value, {
                arch: arch,
                editable: this.mode === 'edit' && arch.attrs.editable,
                readonly: this.mode === 'readonly',
                viewType: "list",
                question_data : this.question_data,
                is_finished: is_finished,
            });
            this.$el.addClass('o_field_x2many o_field_x2many_2d_matrix');
            return this.renderer.appendTo(this.$el);
        },

        //--------------------------------------------------------------------------
        // Handlers
        //--------------------------------------------------------------------------

        /**
         * Opens a FormViewDialog to allow creating a new record for a one2many.
         *
         * @override
         * @private
         * @param {OdooEvent|MouseEvent} ev this event comes either from the 'Add
         *   record' link in the list editable renderer, or from the 'Create' button
         *   in the kanban view
         * @param {Array} ev.data.context additional context for the added records,
         *   if several contexts are provided, multiple records will be added
         *   (form dialog will only use the context at index 0 if provided)
         * @param {boolean} ev.data.forceEditable this is used to bypass the dialog opening
         *   in case you want to add record(s) to a list
         * @param {function} ev.data.onSuccess called when the records are correctly created
         *   (not supported by form dialog)
         * @param {boolean} ev.data.allowWarning defines if the records can be added
         *   to the list even if warnings are triggered (e.g: stock warning for product availability)
         */
        _onAddRecord: function (ev) {
            var self = this;
            var data = ev.data || {};

            // we don't want interference with the components upstream.
            ev.stopPropagation();

            if (this.editable || data.forceEditable) {
                if (!this.activeActions.create) {
                    if (data.onFail) {
                        data.onFail();
                    }
                } else if (!this.creatingRecord) {
                    this.creatingRecord = true;
                    this.trigger_up('edited_list', {id: this.value.id});
                    this._setValue({
                        operation: 'CREATE',
                        position: this.editable || data.forceEditable,
                        context: data.context,
                    }, {
                        allowWarning: data.allowWarning
                    }).always(function () {
                        self.creatingRecord = false;
                    }).done(function () {
                        if (data.onSuccess) {
                            data.onSuccess();
                        }
                    });
                }
            } else {
                this._openFormDialog({
                    context: data.context && data.context[0],
                    on_saved: function (record) {
                        self._setValue({operation: 'ADD', id: record.id});
                    },
                });
            }
        },
        /**
         * Overrides the handler to set a specific 'on_save' callback as the o2m
         * sub-records aren't saved directly when the user clicks on 'Save' in the
         * dialog. Instead, the relational record is changed in the local data, and
         * this change is saved in DB when the user clicks on 'Save' in the main
         * form view.
         *
         * @private
         * @param {OdooEvent} ev
         */
        _onOpenRecord: function (ev) {
            // we don't want interference with the components upstream.
            var self = this;
            ev.stopPropagation();

            var id = ev.data.id;
            var onSaved = function (record) {
                if (_.some(self.value.data, {id: record.id})) {
                    // the record already exists in the relation, so trigger an
                    // empty 'UPDATE' operation when the user clicks on 'Save' in
                    // the dialog, to notify the main record that a subrecord of
                    // this relational field has changed (those changes will be
                    // already stored on that subrecord, thanks to the 'Save').
                    self._setValue({operation: 'UPDATE', id: record.id});
                } else {
                    // the record isn't in the relation yet, so add it ; this can
                    // happen if the user clicks on 'Save & New' in the dialog (the
                    // opened record will be updated, and other records will be
                    // created)
                    self._setValue({operation: 'ADD', id: record.id});
                }
            };
            this._openFormDialog({
                id: id,
                on_saved: onSaved,
                on_remove: function () {
                    self._setValue({operation: 'DELETE', ids: [id]});
                },
                deletable: this.activeActions.delete,
                readonly: this.mode === 'readonly',
            });
        },
    });

    field_registry.add('WidgetEduQuestion', WidgetEduQuestion);

    return {
        WidgetEduQuestion: WidgetEduQuestion,
    };

});
