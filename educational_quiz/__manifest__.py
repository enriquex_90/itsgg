# -*- coding: utf-8 -*-
# noinspection PyStatementEffect
{
    'name': "Modulo de Examenes",

    'summary': """
        Modulo para evaluaciones en linea""",

    'description': """
        Modulo para evaluaciones en linea
    """,

    'author': "Manexware",
    'website': "https://www.manexware.com/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'educational_core'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'data/sequence_data.xml',
        'views/assets.xml',
        'views/menu.xml',
        'views/eval_template_views.xml',
        'views/questions_template_views.xml',
        'views/answers_template_views.xml',
        'views/eval_views.xml',
        # 'views/eval_student_views.xml',
        # 'views/questions_views.xml',
        # 'views/answers_views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
