# -*- coding: utf-8 -*-


from odoo import _, models, fields, api


class EduSurveyType(models.Model):
    _name = 'edu.survey.type'
    _description = 'Survey Type'

    name = fields.Char(string="Nombre", required=True)
    version = fields.Float(string="Version")
    code = fields.Char(string=u"Código")
    type = fields.Selection([('E', 'ESTUDIANTES'),
                             ('D', 'DOCENTES'),
                             ('C', 'CORDINADORES'),
                             ('B', 'BIBLIOTECARIO'),
                             ('A', 'ADMINISTRADOR'),
                             ('G', 'GESTOR TITULACION'),
                             ('M', 'GESTOR MATRICULACION'),
                             ('P', 'GESTOR PRACTICAS'),
                             ('R', 'GESTOR RRHH'),
                             ('S', 'SECRETARIA')],
                            string="Encuesta dirijida a", required=True)

    '''
    #VARIABLES SEGÚN SISTEMA ACADÉMICO DE MARCELO

    A ADMINISTRADOR
    B BIBLIOTECARIO
    C COORDINADOR
    D DOCENTE
    E ESTUDIANTE
    G GESTOR TITULACION
    M GESTOR MATRICULACION
    P GESTOR PRACTICAS
    R GESTOR RRHH
    S SECRETARIA
    '''
