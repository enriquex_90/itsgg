# -*- coding: utf-8 -*-


from odoo import _, models, fields, api


class EduSurveyQuestion(models.Model):
    _name = 'edu.survey.question'
    _description = 'Survey question'

    sequence = fields.Integer('Secuencia')
    name = fields.Char(string="Nombre", required=True)
    criteria_id = fields.Many2one('edu.survey.question.criteria', string="Criterio")


class EduSurveyQuestionCriteria(models.Model):
    _name = 'edu.survey.question.criteria'
    _description = 'Criterio de pregunta'

    name = fields.Char(string="Nombre", required=True)

    _sql_constraints = [
        ('name_unique',
         'unique (name)',
         'Criterio duplicado')
    ]