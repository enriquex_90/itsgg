from odoo import models, fields, api, exceptions


class EduSurveyFaculty(models.Model):
    _name = 'edu.survey.faculty'
    _description = 'Survey Eval'

    name = fields.Char(compute='_compute_name')
    state = fields.Selection(
        [('draft', 'Borrador'), ('validated', 'Validado')], default="draft")
    period_id = fields.Many2one('edu.period', 'Periodo')
    faculty_survey_ids = fields.One2many('edu.survey.faculty.survey',
                                         inverse_name="survey_faculty_id")

    department_id = fields.Many2one('edu.department', 'Departamento',
                                    related='faculty_id.career_id.department_id',
                                    store=True)
    faculty_id = fields.Many2one('edu.faculty', "Docente")

    #

    hr_weekly = fields.Selection([('valor1', '20'), ('valor2', '40')], "Horas semanales", required=True,
                                 default='valor1', readonly=True)

    he_teaching = fields.Float("Hetero Evaluación Docencia", compute="compute_he_teaching")
    he_trainer = fields.Float("Hetero Evaluación Formación")
    he_tutor_integrating = fields.Float("Hetero Evaluación Tutor de cátedra")
    he_tutor_pedagogical = fields.Float("Hetero Evaluación Tutor pedagógico")
    he_tutor_practices = fields.Float("Hetero Evaluación Tutor practicas")
    he_tutor_certification = fields.Float("Hetero EvaluaciónTutor Titulacion")
    he_linkage = fields.Float("Hetero Evaluación Viculación/PPP")
    he_management = fields.Float("Hetero Evaluación Gestión Academica")
    he_investigation = fields.Float("Hetero Evaluación Investigación/Titulación")

    he_r_teaching = fields.Float("Total Hetero Evaluación Docencia", readonly=True, compute="compute_he_r_teaching")
    he_r_linkage = fields.Float("Total Hetero Evaluación Vinculación", readonly=True, compute="compute_he_r_linkage")
    he_r_management = fields.Float("Total Hetero Evaluación Gestión", readonly=True, compute="compute_he_r_management")
    he_r_investigation = fields.Float("Total Hetero Evaluación Investigación", readonly=True,
                                      compute="compute_he_r_investigation")

    he_r_teaching_e = fields.Float('Resultado Hetero Evaluación Docencia')
    he_r_linkage_e = fields.Float('Resultado Hetero Evaluación  Viculación')
    he_r_management_e = fields.Float('Resultado Hetero Evaluación  Gestión')
    he_r_investigation_e = fields.Float('Resultado Hetero Evaluación  Investgación')

    ae_teaching = fields.Float("Auto Evaluación Docencia", compute="compute_ae_teaching")
    ae_trainer = fields.Float("Auto Evaluación Formación")
    ae_tutor_integrating = fields.Float("Auto Evaluación Tutor de cátedra")
    ae_tutor_pedagogical = fields.Float("Auto Evaluación Tutor pedagógico")
    ae_tutor_practices = fields.Float("Auto Evaluación Tutor practicas")
    ae_tutor_certification = fields.Float("Auto Evaluación Tutor Titulacion")
    ae_linkage = fields.Float("Auto Evaluación Viculación/PPP")
    ae_management = fields.Float("Auto Evaluación Gestión Academica")
    ae_investigation = fields.Float("Auto Evaluación Investigación/Titulación")

    ae_r_teaching = fields.Float("Total Auto Evaluación Docencia", readonly=True, compute="compute_ae_r_teaching")
    ae_r_linkage = fields.Float("Total Auto Evaluación Vinculación", readonly=True, compute="compute_ae_r_linkage")
    ae_r_management = fields.Float("Total Auto Evaluación Gestión", readonly=True, compute="compute_ae_r_management")
    ae_r_investigation = fields.Float("Total Auto Evaluación Investigación", readonly=True,
                                      compute="compute_ae_r_investigation")

    ae_r_teaching_e = fields.Float('Resultado Auto Evaluación Docencia')
    ae_r_linkage_e = fields.Float('Resultado Auto Evaluación Viculación')
    ae_r_management_e = fields.Float('Resultado Auto Evaluación Gestión')
    ae_r_investigation_e = fields.Float('Resultado Auto Evaluación Investigación')

    cd_teaching = fields.Float("Co-Evaluación de Directivos Docencia", compute="compute_cd_teaching")
    cd_trainer = fields.Float("Co-Evaluación de Directivos Formación")
    cd_tutor_integrating = fields.Float("Co-Evaluación de Directivos Tutor de cátedra")
    cd_tutor_pedagogical = fields.Float("Co-Evaluación de Directivos Tutor pedagógico")
    cd_tutor_practices = fields.Float("Co-Evaluación de Directivos Tutor practicas")
    cd_tutor_certification = fields.Float("Co-Evaluación de Directivos Tutor Titulacion")
    cd_linkage = fields.Float("Co-Evaluación de Directivos Viculación/PPP")
    cd_management = fields.Float("Co-Evaluación de Directivos Gestión Academica")
    cd_investigation = fields.Float("Co-Evaluación de Directivos Investigación/Titulación")

    cd_r_teaching = fields.Float("Total Co-Evaluación de Directivos Docencia", readonly=True,
                                 compute="compute_cd_r_teaching")
    cd_r_linkage = fields.Float("Total Co-Evaluación de Directivos Vinculación", readonly=True,
                                compute="compute_cd_r_linkage")
    cd_r_management = fields.Float("Total Co-Evaluación de Directivos Gestión", readonly=True,
                                   compute="compute_cd_r_management")
    cd_r_investigation = fields.Float("Total Co-Evaluación de Directivos Investigación", readonly=True,
                                      compute="compute_cd_r_investigation")

    cd_r_teaching_e = fields.Float('Resultado Co-Evaluación de Directivos Docencia')
    cd_r_linkage_e = fields.Float('Resultado Co-Evaluación de Directivos Vinculación')
    cd_r_management_e = fields.Float('Resultado Co-Evaluación de Directivos Gestión')
    cd_r_investigation_e = fields.Float('Resultado Co-Evaluación de Directivos Investigación')

    ca_teaching = fields.Float("Co-Evaluación Pares Academicos Docencia", compute="compute_ca_teaching")
    ca_trainer = fields.Float("Co-Evaluación Pares Academicos Formación")
    ca_tutor_integrating = fields.Float("Co-Evaluación Pares Academicos Tutor de cátedra")
    ca_tutor_pedagogical = fields.Float("Co-Evaluación Pares Academicos Tutor pedagógico")
    ca_tutor_practices = fields.Float("Co-Evaluación Pares Academicos Tutor practicas")
    ca_tutor_certification = fields.Float("Co-Evaluación Pares Academicos Tutor Titulacion")
    ca_linkage = fields.Float("Co-Evaluación Pares Academicos Viculación/PPP")
    ca_management = fields.Float("Co-Evaluación Pares Academicos Gestión Academica")
    ca_investigation = fields.Float("Co-Evaluación Pares Academicos Investigación/Titulación")

    ca_r_teaching = fields.Float("Total Co-Evaluación Pares Academicos Docencia", readonly=True,
                                 compute="compute_ca_r_teaching")
    ca_r_linkage = fields.Float("Total Co-Evaluación Pares Academicos Vinculación", readonly=True,
                                compute="compute_ca_r_linkage")
    ca_r_management = fields.Float("Total Co-Evaluación Pares Academicos Gestión", readonly=True,
                                   compute="compute_ca_r_management")
    ca_r_investigation = fields.Float("Total Co-Evaluación Pares Academicos Investigación", readonly=True,
                                      compute="compute_ca_r_investigation")

    ca_r_teaching_e = fields.Float('Resultado Co-Evaluación Pares Academicos Docencia')
    ca_r_linkage_e = fields.Float('Resultado Co-Evaluación Pares Academicos Vinculación')
    ca_r_management_e = fields.Float('Resultado Co-Evaluación Pares Academicos Gestión')
    ca_r_investigation_e = fields.Float('Resultado Co-Evaluación Pares Academicos Investigación')

    t_teaching = fields.Float("Total General Docencia", compute="compute_t_teaching")
    t_linkage = fields.Float("Total General Viculación/PPP", compute="compute_t_linkage")
    t_management = fields.Float("Total General Gestión Academica", compute="compute_t_management")
    t_investigation = fields.Float("Total General Investigación/Titulación", compute="compute_t_investigation")

    total = fields.Float("Total General", compute="compute_total")

    hr_teaching = fields.Integer("Horas de Docencia", compute="compute_hr_teaching")
    hr_r_trainer = fields.Integer("Horas de Formación")
    hr_r_tutor_integrating = fields.Integer("Horas de Tutor de cátedra")
    hr_r_tutor_pedagogical = fields.Integer("Horas de Tutor pedagógico")
    hr_r_tutor_practices = fields.Integer("Horas de Tutor practicas")
    hr_r_tutor_certification = fields.Integer("Horas de Tutor Titulacion")
    hr_linkage = fields.Integer("Horas de Viculación/PPP")
    hr_management = fields.Integer("Horas de Gestión Academica")
    hr_investigation = fields.Integer("Horas de Investigación/Titulación")

    trainer = fields.Boolean()
    tutor_integrating = fields.Boolean()
    tutor_pedagogical = fields.Boolean()
    tutor_practices = fields.Boolean()
    tutor_certification = fields.Boolean()
    link = fields.Boolean()
    manager = fields.Boolean()
    researcher = fields.Boolean()

    #

    hr_trainer = fields.Integer("Formación horas")
    hr_tutor_integrating = fields.Integer("Tutor de Cátedra horas")
    hr_tutor_pedagogical = fields.Integer("Tutor pedagógico horas")
    hr_tutor_practices = fields.Integer("Tutor practicas horas")
    hr_tutor_certification = fields.Integer("Tutor Titulacion horas")
    hr_link = fields.Integer(u"Vinculación horas")
    hr_manager = fields.Integer("Gestor horas")
    hr_researcher = fields.Integer("Investigador horas")
    hr_total = fields.Integer('Total de horas', compute="compute_hr_total", store=True)

    is_trainer = fields.Boolean("Formación")
    is_tutor_integrating = fields.Boolean("Tutor de cátedra")
    is_tutor_pedagogical = fields.Boolean("Tutor pedagógico")
    is_tutor_practices = fields.Boolean("Tutor practicas")
    is_tutor_certification = fields.Boolean("Tutor Titulacion")
    is_link = fields.Boolean(u"Vinculación")
    is_manager = fields.Boolean("Gestor")
    is_researcher = fields.Boolean("Investigador")

    hr_trainer = fields.Integer("Formación horas")
    hr_tutor_integrating = fields.Integer("Tutor de Cátedra horas")
    hr_tutor_pedagogical = fields.Integer("Tutor pedagógico horas")
    hr_tutor_practices = fields.Integer("Tutor practicas horas")
    hr_tutor_certification = fields.Integer("Tutor Titulacion horas")
    hr_link = fields.Integer("Vinculación horas")
    hr_manager = fields.Integer("Gestor horas")
    hr_researcher = fields.Integer("Investigador horas")
    hr_total = fields.Integer('Total de horas', compute="compute_hr_total", store=True)

    survey_ids = fields.One2many('edu.period.survey', string='Encuestas',
                                 inverse_name="period_id")

    directive = fields.Char("Directiva")
    position = fields.Char("Cargo")
    institution_actions = fields.Text("Acciones Institucionales")
    teacher_actions = fields.Text("Acciones Docente")

    @api.multi
    @api.depends('hr_trainer', 'hr_tutor_integrating', 'hr_tutor_practices', 'hr_tutor_certification',
                 'hr_tutor_pedagogical', 'hr_link', 'hr_manager', 'hr_researcher')
    def compute_hr_total(self):
        for record in self:
            record.hr_total = record.hr_trainer + record.hr_tutor_integrating + record.hr_tutor_practices + record.hr_tutor_certification + \
                              record.hr_tutor_pedagogical + record.hr_link + record.hr_manager + record.hr_researcher

    @api.multi
    @api.depends('faculty_id', 'period_id')
    def _compute_name(self):
        for record in self:
            if record.faculty_id and record.period_id:
                record.name = record.faculty_id.name + " - " + str(
                    record.period_id.name)
            else:
                record.name = "TEST"
                # record.name = record.faculty_id.name + " - " + record.survey_id.name

    #
    @api.multi
    @api.depends('he_r_teaching', 'ae_r_teaching', 'cd_r_teaching', 'ca_r_teaching')
    def compute_t_teaching(self):
        for record in self:
            record.t_teaching = (record.he_r_teaching + record.ae_r_teaching +
                                 record.cd_r_teaching + record.ca_r_teaching)

    @api.depends('he_r_linkage', 'ae_r_linkage', 'cd_r_linkage', 'ca_r_linkage')
    def compute_t_linkage(self):
        for record in self:
            record.t_linkage = (record.he_r_linkage + record.ae_r_linkage +
                                record.cd_r_linkage + record.ca_r_linkage)

    @api.depends('he_r_management', 'ae_r_management', 'cd_r_management', 'ca_r_management')
    def compute_t_management(self):
        for record in self:
            record.t_management = (record.he_r_management + record.ae_r_management +
                                   record.cd_r_management + record.ca_r_management)

    @api.depends('he_r_investigation', 'ae_r_investigation', 'cd_r_investigation', 'ca_r_investigation')
    def compute_t_investigation(self):
        for record in self:
            record.t_investigation = (record.he_r_investigation + record.ae_r_investigation +
                                      record.cd_r_investigation + record.ca_r_investigation)

    @api.depends('t_teaching', 't_linkage', 't_management', 't_investigation')
    def compute_total(self):
        for record in self:
            result1 = (record.hr_teaching + record.hr_linkage +
                       record.hr_management + record.hr_investigation)
            if result1 != 0:
                v1 = record.t_teaching * (record.hr_teaching / result1)
                v2 = record.t_linkage * (record.hr_linkage / result1)
                v3 = record.t_management * (record.hr_management / result1)
                v4 = record.t_investigation * (record.hr_investigation / result1)

                record.total = v1 + v2 + v3 + v4

    @api.depends('he_trainer', 'he_tutor_integrating',
                 'he_tutor_pedagogical', 'he_tutor_practices', 'he_tutor_certification')
    def compute_he_teaching(self):
        for record in self:
            result = record.hr_r_trainer + record.hr_r_tutor_integrating + \
                     record.hr_r_tutor_pedagogical + record.hr_r_tutor_practices + \
                     record.hr_r_tutor_certification

            if result != 0:
                v1 = record.he_trainer * (record.hr_r_trainer / result)
                v2 = record.he_tutor_integrating * (record.hr_r_tutor_integrating / result)
                v3 = record.he_tutor_pedagogical * (record.hr_r_tutor_pedagogical / result)
                v4 = record.he_tutor_practices * (record.hr_r_tutor_practices / result)
                v5 = record.he_tutor_certification * (record.hr_r_tutor_certification / result)

                aux = v1 + v2 + v3 + v4 + v5
                record.he_teaching = aux

    @api.depends('ae_trainer', 'ae_tutor_integrating',
                 'ae_tutor_pedagogical', 'ae_tutor_practices', 'ae_tutor_certification')
    def compute_ae_teaching(self):
        for record in self:
            result = record.hr_r_trainer + record.hr_r_tutor_integrating + \
                     record.hr_r_tutor_pedagogical + record.hr_r_tutor_practices + \
                     record.hr_r_tutor_certification
            if result != 0:
                v1 = record.ae_trainer * (record.hr_r_trainer / result)
                v2 = record.ae_tutor_integrating * (record.hr_r_tutor_integrating / result)
                v3 = record.ae_tutor_pedagogical * (record.hr_r_tutor_pedagogical / result)
                v4 = record.ae_tutor_practices * (record.hr_r_tutor_practices / result)
                v5 = record.ae_tutor_certification * (record.hr_r_tutor_certification / result)

                aux = v1 + v2 + v3 + v4 + v5
                record.ae_teaching = aux

    @api.depends('cd_trainer', 'cd_tutor_integrating',
                 'cd_tutor_pedagogical', 'cd_tutor_practices', 'cd_tutor_certification')
    def compute_cd_teaching(self):
        for record in self:
            result = record.hr_r_trainer + record.hr_r_tutor_integrating + \
                     record.hr_r_tutor_pedagogical + record.hr_r_tutor_practices + \
                     record.hr_r_tutor_certification
            if result != 0:
                v1 = record.cd_trainer * (record.hr_r_trainer / result)
                v2 = record.cd_tutor_integrating * (record.hr_r_tutor_integrating / result)
                v3 = record.cd_tutor_pedagogical * (record.hr_r_tutor_pedagogical / result)
                v4 = record.cd_tutor_practices * (record.hr_r_tutor_practices / result)
                v5 = record.cd_tutor_certification * (record.hr_r_tutor_certification / result)

                aux = v1 + v2 + v3 + v4 + v5
                record.cd_teaching = aux

    @api.depends('ca_trainer', 'ca_tutor_integrating',
                 'ca_tutor_pedagogical', 'ca_tutor_practices', 'ca_tutor_certification')
    def compute_ca_teaching(self):
        for record in self:
            result = record.hr_r_trainer + record.hr_r_tutor_integrating + \
                     record.hr_r_tutor_pedagogical + record.hr_r_tutor_practices + \
                     record.hr_r_tutor_certification
            if result != 0:
                v1 = record.ca_trainer * (record.hr_r_trainer / result)
                v2 = record.ca_tutor_integrating * (record.hr_r_tutor_integrating / result)
                v3 = record.ca_tutor_pedagogical * (record.hr_r_tutor_pedagogical / result)
                v4 = record.ca_tutor_practices * (record.hr_r_tutor_practices / result)
                v5 = record.ca_tutor_certification * (record.hr_r_tutor_certification / result)

                aux = v1 + v2 + v3 + v4 + v5
                record.ca_teaching = aux

    @api.depends('hr_r_trainer', 'hr_r_tutor_integrating',
                 'hr_r_tutor_pedagogical', 'hr_r_tutor_practices', 'hr_r_tutor_certification')
    def compute_hr_teaching(self):
        for record in self:
            record.hr_teaching = record.hr_r_trainer + record.hr_r_tutor_integrating + \
                                 record.hr_r_tutor_pedagogical + record.hr_r_tutor_practices + \
                                 record.hr_r_tutor_certification

    def calculate_hours(self):
        for record in self:
            self.hr_r_trainer = record.faculty_id.hr_trainer
            self.hr_r_tutor_integrating = record.faculty_id.hr_tutor_integrating
            self.hr_r_tutor_pedagogical = record.faculty_id.hr_tutor_pedagogical
            self.hr_r_tutor_practices = record.faculty_id.hr_tutor_practices
            self.hr_r_tutor_certification = record.faculty_id.hr_tutor_certification
            self.hr_linkage = record.faculty_id.hr_link
            self.hr_management = record.faculty_id.hr_manager
            self.hr_investigation = record.faculty_id.hr_researcher

    def calculate_hours_all(self):
        for record in self:
            self.hr_r_trainer = record.faculty_id.hr_trainer
            self.trainer = record.faculty_id.is_trainer
            self.hr_r_tutor_integrating = record.faculty_id.hr_tutor_integrating
            self.tutor_integrating = record.faculty_id.is_tutor_integrating
            self.hr_r_tutor_pedagogical = record.faculty_id.hr_tutor_pedagogical
            self.tutor_pedagogical = record.faculty_id.is_tutor_pedagogical
            self.hr_r_tutor_practices = record.faculty_id.hr_tutor_practices
            self.tutor_practices = record.faculty_id.is_tutor_practices
            self.hr_r_tutor_certification = record.faculty_id.hr_tutor_certification
            self.tutor_certification = record.faculty_id.is_tutor_certification
            self.hr_linkage = record.faculty_id.hr_link
            self.link = record.faculty_id.is_link
            self.hr_management = record.faculty_id.hr_manager
            self.manager = record.faculty_id.is_manager
            self.hr_investigation = record.faculty_id.hr_researcher
            self.researcher = record.faculty_id.is_researcher
            self.hr_weekly = record.faculty_id.hr_weekly

    def load_survey(self):
        SURVEY_EVAL = self.env['edu.survey.eval']
        faculty = self.faculty_id
        faculty_survey = []
        for survey in self.period_id.survey_ids:
            domain = [('survey_id', '=', survey.survey_id.id),
                      ('faculty_id', '=', faculty.id), ('period_id', '=', self.period_id.id)]
            survey_eval_ids = SURVEY_EVAL.search(domain + [
                ('state', '=', 'validated')])
            survey_eval_draft_ids = SURVEY_EVAL.search(domain +
                                                       [('state', '=', 'draft')])

            questions = []
            if survey_eval_ids:
                for questions_index, question in enumerate(survey.survey_id.question_ids):
                    question_score = 0
                    for survey_eval in survey_eval_ids:
                        try:
                            question_score += survey_eval.question_ids[questions_index].score
                        except IndexError as e:
                            print("error {}".format(survey_eval.id))
                    questions.append(
                        (0, 0, {'name': question.name, 'score': question_score / len(survey_eval_ids)}))
                faculty_survey.append(
                    (0, 0, {'survey_id': survey.survey_id.id,
                            'question_ids': questions}))
            else:
                if survey_eval_draft_ids:
                    for questions_index, question in enumerate(survey.survey_id.question_ids):
                        questions.append(
                            (0, 0, {'name': question.name, 'score': 5}))
                    faculty_survey.append(
                        (0, 0, {'survey_id': survey.survey_id.id,
                            'question_ids': questions}))

        self.faculty_survey_ids.unlink()
        self.write({'faculty_survey_ids': faculty_survey})

    def calculate(self):
        for record in self:
            values = {
            'he_trainer' : 0,
            'he_r_teaching_e' : 0,
            'he_tutor_integrating' : 0,
            'he_r_teaching_e' : 0,
            'he_tutor_pedagogical' : 0,
            'he_r_teaching_e' : 0,
            'he_tutor_practices' : 0,
            'he_r_teaching_e' : 0,
            'he_tutor_certification' : 0,
            'he_r_teaching_e' : 0,
            'he_linkage' : 0,
            'he_r_linkage_e' : 0,
            'he_management' : 0,
            'he_r_management_e' : 0,
            'he_investigation' : 0,
            'he_r_investigation_e' : 0,
            'ae_trainer' : 0,
            'ae_r_teaching_e' : 0,
            'ae_tutor_integrating' : 0,
            'ae_r_teaching_e' : 0,
            'ae_tutor_pedagogical' : 0,
            'ae_r_teaching_e' : 0,
            'ae_tutor_practices' : 0,
            'ae_r_teaching_e' : 0,
            'ae_tutor_certification' : 0,
            'ae_r_teaching_e' : 0,
            'ae_linkage' : 0,
            'ae_r_linkage_e' : 0,
            'ae_management' : 0,
            'ae_r_management_e' : 0,
            'ae_investigation' : 0,
            'ae_r_investigation_e' : 0,
            'cd_trainer' : 0,
            'cd_r_teaching_e' : 0,
            'cd_tutor_integrating' : 0,
            'cd_r_teaching_e' : 0,
            'cd_tutor_pedagogical' : 0,
            'cd_r_teaching_e' : 0,
            'cd_tutor_practices' : 0,
            'cd_r_teaching_e' : 0,
            'cd_tutor_certification' : 0,
            'cd_r_teaching_e' : 0,
            'cd_linkage' : 0,
            'cd_r_linkage_e' : 0,
            'cd_management' : 0,
            'cd_r_management_e' : 0,
            'cd_investigation' : 0,
            'cd_r_investigation_e' : 0,
            'ca_trainer' : 0,
            'ca_r_teaching_e' : 0,
            'ca_tutor_integrating' : 0,
            'ca_r_teaching_e' : 0,
            'ca_tutor_pedagogical' : 0,
            'ca_r_teaching_e' : 0,
            'ca_tutor_practices' : 0,
            'ca_r_teaching_e' : 0,
            'ca_tutor_certification' : 0,
            'ca_r_teaching_e' : 0,
            'ca_linkage' : 0,
            'ca_r_linkage_e' : 0,
            'ca_management' : 0,
            'ca_r_management_e' : 0,
            'ca_investigation' : 0,
            'ca_r_investigation_e' : 0,
            }
            for survey in record.faculty_survey_ids:
                if survey.survey_id.type == 'H' and survey.survey_id.profile == 'S':
                    values['he_trainer'] =survey.score
                    values['he_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'H' and survey.survey_id.profile == 'C':
                    values['he_tutor_integrating'] =survey.score
                    values['he_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'H' and survey.survey_id.profile == 'P':
                    values['he_tutor_pedagogical'] =survey.score
                    values['he_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'H' and survey.survey_id.profile == 'A':
                    values['he_tutor_practices'] =survey.score
                    values['he_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'H' and survey.survey_id.profile == 'T':
                    values['he_tutor_certification'] =survey.score
                    values['he_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'H' and survey.survey_id.profile == 'V':
                    values['he_linkage'] =survey.score
                    values['he_r_linkage_e'] =survey.percent
                if survey.survey_id.type == 'H' and survey.survey_id.profile == 'G':
                    values['he_management'] =survey.score
                    values['he_r_management_e'] =survey.percent
                if survey.survey_id.type == 'H' and survey.survey_id.profile == 'I':
                    values['he_investigation'] =survey.score
                    values['he_r_investigation_e'] =survey.percent
                if survey.survey_id.type == 'A' and survey.survey_id.profile == 'S':
                    values['ae_trainer'] =survey.score
                    values['ae_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'A' and survey.survey_id.profile == 'C':
                    values['ae_tutor_integrating'] =survey.score
                    values['ae_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'A' and survey.survey_id.profile == 'P':
                    values['ae_tutor_pedagogical'] =survey.score
                    values['ae_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'A' and survey.survey_id.profile == 'A':
                    values['ae_tutor_practices'] =survey.score
                    values['ae_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'A' and survey.survey_id.profile == 'T':
                    values['ae_tutor_certification'] =survey.score
                    values['ae_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'A' and survey.survey_id.profile == 'V':
                    values['ae_linkage'] =survey.score
                    values['ae_r_linkage_e'] =survey.percent
                if survey.survey_id.type == 'A' and survey.survey_id.profile == 'G':
                    values['ae_management'] =survey.score
                    values['ae_r_management_e'] =survey.percent
                if survey.survey_id.type == 'A' and survey.survey_id.profile == 'I':
                    values['ae_investigation'] =survey.score
                    values['ae_r_investigation_e'] =survey.percent
                if survey.survey_id.type == 'D' and survey.survey_id.profile == 'S':
                    values['cd_trainer'] =survey.score
                    values['cd_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'D' and survey.survey_id.profile == 'C':
                    values['cd_tutor_integrating'] =survey.score
                    values['cd_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'D' and survey.survey_id.profile == 'P':
                    values['cd_tutor_pedagogical'] =survey.score
                    values['cd_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'D' and survey.survey_id.profile == 'A':
                    values['cd_tutor_practices'] =survey.score
                    values['cd_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'D' and survey.survey_id.profile == 'T':
                    values['cd_tutor_certification'] =survey.score
                    values['cd_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'D' and survey.survey_id.profile == 'V':
                    values['cd_linkage'] =survey.score
                    values['cd_r_linkage_e'] =survey.percent
                if survey.survey_id.type == 'D' and survey.survey_id.profile == 'G':
                    values['cd_management'] =survey.score
                    values['cd_r_management_e'] =survey.percent
                if survey.survey_id.type == 'D' and survey.survey_id.profile == 'I':
                    values['cd_investigation'] =survey.score
                    values['cd_r_investigation_e'] =survey.percent
                if survey.survey_id.type == 'P' and survey.survey_id.profile == 'S':
                    values['ca_trainer'] =survey.score
                    values['ca_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'P' and survey.survey_id.profile == 'C':
                    values['ca_tutor_integrating'] =survey.score
                    values['ca_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'P' and survey.survey_id.profile == 'P':
                    values['ca_tutor_pedagogical'] =survey.score
                    values['ca_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'P' and survey.survey_id.profile == 'A':
                    values['ca_tutor_practices'] =survey.score
                    values['ca_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'P' and survey.survey_id.profile == 'T':
                    values['ca_tutor_certification'] =survey.score
                    values['ca_r_teaching_e'] =survey.percent
                if survey.survey_id.type == 'P' and survey.survey_id.profile == 'V':
                    values['ca_linkage'] =survey.score
                    values['ca_r_linkage_e'] =survey.percent
                if survey.survey_id.type == 'P' and survey.survey_id.profile == 'G':
                    values['ca_management'] =survey.score
                    values['ca_r_management_e'] =survey.percent
                if survey.survey_id.type == 'P' and survey.survey_id.profile == 'I':
                    values['ca_investigation'] =survey.score
                    values['ca_r_investigation_e'] = survey.percent
                record.write(values)

    @api.depends('he_teaching', 'he_r_teaching_e')
    def compute_he_r_teaching(self):
        for record in self:
            record.he_r_teaching = (record.he_teaching * record.he_r_teaching_e) / 5

    @api.depends('he_linkage', 'he_r_linkage_e')
    def compute_he_r_linkage(self):
        for record in self:
            record.he_r_linkage = (record.he_linkage * record.he_r_linkage_e) / 5

    @api.depends('he_management', 'he_r_management_e')
    def compute_he_r_management(self):
        for record in self:
            record.he_r_management = (record.he_management * record.he_r_management_e) / 5

    @api.depends('he_investigation', 'he_r_investigation_e')
    def compute_he_r_investigation(self):
        for record in self:
            record.he_r_investigation = (record.he_investigation * record.he_r_investigation_e) / 5

    @api.depends('ae_teaching', 'ae_r_teaching_e')
    def compute_ae_r_teaching(self):
        for record in self:
            record.ae_r_teaching = (record.ae_teaching * record.ae_r_teaching_e) / 5

    @api.depends('ae_linkage', 'ae_r_linkage_e')
    def compute_ae_r_linkage(self):
        for record in self:
            record.ae_r_linkage = (record.ae_linkage * record.ae_r_linkage_e) / 5

    @api.depends('ae_management', 'ae_r_management_e')
    def compute_ae_r_management(self):
        for record in self:
            record.ae_r_management = (record.ae_management * record.ae_r_management_e) / 5

    @api.depends('ae_investigation', 'ae_r_investigation_e')
    def compute_ae_r_investigation(self):
        for record in self:
            record.ae_r_investigation = (record.ae_investigation * record.ae_r_investigation_e) / 5

    @api.depends('cd_teaching', 'cd_r_teaching_e')
    def compute_cd_r_teaching(self):
        for record in self:
            record.cd_r_teaching = (record.cd_teaching * record.cd_r_teaching_e) / 5

    @api.depends('cd_linkage', 'cd_r_linkage_e')
    def compute_cd_r_linkage(self):
        for record in self:
            record.cd_r_linkage = (record.cd_linkage * record.cd_r_linkage_e) / 5

    @api.depends('cd_management', 'cd_r_management_e')
    def compute_cd_r_management(self):
        for record in self:
            record.cd_r_management = (record.cd_management * record.cd_r_management_e) / 5

    @api.depends('cd_investigation', 'cd_r_investigation_e')
    def compute_cd_r_investigation(self):
        for record in self:
            record.cd_r_investigation = (record.cd_investigation * record.cd_r_investigation_e) / 5

    @api.depends('ca_teaching', 'ca_r_teaching_e')
    def compute_ca_r_teaching(self):
        for record in self:
            record.ca_r_teaching = (record.ca_teaching * record.ca_r_teaching_e) / 5

    @api.depends('ca_linkage', 'ca_r_linkage_e')
    def compute_ca_r_linkage(self):
        for record in self:
            record.ca_r_linkage = (record.ca_linkage * record.ca_r_linkage_e) / 5

    @api.depends('ca_management', 'ca_r_management_e')
    def compute_ca_r_management(self):
        for record in self:
            record.ca_r_management = (record.ca_management * record.ca_r_management_e) / 5

    @api.depends('ca_investigation', 'ca_r_investigation_e')
    def compute_ca_r_investigation(self):
        for record in self:
            record.ca_r_investigation = (record.ca_investigation * record.ca_r_investigation_e) / 5

    def validate(self):
        self.state = 'validated'

    @api.model
    def open_faculty(self):
        action_faculty = self.env.ref('educational_survey.edu_my_survey_faculty_act')
        result = action_faculty.read()[0]
        faculty = self.search([('faculty_id.user_id.id', '=', self.env.user.id)])
        if len(faculty) == 1:
            result['res_id'] = faculty.ids[0]
        return result


#


class EduSurveyFacultySurvey(models.Model):
    _name = 'edu.survey.faculty.survey'
    _description = 'Resultado de encuesta docentes'

    survey_faculty_id = fields.Many2one('edu.survey.faculty')
    survey_id = fields.Many2one('edu.survey', "Encuentas")
    percent = fields.Integer('Porcentaje', related='survey_id.percent')
    question_ids = fields.One2many('edu.survey.faculty.question',
                                   inverse_name="survey_faculty_survey_id")
    score_percent = fields.Float('Calificación (%)', compute="_compute_score")
    score = fields.Float('Calificación (5)', compute="_compute_score")

    @api.one
    @api.depends('question_ids')
    def _compute_score(self):
        if self.question_ids and self.percent:
            question_score = 0.0
            for question in self.question_ids:
                question_score += question.score
            self.score = question_score / len(self.question_ids)
            self.score_percent = (self.score * self.percent) / 5


class EduSurveyFacultyQuestion(models.Model):
    _name = 'edu.survey.faculty.question'
    _description = 'Resultado por pregunta de encuenta docentes'

    name = fields.Char("Nombre", required=True)
    survey_faculty_survey_id = fields.Many2one('edu.survey.faculty.survey',
                                               "Encuesta",
                                               ondelete='cascade')
    score = fields.Float('Calificación')
