from datetime import datetime

from odoo import _, models, fields, api, exceptions
from odoo.exceptions import UserError, ValidationError

import numpy as np

WEIGHTS = [1.0, 0.75, 0.50, 0.25, 0]


class EduSurveyEval(models.Model):
    _name = 'edu.survey.eval'
    _description = 'Encuestas Evaluadas'

    name = fields.Char(compute='_compute_name')
    code = fields.Char(compute='_compute_code', store=True, name="Código")
    state = fields.Selection(
        [('draft', 'Borrador'), ('validated', 'Validado'), ('not_completed', 'No Realizado'),
         ('finished', 'Terminado'),('lated','Atrasado') ], default="draft",
        string='Estado')
    period_id = fields.Many2one('edu.period', string='Periodo', required=True)
    question_ids = fields.One2many('edu.survey.eval.question',
                                   inverse_name="survey_eval_id")

    faculty_id = fields.Many2one('edu.faculty', string="Docente evaluado", required=True)
    student_id = fields.Many2one('edu.student', string="Estudiante")
    user_id = fields.Many2one('res.users', string="Evaluador",
                              help="Usuario que llena la encuesta",
                              ondelete="restrict")
    survey_id = fields.Many2one('edu.survey', string='Plantilla de Encuesta', required=True)
    subject_parallel_id = fields.Many2one('edu.subject.parallel',
                                          string='Materia/Paralelo')
    parallel_id = fields.Many2one('edu.parallel',
                                  related='subject_parallel_id.parallel_id',
                                  store=True)

    career_id = fields.Many2one('edu.career',
                                related='faculty_id.career_id',
                                store=True)

    score = fields.Float(u'Calificación')

    observation = fields.Text('Observación')

    _sql_constraints = [
        ('user_survey_faculty_subject_parallel_period_unique',
         'unique (user_id,survey_id,faculty_id,subject_parallel_id,period_id)',
         'Encuesta duplicada')
        ,
        ('user_code_unique',
         'unique (code)',
         'Esta encuesta duplicada')
    ]

    @api.multi
    @api.depends('faculty_id', 'student_id', 'survey_id',
                 'subject_parallel_id')
    def _compute_name(self):
        for record in self:
            if record.faculty_id and record.student_id and record.survey_id:
                record.name = record.faculty_id.name + " - " + record.student_id.name + " - " + record.survey_id.name
            elif record.faculty_id and record.survey_id and record.subject_parallel_id:
                record.name = record.faculty_id.name + " - " + record.survey_id.name + " - " + record.subject_parallel_id.name
            else:
                if record.faculty_id and record.survey_id:
                    record.name = record.faculty_id.name + " - " + record.survey_id.name

    @api.multi
    @api.depends('user_id', 'survey_id', 'faculty_id',
                 'subject_parallel_id', 'period_id')
    def _compute_code(self):
        for record in self:
            if record.user_id and record.survey_id and record.faculty_id and record.subject_parallel_id and record.period_id:
                record.code = str(record.user_id.id) + "-" + str(record.survey_id.id) + "-" + str(
                    record.faculty_id.id) + "-" + str(record.subject_parallel_id.id) + "-" + str(record.period_id.id)
            else:
                record.code = str(record.user_id.id) + "-" + str(record.survey_id.id) + "-" + str(
                    record.faculty_id.id) + "-N-" + str(record.period_id.id)

    @api.onchange('survey_id')
    def _onchange_survey_id(self):
        if self.survey_id:
            self.question_ids = None
            questions = []
            for question in self.survey_id.question_ids:
                questions.append({
                    'name': question.name
                })
            self.question_ids = questions

    def validate(self):
        n5, n4, n3, n2, n1 = [0, 0, 0, 0, 0]
        for question in self.question_ids:
            if not question.score:
                raise UserError("Debes Calificar todas las preguntas")
            if question.score == 5:
                n5 += 1
            if question.score == 4:
                n4 += 1
            if question.score == 3:
                n3 += 1
            if question.score == 2:
                n2 += 1
            if question.score == 1:
                n1 += 1
        values = [n5, n4, n3, n2, n1]
        self.score = np.average(WEIGHTS, weights=values) * 100
        self.state = 'validated'

        # Validacion de fecha limite para evaluaciones
        if self.survey_id.block_date:
            date = datetime.now().date()
            if self.survey_id.block_date < date:
                raise ValidationError(_('Ya pasó la fecha limite de esta encuesta.'))

    def to_draft(self):
        self.state = 'draft'

    def calculate_score(self, period):
        survey_ids = self.search(
            [('period_id', '=', period), ('state', '=', 'validated')])
        for survey in survey_ids:
            n5, n4, n3, n2, n1 = [0, 0, 0, 0, 0]
            for question in survey.question_ids:
                if question.score == 5:
                    n5 += 1
                if question.score == 4:
                    n4 += 1
                if question.score == 3:
                    n3 += 1
                if question.score == 2:
                    n2 += 1
                if question.score == 1:
                    n1 += 1
            values = [n5, n4, n3, n2, n1]
            score = np.average(WEIGHTS, weights=values) * 100
            survey.write({'score': score})


class EduSurveyEvalQuestion(models.Model):
    _name = 'edu.survey.eval.question'
    _description = 'Survey eval question'

    sequence = fields.Integer('Secuencia')
    name = fields.Char(string="Nombre", required=False)
    question_id =  fields.Many2one('edu.survey.question', string='Pregunta')
    type_id = fields.Many2one('edu.survey.type')
    survey_eval_id = fields.Many2one('edu.survey.eval', string="Encuesta",
                                     ondelete='cascade')
    score = fields.Selection([(5, '5 - Total Acuerdo'),
                              (4, '4 - Acuerdo'),
                              (3, '3 - Medianamente Acuerdo'),
                              (2, '2 - Desacuerdo'),
                              (1, '1 - Total Desacuerdo')],
                             string='Seleccione Calificación')
