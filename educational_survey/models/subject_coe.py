# -*- coding: utf-8 -*-

from odoo import _, models, fields, api

DAY_WEEK = [('monday','Lunes'),
            ('tuesday', 'Martes'),
            ('wednesday', 'Miercoles'),
            ('thursday', 'Jueves'),
            ('friday', 'Viernes'),
            ('saturday', 'Sabado'),
            ('sunday', 'Domingo'),]
class EduSubjectCoe(models.Model):
    _name = 'edu.subject.coe'
    _description = "Materias coevaluación"

    name = fields.Char(compute="_compute_name")
    period_id = fields.Many2one('edu.period', string='Periodo', required=True)
    faculty_id = fields.Many2one('edu.faculty',string='Docente', required=True)
    subject_id = fields.Many2one('edu.subject', string="Materia", required=True)
    day_week = fields.Selection(DAY_WEEK, string="Día", required=True)
    date = fields.Date('Fecha', required=True)
    time = fields.Char('Hora', required=True)
    pair_faculty_1 = fields.Many2one('edu.faculty', string="Evaluador 1", required=True)
    pair_faculty_2 = fields.Many2one('edu.faculty', string="Evaluador 2", required=True)
    level = fields.Integer(string="Nivel", related='subject_id.level', required=True)
    literal = fields.Char(string="Literal", required=True, size=1,
                          help='Ejemplos A,B,C,D')

    _sql_constraints = [('code_unique', 'unique(faculty_id,period_id)', 'Este docente ya tiene un par académico para este periodo')]

    @api.multi
    @api.depends('faculty_id','period_id')
    def _compute_name(self):
        for record in self:
            if record.faculty_id and record.period_id:
                record.name = record.faculty_id.name + str(record.period_id.name)