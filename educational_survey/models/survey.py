# -*- coding: utf-8 -*-
#
# PERFIL DOCENTE HETERO EVALUACIÓN AUTOEVALUACIÓN COEVALUACIÓN DIRECTIVOS COEVALUACIÓN PARES TOTAL
# DOCENCIA              40%             20%             20%             20%             100%
# VINCULACIÓN           10%             20%             40%             30%             100%
# GESTIÓN ACADÉMICA     10%             20%             40%             30%             100%
# INVESTIGACIÓN         10%             20%             35%             35%             100%


from odoo import _, models, fields, api

TYPE = [('H', 'Hetero'),
        ('A', 'Autoevaluación'),
        ('D', 'Co-Directivos'),
        ('P', 'Co-Pares')]

PROFILE = [('S', 'Formación'),
           ('C', 'Cátedra Reto'),
           ('P', 'Tutor Pedagógico'),
           ('A', 'Tutor de Prácticas'),
           ('T', 'Tutor de Titulación'),
           ('V', 'Vinculación'),
           ('G', 'Gestión Académica'),
           ('I', 'Investigación')]


class EduSurvey(models.Model):
    _name = 'edu.survey'
    _description = 'Survey'

    name = fields.Char("Nombre")
    type_id = fields.Many2one('edu.survey.type', string="Tipos")
    question_ids = fields.Many2many('edu.survey.question', required=True)
    period_survey_ids = fields.One2many('edu.period.survey',
                                        inverse_name='survey_id')
    type = fields.Selection(TYPE, string='Tipo')
    profile = fields.Selection(PROFILE, string='Perfil Docente')
    percent = fields.Integer('Porcentaje')
    block_date = fields.Date('Fecha de Bloqueo')
