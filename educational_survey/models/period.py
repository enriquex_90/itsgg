# -*- coding: utf-8 -*-

from odoo import models, fields, api


class EduPeriod(models.Model):
    _inherit = 'edu.period'

    survey_ids = fields.One2many('edu.period.survey', string='Encuestas',
                                 inverse_name="period_id")

    def survey_result(self):
        faculty_ids = self.env['edu.faculty'].search([])
        # faculty_ids = self.env['edu.faculty'].browse(439)
        SURVEY_EVAL = self.env['edu.survey.eval']
        SURVEY_FACULTY = self.env['edu.survey.faculty']
        survey_faculty_ids = []
        for faculty in faculty_ids:
            faculty_survey = []
            for survey in self.survey_ids:
                domain = [('survey_id', '=', survey.survey_id.id),
                     ('faculty_id', '=', faculty.id), ('period_id', '=', self.id)]
                survey_eval_ids = SURVEY_EVAL.search(domain +[
                     ('state', '=', 'validated')])
                survey_eval_draft_ids = SURVEY_EVAL.search(domain +
                     [('state', '=', 'draft')])

                questions = []
                if survey_eval_ids:
                    for questions_index, question in enumerate(survey.survey_id.question_ids):
                        question_score = 0
                        for survey_eval in survey_eval_ids:
                            try:
                                question_score += survey_eval.question_ids[questions_index].score
                            except IndexError as e:
                                print("error {}".format(survey_eval.id))
                        questions.append(
                            (0, 0, {'name': question.name, 'score': question_score / len(survey_eval_ids)}))
                    faculty_survey.append(
                        (0, 0, {'survey_id': survey.survey_id.id,
                                'question_ids': questions}))
                else:
                    if survey_eval_draft_ids:
                        for questions_index, question in enumerate(survey.survey_id.question_ids):
                            questions.append(
                                (0, 0, {'name': question.name, 'score': 5}))
                        faculty_survey.append(
                            (0, 0, {'survey_id': survey.survey_id.id,
                                    'question_ids': questions}))


            survey_faculty_ids.append(SURVEY_FACULTY.create({'period_id': self.id,
                                   'faculty_id': faculty.id,
                                   'faculty_survey_ids': faculty_survey}))

        survey_faculties = SURVEY_FACULTY.browse(survey_eval_ids)
        survey_faculties.calculate_hours()
        survey_faculties.calculate()

    def finalize_survey(self):
        survey_eval_ids = self.env['edu.survey.eval'].search([('period_id', '=', self.id)])
        survey_faculty_ids = self.env['edu.survey.faculty'].search([('period_id', '=', self.id)])
        for survey_eval in survey_eval_ids:
            if survey_eval.state == 'draft':
                survey_eval.state = 'not_completed'
            if survey_eval.state == 'validated':
                survey_eval.state = 'finished'
        for survey_faculty in survey_faculty_ids:
            if survey_faculty.state == 'draft':
                survey_faculty.state = 'validated'


class EduPeriodSurvey(models.Model):
    _name = 'edu.period.survey'
    _description = 'Periodo Encuesta'

    sequence = fields.Integer('Secuencia')
    period_id = fields.Many2one('edu.period', string="Periodo", required=True)
    survey_id = fields.Many2one('edu.survey', string='Encuesta', required=True)
    generated_survey = fields.Integer('Encuestas Generadas',
                                      compute="_compute_generated_survey")

    _sql_constraints = [
        ('name_unique', 'unique (period_id,survey_id)',
         u'Duplicado - Evaluacion duplicada para este periodo')
    ]

    @api.multi
    def _compute_generated_survey(self):
        SURVEY_EVAL = self.env['edu.survey.eval']
        for record in self:
            record.generated_survey = SURVEY_EVAL.search_count(
                [('period_id', '=', record.period_id.id),
                 ('survey_id', '=', record.survey_id.id)])

    def create_survey(self):
        subject_parallel_ids = self.env['edu.subject.parallel'].search(
            [('parallel_id.period_id', '=', self.period_id.id)])
        SURVEY_EVAL = self.env['edu.survey.eval']
        # Hetero Formacion
        if self.survey_id.type == 'H' and self.survey_id.profile == 'S':
            for subject_parallel in subject_parallel_ids:
                if subject_parallel.faculty_id:
                    for student in subject_parallel.student_ids:
                        if student.user_id.id:
                            self._create_survey(user_id=student.user_id.id,
                                                faculty_id=subject_parallel.faculty_id.id,
                                                model=SURVEY_EVAL, subject_parallel_id=subject_parallel.id,
                                                student_id=student.id)
        # Auto Formacion
        if self.survey_id.type == 'A' and self.survey_id.profile == 'S':
            for subject_parallel in subject_parallel_ids:
                if subject_parallel.faculty_id.user_id:
                    self._create_survey(user_id=subject_parallel.faculty_id.user_id.id,
                                        faculty_id=subject_parallel.faculty_id.id,
                                        model=SURVEY_EVAL, subject_parallel_id=subject_parallel.id)
        # Directivos Formacion
        if self.survey_id.type == 'D' and self.survey_id.profile == 'S':
            faculty_ids = self.env['edu.faculty'].search([])
            for faculty in faculty_ids:
                if faculty.career_id.coordinator.user_id:
                    self._create_survey(user_id=faculty.career_id.coordinator.user_id.id,
                                        faculty_id=faculty.id,
                                        model=SURVEY_EVAL)
        # Pares Formacion
        if self.survey_id.type == 'P' and self.survey_id.profile == 'S':
            subject_coe_ids = self.env['edu.subject.coe'].search(
                [('period_id', '=', self.period_id.id)])
            for subject_coe in subject_coe_ids:
                if subject_coe.pair_faculty_1.user_id:
                    self._create_survey(user_id=subject_coe.pair_faculty_1.user_id.id,
                                        faculty_id=subject_coe.faculty_id.id,
                                        model=SURVEY_EVAL)
                if subject_coe.pair_faculty_2.user_id:
                    self._create_survey(user_id=subject_coe.pair_faculty_2.user_id.id,
                                        faculty_id=subject_coe.faculty_id.id,
                                        model=SURVEY_EVAL)
        # Hetero Catedra Reto
        if self.survey_id.type == 'H' and self.survey_id.profile == 'C':
            for subject_parallel in subject_parallel_ids:
                if subject_parallel.faculty_id and subject_parallel.subject_id.integrating:
                    for student in subject_parallel.student_ids:
                        self._create_survey(user_id=student.user_id.id,
                                            faculty_id=subject_parallel.faculty_id.id,
                                            model=SURVEY_EVAL, subject_parallel_id=subject_parallel.id,
                                            student_id=student.id)
        # Auto Catedra Reto
        if self.survey_id.type == 'A' and self.survey_id.profile == 'C':
            for subject_parallel in subject_parallel_ids:
                if subject_parallel.faculty_id and subject_parallel.subject_id.integrating:
                    self._create_survey(user_id=subject_parallel.faculty_id.user_id.id,
                                        faculty_id=subject_parallel.faculty_id.id,
                                        model=SURVEY_EVAL, subject_parallel_id=subject_parallel.id)
        # Directivos Catedra Reto
        if self.survey_id.type == 'D' and self.survey_id.profile == 'C':
            faculty_ids = self.env['edu.faculty'].search([])
            for faculty in faculty_ids:
                subject_parallel = self.env['edu.subject.parallel'].search(
                    [('parallel_id.period_id', '=', self.period_id.id), ('faculty_id', '=', faculty.id),
                     ('subject_id.integrating', '=', True)], limit=1)
                if subject_parallel and faculty.career_id.coordinator_id.user_id:
                    self._create_survey(user_id=subject_parallel.faculty_id.career_id.coordinator_id.user_id.id,
                                        faculty_id=subject_parallel.faculty_id.id,
                                        model=SURVEY_EVAL, subject_parallel_id=subject_parallel.id)
        # Pares Catedra Reto
        if self.survey_id.type == 'P' and self.survey_id.profile == 'C':
            faculty_ids = self.env['edu.faculty'].search([])
            for faculty in faculty_ids:
                subject_parallel = self.env['edu.subject.parallel'].search(
                    [('parallel_id.period_id', '=', self.period_id.id), ('faculty_id', '=', faculty.id),
                     ('subject_id.integrating', '=', True)], limit=1)
                if subject_parallel:
                    subject_coe = self.env['edu.subject.coe'].search(
                        [('period_id', '=', self.period_id.id), ('faculty_id', '=', faculty.id)], limit=1)
                    if subject_coe:
                        if subject_coe.pair_faculty_1.user_id:
                            self._create_survey(user_id=subject_coe.pair_faculty_1.user_id.id,
                                                faculty_id=subject_coe.faculty_id.id,
                                                model=SURVEY_EVAL)
                        if subject_coe.pair_faculty_2.user_id:
                            self._create_survey(user_id=subject_coe.pair_faculty_2.user_id.id,
                                                faculty_id=subject_coe.faculty_id.id,
                                                model=SURVEY_EVAL)
        # Hetero Pedagogico
        if self.survey_id.type == 'H' and self.survey_id.profile == 'P':
            pedagogical_ids = self.env['edu.pedagogical'].search([('period_id', '=', self.period_id.id)])
            for pedagogical in pedagogical_ids:
                for student in pedagogical.student_ids:
                    self._create_survey(user_id=student.user_id.id,
                                        faculty_id=pedagogical.faculty_id.id,
                                        model=SURVEY_EVAL,
                                        student_id=student.id)
        # Auto Pedagogico
        if self.survey_id.type == 'A' and self.survey_id.profile == 'P':
            faculty_ids = self.env['edu.faculty'].search([('is_tutor_pedagogical','=',True)])
            for faculty in faculty_ids:
                self._create_survey(user_id=faculty.user_id.id,
                                    faculty_id=faculty.id,
                                    model=SURVEY_EVAL)
        # Directivos Pedagogico
        if self.survey_id.type == 'D' and self.survey_id.profile == 'P':
            faculty_ids = self.env['edu.faculty'].search([('is_tutor_pedagogical', '=', True)])
            for faculty in faculty_ids:
                self._create_survey(user_id=faculty.career_id.coordinator_id.user_id.id,
                                    faculty_id=faculty.id,
                                    model=SURVEY_EVAL)
        # Pares Pedagogico
        if self.survey_id.type == 'P' and self.survey_id.profile == 'P':
            faculty_ids = self.env['edu.faculty'].search([('is_tutor_pedagogical', '=', True)])
            for faculty in faculty_ids:
                subject_coe = self.env['edu.subject.coe'].search(
                    [('period_id', '=', self.period_id.id), ('faculty_id', '=', faculty.id)], limit=1)
                if subject_coe:
                    if subject_coe.pair_faculty_1.user_id:
                        self._create_survey(user_id=subject_coe.pair_faculty_1.user_id.id,
                                            faculty_id=subject_coe.faculty_id.id,
                                            model=SURVEY_EVAL)
                    if subject_coe.pair_faculty_2.user_id:
                        self._create_survey(user_id=subject_coe.pair_faculty_2.user_id.id,
                                            faculty_id=subject_coe.faculty_id.id,
                                            model=SURVEY_EVAL)
        # Hetero Practicas
        if self.survey_id.type == 'H' and self.survey_id.profile == 'A':
            practices_ids = self.env['edu.practices'].search([('period_id', '=', self.period_id.id)])
            for practice in practices_ids:
                for student in practice.student_ids:
                    self._create_survey(user_id=student.user_id.id,
                                        faculty_id=practice.faculty_id.id,
                                        model=SURVEY_EVAL,
                                        student_id=student.id)
        # Auto Practicas
        if self.survey_id.type == 'A' and self.survey_id.profile == 'A':
            faculty_ids = self.env['edu.faculty'].search([('is_tutor_practices','=',True)])
            for faculty in faculty_ids:
                self._create_survey(user_id=faculty.user_id.id,
                                    faculty_id=faculty.id,
                                    model=SURVEY_EVAL)
        # Directivos Practicas
        if self.survey_id.type == 'D' and self.survey_id.profile == 'A':
            faculty_ids = self.env['edu.faculty'].search([('is_tutor_practices', '=', True)])
            for faculty in faculty_ids:
                self._create_survey(user_id=faculty.career_id.coordinator_id.user_id.id,
                                    faculty_id=faculty.id,
                                    model=SURVEY_EVAL)
        # Pares Practicas
        if self.survey_id.type == 'P' and self.survey_id.profile == 'A':
            faculty_ids = self.env['edu.faculty'].search([('is_tutor_practices', '=', True)])
            for faculty in faculty_ids:
                subject_coe = self.env['edu.subject.coe'].search(
                    [('period_id', '=', self.period_id.id), ('faculty_id', '=', faculty.id)], limit=1)
                if subject_coe:
                    if subject_coe.pair_faculty_1.user_id:
                        self._create_survey(user_id=subject_coe.pair_faculty_1.user_id.id,
                                            faculty_id=subject_coe.faculty_id.id,
                                            model=SURVEY_EVAL)
                    if subject_coe.pair_faculty_2.user_id:
                        self._create_survey(user_id=subject_coe.pair_faculty_2.user_id.id,
                                            faculty_id=subject_coe.faculty_id.id,
                                            model=SURVEY_EVAL)
        # Hetero Titulacion
        if self.survey_id.type == 'H' and self.survey_id.profile == 'T':
            certification_ids = self.env['edu.certification'].search([('period_id', '=', self.period_id.id)])
            for certification in certification_ids:
                for student in certification.student_ids:
                    self._create_survey(user_id=student.user_id.id,
                                        faculty_id=certification.faculty_id.id,
                                        model=SURVEY_EVAL,
                                        student_id=student.id)
        # Auto Titulacion
        if self.survey_id.type == 'A' and self.survey_id.profile == 'T':
            faculty_ids = self.env['edu.faculty'].search([('is_tutor_certification','=',True)])
            for faculty in faculty_ids:
                self._create_survey(user_id=faculty.user_id.id,
                                    faculty_id=faculty.id,
                                    model=SURVEY_EVAL)
        # Directivos Titulacion
        if self.survey_id.type == 'D' and self.survey_id.profile == 'T':
            faculty_ids = self.env['edu.faculty'].search([('is_tutor_certification', '=', True)])
            for faculty in faculty_ids:
                self._create_survey(user_id=faculty.career_id.coordinator_id.user_id.id,
                                    faculty_id=faculty.id,
                                    model=SURVEY_EVAL)
        # Pares Titulacion
        if self.survey_id.type == 'P' and self.survey_id.profile == 'T':
            faculty_ids = self.env['edu.faculty'].search([('is_tutor_certification', '=', True)])
            for faculty in faculty_ids:
                subject_coe = self.env['edu.subject.coe'].search(
                    [('period_id', '=', self.period_id.id), ('faculty_id', '=', faculty.id)], limit=1)
                if subject_coe:
                    if subject_coe.pair_faculty_1.user_id:
                        self._create_survey(user_id=subject_coe.pair_faculty_1.user_id.id,
                                            faculty_id=subject_coe.faculty_id.id,
                                            model=SURVEY_EVAL)
                    if subject_coe.pair_faculty_2.user_id:
                        self._create_survey(user_id=subject_coe.pair_faculty_2.user_id.id,
                                            faculty_id=subject_coe.faculty_id.id,
                                            model=SURVEY_EVAL)
        # Hetero Vinculacion
        if self.survey_id.type == 'H' and self.survey_id.profile == 'V':
            link_ids = self.env['edu.link'].search([('period_id', '=', self.period_id.id)])
            for link in link_ids:
                for student in link.student_ids:
                    self._create_survey(user_id=student.user_id.id,
                                        faculty_id=link.faculty_id.id,
                                        model=SURVEY_EVAL,
                                        student_id=student.id)
        # Auto Vinculacion
        if self.survey_id.type == 'A' and self.survey_id.profile == 'V':
            faculty_ids = self.env['edu.faculty'].search([('is_link','=',True)])
            for faculty in faculty_ids:
                self._create_survey(user_id=faculty.user_id.id,
                                    faculty_id=faculty.id,
                                    model=SURVEY_EVAL)
        # Directivos Vinculacion
        if self.survey_id.type == 'D' and self.survey_id.profile == 'V':
            faculty_ids = self.env['edu.faculty'].search([('is_link', '=', True)])
            for faculty in faculty_ids:
                self._create_survey(user_id=faculty.career_id.coordinator_id.user_id.id,
                                    faculty_id=faculty.id,
                                    model=SURVEY_EVAL)
        # Pares Vinculacion
        if self.survey_id.type == 'P' and self.survey_id.profile == 'V':
            faculty_ids = self.env['edu.faculty'].search([('is_link', '=', True)])
            for faculty in faculty_ids:
                subject_coe = self.env['edu.subject.coe'].search(
                    [('period_id', '=', self.period_id.id), ('faculty_id', '=', faculty.id)], limit=1)
                if subject_coe:
                    if subject_coe.pair_faculty_1.user_id:
                        self._create_survey(user_id=subject_coe.pair_faculty_1.user_id.id,
                                            faculty_id=subject_coe.faculty_id.id,
                                            model=SURVEY_EVAL)
                    if subject_coe.pair_faculty_2.user_id:
                        self._create_survey(user_id=subject_coe.pair_faculty_2.user_id.id,
                                            faculty_id=subject_coe.faculty_id.id,
                                            model=SURVEY_EVAL)
        # Hetero Gestion
        if self.survey_id.type == 'H' and self.survey_id.profile == 'G':
            manager_ids = self.env['edu.manager'].search([('period_id', '=', self.period_id.id)])
            for manager in manager_ids:
                for student in manager.student_ids:
                    self._create_survey(user_id=student.user_id.id,
                                        faculty_id=manager.faculty_id.id,
                                        model=SURVEY_EVAL,
                                        student_id=student.id)
        # Auto Gestion
        if self.survey_id.type == 'A' and self.survey_id.profile == 'G':
            faculty_ids = self.env['edu.faculty'].search([('is_manager','=',True)])
            for faculty in faculty_ids:
                self._create_survey(user_id=faculty.user_id.id,
                                    faculty_id=faculty.id,
                                    model=SURVEY_EVAL)
        # Directivos Gestion
        if self.survey_id.type == 'D' and self.survey_id.profile == 'G':
            faculty_ids = self.env['edu.faculty'].search([('is_manager', '=', True)])
            for faculty in faculty_ids:
                self._create_survey(user_id=faculty.career_id.coordinator_id.user_id.id,
                                    faculty_id=faculty.id,
                                    model=SURVEY_EVAL)
        # Pares Gestion
        if self.survey_id.type == 'P' and self.survey_id.profile == 'G':
            faculty_ids = self.env['edu.faculty'].search([('is_manager', '=', True)])
            for faculty in faculty_ids:
                subject_coe = self.env['edu.subject.coe'].search(
                    [('period_id', '=', self.period_id.id), ('faculty_id', '=', faculty.id)], limit=1)
                if subject_coe:
                    if subject_coe.pair_faculty_1.user_id:
                        self._create_survey(user_id=subject_coe.pair_faculty_1.user_id.id,
                                            faculty_id=subject_coe.faculty_id.id,
                                            model=SURVEY_EVAL)
                    if subject_coe.pair_faculty_2.user_id:
                        self._create_survey(user_id=subject_coe.pair_faculty_2.user_id.id,
                                            faculty_id=subject_coe.faculty_id.id,
                                            model=SURVEY_EVAL)
        # Hetero Investigacion
        if self.survey_id.type == 'H' and self.survey_id.profile == 'I':
            researcher_ids = self.env['edu.researcher'].search([('period_id', '=', self.period_id.id)])
            for researcher in researcher_ids:
                for student in researcher.student_ids:
                    self._create_survey(user_id=student.user_id.id,
                                        faculty_id=researcher.faculty_id.id,
                                        model=SURVEY_EVAL,
                                        student_id=student.id)
        # Auto Investigacion
        if self.survey_id.type == 'A' and self.survey_id.profile == 'I':
            faculty_ids = self.env['edu.faculty'].search([('is_researcher','=',True)])
            for faculty in faculty_ids:
                self._create_survey(user_id=faculty.user_id.id,
                                    faculty_id=faculty.id,
                                    model=SURVEY_EVAL)
        # Directivos Investigacion
        if self.survey_id.type == 'D' and self.survey_id.profile == 'I':
            faculty_ids = self.env['edu.faculty'].search([('is_researcher', '=', True)])
            for faculty in faculty_ids:
                self._create_survey(user_id=faculty.career_id.coordinator_id.user_id.id,
                                    faculty_id=faculty.id,
                                    model=SURVEY_EVAL)
        # Pares Investigacion
        if self.survey_id.type == 'P' and self.survey_id.profile == 'I':
            faculty_ids = self.env['edu.faculty'].search([('is_researcher', '=', True)])
            for faculty in faculty_ids:
                subject_coe = self.env['edu.subject.coe'].search(
                    [('period_id', '=', self.period_id.id), ('faculty_id', '=', faculty.id)], limit=1)
                if subject_coe:
                    if subject_coe.pair_faculty_1.user_id:
                        self._create_survey(user_id=subject_coe.pair_faculty_1.user_id.id,
                                            faculty_id=subject_coe.faculty_id.id,
                                            model=SURVEY_EVAL)
                    if subject_coe.pair_faculty_2.user_id:
                        self._create_survey(user_id=subject_coe.pair_faculty_2.user_id.id,
                                            faculty_id=subject_coe.faculty_id.id,
                                            model=SURVEY_EVAL)

    def _create_survey(self, user_id, faculty_id, model, student_id=None, subject_parallel_id=None):
        if not self._validate_duplicate(user_id=user_id, faculty_id=faculty_id, model=model,
                                        student_id=student_id, subject_parallel_id=subject_parallel_id):
            values = {
                'period_id': self.period_id.id,
                'user_id': user_id,
                'faculty_id': faculty_id,
                'survey_id': self.survey_id.id
            }
            if student_id:
                values['student_id'] = student_id
            if subject_parallel_id:
                values['subject_parallel_id'] = subject_parallel_id
            values['question_ids'] = []
            for question in self.survey_id.question_ids:
                values['question_ids'].append(
                    (0, 0, {'question_id': question.id}))
            model.create(values)

    def _validate_duplicate(self, user_id, faculty_id, model, student_id=None, subject_parallel_id=None):
        domain = [('period_id', '=', self.period_id.id),
                  ('survey_id', '=', self.survey_id.id),
                  ('user_id', '=', user_id),
                  ('faculty_id', '=', faculty_id),
                  ]
        if student_id:
            domain.append(('student_id', '=', student_id))
        if subject_parallel_id:
            domain.append(('subject_parallel_id', '=', subject_parallel_id))
        return model.search_count(domain)
