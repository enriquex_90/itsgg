# -*- coding: utf-8 -*-

from odoo import _, models, fields, api


class EduResearcher(models.Model):
    _inherit = 'edu.researcher'

    pair_faculty_1 = fields.Many2one('edu.faculty', string="Evaluador 1")
    pair_faculty_2 = fields.Many2one('edu.faculty', string="Evaluador 2")