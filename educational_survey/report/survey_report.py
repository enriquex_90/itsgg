import icu

from odoo import models


class SurveyXlsx(models.AbstractModel):
    _name = 'report.educational_survey.result'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, doc):
        areas = {'software': 'INFORMÁTICA',
                 'design': 'DISEÑO',
                 'marketing': 'MARKETING',
                 'offset': 'OFFSET'}

        # formatos
        bold_border_format = workbook.add_format(
            {'bold': True, 'border': 1})
        faculty_format = workbook.add_format(
            {'border': 1, 'valign': 'vcenter'})
        criteria_format = workbook.add_format(
            {'bold': True, 'border': 1, 'font_size': 9})

        values_format = workbook.add_format(
            {'border': 1, 'font_size': 9, 'align': 'center'})

        percentage_format = workbook.add_format({"num_format": "0.00%", 'border': 1, 'font_size': 9, 'align': 'center'})

        departments = self.env['edu.department'].search([])
        for department in departments:
            sheet = workbook.add_worksheet(department.name)
            faculty_ids = self.env['edu.survey.faculty'].search(
                [('department_id', '=', department.id),('period_id','=',doc.id)])

            sheet.set_column(0, 0, 38)
            sheet.set_column(1, 1, 25)

            sheet.merge_range('G1:K1', "COE-EVALUACIÓN", bold_border_format)
            sheet.write(1, 0, "APELLIDOS Y NOMBRES", bold_border_format)
            sheet.write(1, 1, "CRITERIOS", bold_border_format)
            sheet.merge_range('C2:D2', "HETERO-EVALUACIÓN", bold_border_format)
            sheet.merge_range('E2:F2', "AUTO-EVALUACIÓN", bold_border_format)
            sheet.merge_range('G2:H2', "EVALUACIÓN DIRECTIVOS",
                              bold_border_format)
            sheet.merge_range('I2:J2', "PAR ACÁDEMICOS", bold_border_format)
            sheet.write('K2', "TOTAL", bold_border_format)
            sheet.write('M2', "%EVALUACIÓN", bold_border_format)

            collator = icu.Collator.createInstance(icu.Locale('es_EC.UTF-8'))
            faculty_ids = sorted(faculty_ids,
                                 key=lambda k: collator.getSortKey(k['name']))
            index = 2

            for counter, faculty in enumerate(faculty_ids, 2):
                sheet.merge_range(index, 0, index + 3, 0,
                                  faculty.faculty_id.full_name, faculty_format)
                sheet.write(index, 1, "DOCENCIA", criteria_format)
                sheet.write(index + 1, 1, "VINCULACIÓN / PPP", criteria_format)
                sheet.write(index + 2, 1, "GESTIÓN ACADÉMICA", criteria_format)
                sheet.write(index + 3, 1, "INVESTIGACIÓN/TITULACIÓN", criteria_format)

                sheet.write(index, 2,faculty['he_r_teaching_e'], percentage_format)
                sheet.write(index + 1, 2, faculty['he_r_linkage_e'], percentage_format)
                sheet.write(index + 2, 2, faculty['he_r_management_e'], percentage_format)
                sheet.write(index + 3, 2,faculty['he_r_investigation_e'], values_format)
                sheet.write(index, 3, faculty['he_r_teaching'], percentage_format)
                sheet.write(index + 1, 3, faculty['he_r_linkage'], percentage_format)
                sheet.write(index + 2, 3, faculty['he_r_management'], percentage_format)
                sheet.write(index + 3, 3, faculty['he_r_investigation'], percentage_format)

                sheet.write(index, 4, faculty['ae_r_teaching_e'], percentage_format)
                sheet.write(index + 1, 4, faculty['ae_r_linkage_e'], percentage_format)
                sheet.write(index + 2, 4, faculty['ae_r_management_e'], percentage_format)
                sheet.write(index + 3, 4, faculty['ae_r_investigation_e'], percentage_format)
                sheet.write(index, 5, faculty['ae_r_teaching'], percentage_format)
                sheet.write(index + 1, 5, faculty['ae_r_linkage'], percentage_format)
                sheet.write(index + 2, 5, faculty['ae_r_management'], percentage_format)
                sheet.write(index + 3, 5, faculty['ae_r_investigation'], percentage_format)

                sheet.write(index, 6, faculty['cd_r_teaching_e'], percentage_format)
                sheet.write(index + 1, 6, faculty['cd_r_linkage_e'], percentage_format)
                sheet.write(index + 2, 6, faculty['cd_r_management_e'], percentage_format)
                sheet.write(index + 3, 6, faculty['cd_r_investigation_e'], percentage_format)
                sheet.write(index, 7, faculty['cd_r_teaching'], percentage_format)
                sheet.write(index + 1, 7, faculty['cd_r_linkage'], percentage_format)
                sheet.write(index + 2, 7, faculty['cd_r_management'], percentage_format)
                sheet.write(index + 3, 7, faculty['cd_r_investigation'], percentage_format)

                sheet.write(index, 8, faculty['ca_r_teaching_e'], percentage_format)
                sheet.write(index + 1, 8, faculty['ca_r_linkage_e'], percentage_format)
                sheet.write(index + 2, 8, faculty['ca_r_management_e'], percentage_format)
                sheet.write(index + 3, 8, faculty['ca_r_investigation_e'], percentage_format)
                sheet.write(index, 9, faculty['ca_r_teaching'], percentage_format)
                sheet.write(index + 1, 9, faculty['ca_r_linkage'], percentage_format)
                sheet.write(index + 2, 9, faculty['ca_r_management'], percentage_format)
                sheet.write(index + 3, 9, faculty['ca_r_investigation'], percentage_format)

                sheet.write(index, 10, faculty['t_teaching'], values_format)
                sheet.write(index + 1, 10, faculty['t_linkage'], values_format)
                sheet.write(index + 2, 10, faculty['t_management'], values_format)
                sheet.write(index + 3, 10, faculty['t_investigation'], values_format)

                sheet.write(index, 12, "{0:.2f}".format(faculty['total']), values_format)

                index += 4

        faculty_ids = self.env['edu.survey.faculty'].search([('period_id','=',doc.id)])

        sheet = workbook.add_worksheet("RESUMEN")

        sheet.set_column(1, 1, 38)
        sheet.set_column(3, 3, 22)

        sheet.merge_range('A1:A2', "No.", bold_border_format)
        sheet.merge_range('B1:B2', "APELLIDOS Y NOMBRES", bold_border_format)
        sheet.merge_range('C1:C2', "CARRERA", bold_border_format)
        sheet.merge_range('D1:D2', "CRITERIOS", bold_border_format)
        sheet.merge_range('E1:E2', "HETERO-EVALUACIÓN", bold_border_format)
        sheet.merge_range('F1:F2', "AUTO-EVALUACIÓN", bold_border_format)
        sheet.merge_range('G1:G2', "CO-EVALUACIÓN DE DIRECTIVOS", bold_border_format)
        sheet.merge_range('H1:H2', "CO-EVALUACIÓN DE PARES ACADEMICOS", bold_border_format)
        sheet.merge_range('I1:I2', "%EVALUACIÓN", bold_border_format)
        sheet.merge_range('J1:J2', "PROMEDIO", bold_border_format)

        index = 2
        aux = 1
        for counter, faculty in enumerate(faculty_ids, 2):
            sheet.merge_range(index, 1, index + 3, 1,
                              faculty.faculty_id.full_name, values_format)
            sheet.write(index, 3, "DOCENCIA", values_format)
            sheet.write(index + 1, 3, "VINCULACIÓN / PPP", values_format)
            sheet.write(index + 2, 3, "GESTIÓN ACADÉMICA", values_format)
            sheet.write(index + 3, 3, "INVESTIGACIÓN/TITULACIÓN", values_format)
            if faculty.faculty_id.career_id.name != '0':
                sheet.merge_range(index, 2, index + 3, 2,
                                  faculty.faculty_id.career_id.name, values_format)
            sheet.merge_range(index, 0, index + 3, 0, aux, values_format)

            sheet.write(index, 4, faculty['he_r_teaching'], percentage_format)
            sheet.write(index + 1, 4, faculty['he_r_linkage'], percentage_format)
            sheet.write(index + 2, 4, faculty['he_r_management'], percentage_format)
            sheet.write(index + 3, 4, faculty['he_r_investigation'], percentage_format)

            sheet.write(index, 5, faculty['ae_r_teaching'], percentage_format)
            sheet.write(index + 1, 5, faculty['ae_r_linkage'], percentage_format)
            sheet.write(index + 2, 5, faculty['ae_r_management'], percentage_format)
            sheet.write(index + 3, 5, faculty['ae_r_investigation'], percentage_format)

            sheet.write(index, 6, faculty['cd_r_teaching'], percentage_format)
            sheet.write(index + 1, 6, faculty['cd_r_linkage'], percentage_format)
            sheet.write(index + 2, 6, faculty['cd_r_management'], percentage_format)
            sheet.write(index + 3, 6, faculty['cd_r_investigation'], percentage_format)

            sheet.write(index, 7, faculty['ca_r_teaching'], percentage_format)
            sheet.write(index + 1, 7, faculty['ca_r_linkage'], percentage_format)
            sheet.write(index + 2, 7, faculty['ca_r_management'], percentage_format)
            sheet.write(index + 3, 7, faculty['ca_r_investigation'], percentage_format)

            sheet.write(index, 8, str("{0:.2f}".format(faculty['t_teaching'])), values_format)
            sheet.write(index + 1, 8, str("{0:.2f}".format(faculty['t_linkage'])), values_format)
            sheet.write(index + 2, 8, str("{0:.2f}".format(faculty['t_management'])), values_format)
            sheet.write(index + 3, 8, str("{0:.2f}".format(faculty['t_investigation'])), values_format)

            sheet.write(index, 9, str("{0:.2f}".format(faculty['t_teaching'])), values_format)
            sheet.write(index + 1, 9, str("{0:.2f}".format(faculty['t_linkage'])), values_format)
            sheet.write(index + 2, 9, str("{0:.2f}".format(faculty['t_management'])), values_format)
            sheet.write(index + 3, 9, str("{0:.2f}".format(faculty['t_investigation'])), values_format)

            aux += 1
            index += 4

        sheet = workbook.add_worksheet("INFORME")

        sheet.merge_range('A1:A2', "No.", bold_border_format)
        sheet.merge_range('B1:B2', "APELLIDOS Y NOMBRES", bold_border_format)
        sheet.merge_range('C1:C2', "PROMEDIO", bold_border_format)

        index = 2
        aux = 1
        for counter, faculty in enumerate(faculty_ids, 2):
            sheet.write(index, 1,
                        faculty.faculty_id.full_name, values_format)
            sheet.write(index, 0, aux, values_format)
            sheet.write(index, 2, str("{0:.2f}".format(faculty['total'])), values_format)
            aux += 1
            index += 1

            sheet.merge_range('E1:F1', "PROMEDIO DE", values_format)
            sheet.merge_range('E2:F2', "Entre 100% y 90%", values_format)
            sheet.merge_range('E3:F3', "Entre 89% y 80%", values_format)
            sheet.merge_range('E4:F4', "Entre 79% y 70%", values_format)
            sheet.merge_range('E5:F5', "69% ó menos", values_format)
            sheet.merge_range('G1:H1', "SIGNIFICADO", values_format)
            sheet.merge_range('G2:H2', "Altamente aceptable", values_format)
            sheet.merge_range('G3:H3', "Muy aceptable", values_format)
            sheet.merge_range('G4:H4', "Aceptable", values_format)
            sheet.merge_range('G5:H5', "Poco aceptable", values_format)

            sheet.merge_range('E8:F8', "CATEGORIAS DE RESULTADOS", values_format)
            sheet.merge_range('E9:F9', "Poco aceptable", values_format)
            sheet.merge_range('E10:F10', "Aceptable", values_format)
            sheet.merge_range('E11:F11', "Muy aceptable", values_format)
            sheet.merge_range('E12:F12', "Altamente aceptable", values_format)
            sheet.merge_range('E13:F13', "Total", values_format)
            sheet.merge_range('G8:H8', "DOCENTES", values_format)
            sheet.merge_range('I8:J8', "PORCENTAJE", values_format)

        var1 = 0
        var2 = 0
        var3 = 0
        var4 = 0
        for faculty in faculty_ids:
            # poco
            if 0 <= faculty['total'] <= 69:
                var1 += 1
            # aceptable
            if 70 <= faculty['total'] <= 79:
                var2 += 1
            # muy aceptable
            if 80 <= faculty['total'] <= 89:
                var3 += 1
            # altamente aceptable
            if 90 <= faculty['total'] <= 100:
                var4 += 1

            sheet.merge_range('G9:H9', var1, values_format)
            sheet.merge_range('G10:H10', var2, values_format)
            sheet.merge_range('G11:H11', var3, values_format)
            sheet.merge_range('G12:H12', var4, values_format)

            # total
            var_total_d = var1 + var2 + var3 + var4

            if var1 != 0:
                sheet.merge_range('I9:J9', var1 / var_total_d, percentage_format)
            else:
                sheet.merge_range('I9:J9', var1, percentage_format)
            if var2 != 0:
                sheet.merge_range('I10:J10', var2 / var_total_d, percentage_format)
            else:
                sheet.merge_range('I10:J10', var2, percentage_format)
            if var3 != 0:
                sheet.merge_range('I11:J11', var3 / var_total_d, percentage_format)
            else:
                sheet.merge_range('I11:J11', var3, percentage_format)
            if var4 != 0:
                sheet.merge_range('I12:J12', var4 / var_total_d, percentage_format)
            else:
                sheet.merge_range('I12:J12', var4, percentage_format)

            var_total_p = (var1 / var_total_d) + (var2 / var_total_d) + (var3 / var_total_d) + (var4 / var_total_d)

            sheet.merge_range('G13:H13', var_total_d, values_format)
            sheet.merge_range('I13:J13', var_total_p, percentage_format)
