from odoo import models, api, fields
from odoo import tools

import icu


class ReportSurveysPendingXls(models.AbstractModel):
    _name = 'report.educational_survey.report_surveys_pending_xls'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, obj):
        sheet = workbook.add_worksheet()

        format21 = workbook.add_format({'font_size': 12, 'align': 'center', 'right': True, 'left': True,
                                        'bottom': True, 'top': True, 'bold': True, 'text_wrap': True, })

        format_header = workbook.add_format({'font_size': 12, 'align': 'center', 'right': True, 'left': True,
                                             'bottom': True, 'top': True, 'bold': True, 'text_wrap': True})

        format2_cod = workbook.add_format({'font_size': 12, 'align': 'left', 'right': True, 'left': True,
                                           'bottom': True, 'top': True, 'text_wrap': True, })

        format_title2 = workbook.add_format({'font_size': 12, 'bottom': False, 'right': False, 'left': False,
                                             'top': False, 'align': 'center', 'bold': True})

        format_header.set_rotation(90)
        format_header.set_shrink()

        if obj.type == '2' and obj.period_id.id:

            domain = [('state', '=', 'draft'), ('survey_id.type', '=', 'H'), ('period_id.id', '=', obj.period_id.id)]
            if obj.profile:
                domain += [('survey_id.profile', '=', obj.profile)]

                lista_e = self.env['edu.survey.eval'].read_group(domain,
                                                                 [], ['user_id'])
            else:
                lista_e = self.env['edu.survey.eval'].read_group(domain,
                                                                 [], ['user_id'])
            user_ids = []

            for item in lista_e:
                if item['user_id']:
                    user_ids.append(item['user_id'][0])
            eval_student = self.env['edu.student'].search([('user_id', 'in', user_ids)])

            sheet.set_column('B:B', 40)
            sheet.set_column('C:C', 20)
            sheet.set_column('D:D', 30)
            sheet.set_column('E:E', 10)
            sheet.set_column('F:F', 40)

            sheet.merge_range('B4:F4', u'%s' % 'Evaluaciones Pendientes', format_title2)
            sheet.merge_range('C5:E5', u'%s' % 'Estudiantes ', format_title2)
            sheet.write(7, 1, u'%s' % 'USUARIO', format21)
            sheet.write(7, 2, u'%s' % 'CEDULA', format21)
            sheet.write(7, 3, u'%s' % 'CORREO', format21)
            sheet.write(7, 4, u'%s' % 'PARALELO', format21)
            sheet.write(7, 5, u'%s' % 'CARRERA', format21)
            index = 8
            for i in eval_student:
                sheet.write(index, 1, i['user_id'].name, format2_cod)
                sheet.write(index, 2, str(i.ced_ruc),
                            format2_cod)
                sheet.write(index, 3, str(i.email),
                            format2_cod)
                subject_parallel_id = i.subject_parallel_ids.filtered(lambda x:x.period_id.id == obj.period_id.id)
                if subject_parallel_id:
                    sheet.write(index, 4, subject_parallel_id[0].parallel_id.name,
                                format2_cod)
                sheet.write(index, 5, i['career_id'].name, format2_cod)
                index += 1

        if obj.type == '1' and obj.eval and obj.period_id.id and not obj.profile:
            lista_t = self.env['edu.survey.eval'].read_group(
                [('state', '=', 'draft'), ('survey_id.type', '=', obj.eval), ('period_id.id', '=', obj.period_id.id)],
                [], ['user_id'])
            user_ids_t = []
            for item in lista_t:
                if item['user_id']:
                    user_ids_t.append(item['user_id'][0])

            eval_teacher = self.env['edu.faculty'].search([('user_id', 'in', user_ids_t)])

            sheet.merge_range('D4:L4', u'%s' % 'Evaluaciones Pendientes (AUTOEVALUACIÓN)', format_title2)
            sheet.merge_range('D5:L5', u'%s' % 'Profesores ', format_title2)
            sheet.merge_range('B8:F8', u'%s' % 'USUARIO', format21)
            sheet.merge_range('G8:O8', u'%s' % 'CARRERA', format21)
            index = 8
            for i in eval_teacher:
                sheet.merge_range(index, 1, index + 0, 5, i['user_id'].name, format2_cod)
                sheet.merge_range(index, 14, index + 0, 6, i['career_id'].name, format2_cod)
                index += 1

        if obj.type == '1' and obj.eval and obj.period_id.id and obj.profile:
            lista_t = self.env['edu.survey.eval'].read_group(
                [('state', '=', 'draft'), ('survey_id.type', '=', obj.eval), ('survey_id.profile', '=', obj.profile),
                 ('period_id.id', '=', obj.period_id.id)],
                [], ['user_id'])
            user_ids_t = []
            for item in lista_t:
                if item['user_id']:
                    user_ids_t.append(item['user_id'][0])

            eval_teacher = self.env['edu.faculty'].search([('user_id', 'in', user_ids_t)])

            sheet.merge_range('D4:L4', u'%s' % 'Evaluaciones Pendientes (AUTOEVALUACIÓN)', format_title2)
            sheet.merge_range('D5:L5', u'%s' % 'Profesores ', format_title2)
            sheet.merge_range('B8:F8', u'%s' % 'USUARIO', format21)
            sheet.merge_range('G8:O8', u'%s' % 'CARRERA', format21)
            index = 8
            for i in eval_teacher:
                sheet.merge_range(index, 1, index + 0, 5, i['user_id'].name, format2_cod)
                sheet.merge_range(index, 14, index + 0, 6, i['career_id'].name, format2_cod)
                index += 1

        if obj.type == '1' and not obj.eval and obj.period_id.id and obj.profile:
            lista_t = self.env['edu.survey.eval'].read_group(
                [('state', '=', 'draft'), ('survey_id.type', '=', obj.eval), ('survey_id.profile', '=', obj.profile),
                 ('period_id.id', '=', obj.period_id.id)],
                [], ['user_id'])
            user_ids_t = []
            for item in lista_t:
                if item['user_id']:
                    user_ids_t.append(item['user_id'][0])

            eval_teacher = self.env['edu.faculty'].search([('user_id', 'in', user_ids_t)])

            sheet.merge_range('D4:L4', u'%s' % 'Evaluaciones Pendientes (AUTOEVALUACIÓN)', format_title2)
            sheet.merge_range('D5:L5', u'%s' % 'Profesores ', format_title2)
            sheet.merge_range('B8:F8', u'%s' % 'USUARIO', format21)
            sheet.merge_range('G8:O8', u'%s' % 'CARRERA', format21)
            index = 8
            for i in eval_teacher:
                sheet.merge_range(index, 1, index + 0, 5, i['user_id'].name, format2_cod)
                sheet.merge_range(index, 14, index + 0, 6, i['career_id'].name, format2_cod)
                index += 1

        if obj.type == '1' and obj.period_id and not obj.eval and not obj.profile:

            lista_e = self.env['edu.survey.eval'].read_group(
                [('state', '=', 'draft'), ('survey_id.type', '!=', 'H'),
                 ('period_id.id', '=', obj.period_id.id)],
                [], ['user_id'])
            user_ids = []
            for item in lista_e:
                if item['user_id']:
                    user_ids.append(item['user_id'][0])
            eval_student = self.env['edu.faculty'].search([('user_id', 'in', user_ids)])

            sheet.merge_range('E4:M4', u'%s' % 'Evaluaciones Pendientes', format_title2)
            sheet.merge_range('E5:M5', u'%s' % 'Profesores ', format_title2)
            sheet.merge_range('B8:F8', u'%s' % 'USUARIO', format21)
            sheet.merge_range('G8:H8', u'%s' % 'PARALELO', format21)
            sheet.merge_range('I8:P8', u'%s' % 'CARRERA', format21)
            index = 8
            for i in eval_student:
                sheet.merge_range(index, 1, index + 0, 5, i['user_id'].name, format2_cod)
                subject_parallel_id = i.subject_parallel_ids.filtered(lambda x: x.period_id.id == obj.period_id.id)
                if subject_parallel_id:
                    sheet.merge_range(index, 7, index + 0, 6, subject_parallel_id[0].parallel_id.name,
                                      format2_cod)
                sheet.merge_range(index, 15, index + 0, 8, i['career_id'].name, format2_cod)
                index += 1


class ReportSurveysPendingStudent(models.Model):
    _name = 'report.survey.pending.student'
    _description = 'Reporte de Evaluaciones Pendientes Estudiantes'
    _auto = False

    student_id = fields.Many2one('edu.student', u'Estudiante', readonly=True, help=u"", )
    ced = fields.Char(related='student_id.ced_ruc', store=True)

    @api.model
    def _select(self):
        SQL = """SELECT DISTINCT student.id as id,
                    student.id AS student_id,
                    student.ced_ruc as ced

        """
        return SQL

    @api.model
    def _from(self):
        SQL = """FROM edu_survey_eval as survey_eval
                join edu_survey as survey on survey.id = survey_eval.survey_id
                join edu_student as student on student.id = survey_eval.student_id
        """
        return SQL

    @api.model
    def _where(self):
        SQL = """
                WHERE survey_eval.state = 'draft' AND survey.type = 'H'
        """
        return SQL

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, 'report_survey_pending_student')
        self.env.cr.execute("""
        CREATE OR REPLACE view report_survey_pending_student AS (
            %(select)s
            %(from)s
            %(where)s
               )
        """ % {'select': self._select(),
               'from': self._from(),
               'where': self._where(),
               })
