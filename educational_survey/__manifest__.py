# -*- coding: utf-8 -*-
# noinspection PyStatementEffect
{
    'name': "Educational Survey",

    'summary': """
        Sistema de evaluación académica.""",

    'description': """
        Sistema para la evaluación académica dirigida a coordinadores, docentes, estudiantes y personal administrativo.
    """,

    'author': "Ing. Enrique Pérez",
    'website': "http://www.manexware.com",
    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['educational_core'],

    'data': [
        'security/edu_security.xml',
        'security/ir.model.access.csv',
        'views/survey_views.xml',
        'views/survey_eval_views.xml',
        'views/survey_type_views.xml',
        'views/survey_question_views.xml',
        'views/survey_faculty_views.xml',
        'views/period_views.xml',
        'views/certification_views.xml',
        'views/manager_views.xml',
        'views/practices_views.xml',
        'views/researcher_views.xml',
        'views/link_views.xml',
        'views/subject_coe_views.xml',
        'report/reports.xml',
        'report/survey_faculty_template.xml',
        'report/survey_faculty_eval_faculty.xml',
        'report/survey_eval.xml',
        'report/report_surveys_pending.xml',
        'wizard/generate_survey_views.xml',
        'wizard/surveys_pending_views.xml',
        'views/my_survey_faculty_views.xml',
        'data/my_result_survey_faculty.xml',
        'views/menu.xml',
    ],

}
