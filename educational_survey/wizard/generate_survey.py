# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from odoo import models, fields, api


class GenerateSurvey(models.TransientModel):
    _name = 'generate.survey'
    _description = "Generar encuestas masivas manual"

    period_id = fields.Many2one('edu.period', string='Periodo', required=True)

    faculty_id = fields.Many2one('edu.faculty', string="Docente", required=True)
    subject_parallel_id = fields.Many2one('edu.subject.parallel',
                                          string='Materia/Paralelo')
    survey_id = fields.Many2one('edu.survey', string='Plantilla de Encuesta',required=True)

    @api.multi
    def generate(self):
        subject_parallel_ids = self.env['edu.subject.parallel'].search(
            [('parallel_id.period_id', '=', self.period_id.id)])
        SURVEY_EVAL = self.env['edu.survey.eval']
        # Hetero Formacion
        if self.survey_id.type == 'H' and self.survey_id.profile == 'S':
            subject_parallel = self.subject_parallel_id
            for student in subject_parallel.student_ids:
                flag = SURVEY_EVAL.search_count(
                    [('period_id', '=', self.period_id.id),
                     ('user_id', '=',
                      subject_parallel.faculty_id.user_id.id),
                     ('faculty_id', '=',
                      self.faculty_id.id),
                     ('student_id', '=', student.id),
                     ('subject_parallel_id', '=', subject_parallel.id),
                     ('survey_id', '=', self.survey_id.id)])
                if not flag:
                    values = {
                        'period_id': self.period_id.id,
                        'user_id': student.user_id.id,
                        'faculty_id': self.faculty_id.id,
                        'student_id': student.id,
                        'subject_parallel_id': subject_parallel.id,
                        'survey_id': self.survey_id.id
                    }
                    values['question_ids'] = []
                    for question in self.survey_id.question_ids:
                        values['question_ids'].append(
                            (0, 0, {'name': question.name}))
                SURVEY_EVAL.create(values)
