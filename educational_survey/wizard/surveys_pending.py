from odoo import _, models, fields, api


class EduSurveyPending(models.TransientModel):
    _name = 'survey.pending'
    _description = 'Survey Pending'

    TYPE_EVAL = [('A', 'Autoevaluación'),
                 ('D', 'Co-Directivos'),
                 ('P', 'Co-Pares')]

    PROFILE = [('S', 'Formación'),
               ('C', 'Cátedra Reto'),
               ('P', 'Tutor Pedagógico'),
               ('A', 'Tutor de Prácticas'),
               ('T', 'Tutor de Titulación'),
               ('V', 'Vinculación'),
               ('G', 'Gestión Académica'),
               ('I', 'Investigación')]


    type = fields.Selection([('1', 'Docente'), ('2', 'Estudiante')], string='De', required=True)
    period_id = fields.Many2one('edu.period', string='Periodo', required=True)
    eval = fields.Selection(TYPE_EVAL, string='Tipo')
    profile = fields.Selection(PROFILE, string='Perfil Docente')

    @api.multi
    def export_xls(self):
        return self.env.ref('educational_survey.surveys_pending_report_xlsx').report_action(self)

    @api.onchange('type')
    def onchange_type_eval(self):
        for i in self:
            if i.type == '2':
                i.eval = ''

