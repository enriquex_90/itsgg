-- PARA EXTRACCIÓN DE DATOS ALUMNOS CON SUS RESPECTIVOS CURSOS DB ACADEMICO QUE SE ENCUENTREN REGISTRADOS
-- POR CARRERA Y POR PERIODO
SELECT
DISTINCT(al.`AlumnoCedula`) AS cedula,
CONCAT(al.`AlumnoApellidos`,' ',al.`AlumnoNombres`) nombres,
cu.`IdNivel` AS nivel,
pa.`idParalelo` AS paralelo,
cu.`IdCiclo` AS ciclo,
ca.`CodigoCarrera` AS carrera
FROM `alumnos` al
INNER JOIN `registro_matricula` rm ON rm.`AlumnoCedula` = al.`AlumnoCedula`
INNER JOIN `cursos` cu ON cu.`idCurso` = rm.`IdCurso`
INNER JOIN `carreras` ca ON ca.`CodigoCarrera` = cu.`CodigoCarrera`
INNER JOIN `paralelos` pa ON cu.`IdParalelo` = pa.`idParalelo`
WHERE cu.`IdCiclo` = 26
AND rm.`Estado` = 'M'
-- and ca.`CodigoCarrera` in ('INFM', 'DESO')
-- and ca.`CodigoCarrera` IN ('MERC', 'MRKT')
-- and ca.`CodigoCarrera` IN ('DIGR', 'DISE')
-- AND ca.`CodigoCarrera` IN ('IOYA', 'TIOA')
ORDER BY cu.`IdNivel`, pa.`idParalelo`;


-- VERIFICAR ESTUDIANTES QUE NO HAN REALIZADO LAS PRUEBAS DB ACADEMICO
SELECT
DISTINCT(al.`AlumnoCedula`),
al.`AlumnoNombres`, al.`AlumnoApellidos`,
cu.`IdNivel`,
pa.`idParalelo`,
cu.`IdCiclo`,
ca.`CodigoCarrera`
FROM `alumnos` al
INNER JOIN `registro_matricula` rm ON rm.`AlumnoCedula` = al.`AlumnoCedula`
INNER JOIN `cursos` cu ON cu.`idCurso` = rm.`IdCurso`
INNER JOIN `carreras` ca ON ca.`CodigoCarrera` = cu.`CodigoCarrera`
INNER JOIN `paralelos` pa ON cu.`IdParalelo` = pa.`idParalelo`
WHERE cu.`IdCiclo` = 26
AND rm.`Estado` = 'M'
AND ca.`CodigoCarrera` IN ('DISE', 'DIGR')
AND al.`AlumnoCedula` NOT IN (
	SELECT
	DISTINCT(al.`AlumnoCedula`)
	FROM `alumnos` al
	INNER JOIN `educacional_quiz_eval_line` evl ON al.`AlumnoCedula` = evl.quien_evalua
	INNER JOIN `registro_matricula` rm ON rm.`AlumnoCedula` = al.`AlumnoCedula`
	INNER JOIN `cursos` cu ON cu.`idCurso` = rm.`IdCurso`
	INNER JOIN `carreras` ca ON ca.`CodigoCarrera` = cu.`CodigoCarrera`
	INNER JOIN `paralelos` pa ON cu.`IdParalelo` = pa.`idParalelo`
	WHERE cu.`IdCiclo` = 26
	AND rm.`Estado` = 'M'
	AND ca.`CodigoCarrera` IN ('DISE', 'DIGR')
)
ORDER BY cu.`IdNivel`, pa.`idParalelo`;


-- QUERY PARA EXTRAER A TODOS LOS PROFESORES CON LAS CARRERAS QUE SE ENCUENTREN REGISTRADOS
  SELECT
    DISTINCT(pr.`Cedula`),
    pr.`Nombres`,
    pr.`apellidos`,
    ca.`DescripcionCarrera`,
    ca.`CodigoCarrera`
  FROM `profesores` pr
  INNER JOIN `profesor_materia_curso` pmc ON pmc.`IdProfesor` = pr.`IdProfesor`
  INNER JOIN `cursos` cu ON cu.`idCurso` = pmc.`idCurso`
  INNER JOIN `carreras` ca ON ca.`CodigoCarrera` = cu.`CodigoCarrera`
  WHERE pr.`Estado` = 'A'
  AND pmc.`Estado` = 'A'
  AND cu.`IdCiclo` = 26
  ORDER BY pr.`Nombres`;
  

-- VERIFICAR DOCENTES QUE NO HAN EVALUADO

SELECT
pr.`Cedula`,
CONCAT(pr.`Nombres`,' ',pr.`apellidos`) AS nombres
FROM `profesores` pr
WHERE pr.`Estado` = 'A'
AND pr.`Cedula` NOT IN (
	SELECT
	pr.`Cedula`
	FROM `profesores` pr
	INNER JOIN `educacional_quiz_eval_line` evl ON pr.`Cedula` = evl.quien_evalua
	WHERE pr.`Estado` = 'A';
);