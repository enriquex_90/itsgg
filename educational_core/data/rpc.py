import functools
import xmlrpc.client
import csv

HOST = 'localhost'
PORT = 8069
DB = 'itsgg_pruebas'
USER = 'admin'
PASS = 'Admin123*'
ROOT = 'http://%s:%d/xmlrpc/' % (HOST, PORT)

# 1. Login
uid = xmlrpc.client.ServerProxy(ROOT + 'common').login(DB, USER, PASS)
print("Logged in as %s (uid:%d)" % (USER, uid))

call = functools.partial(
    xmlrpc.client.ServerProxy(ROOT + 'object').execute,
    DB, uid, PASS)

import os

dir_path = os.path.dirname(os.path.realpath(__file__))
# with open(dir_path + '/student2.csv', newline='') as csvfile:
#     spamreader = csv.reader(csvfile, delimiter=',')
#     for row in spamreader:
#         print(row[0])
#         if row[8]:
#             session_id = call('edu.student', 'create', {
#                 'type_ced_ruc': row[1],
#                 'ced_ruc': row[2],
#                 'phone': row[3],
#                 'first_name': row[4],
#                 'middle_name': row[5],
#                 'last_name': row[6],
#                 'mother_name': row[7],
#                 'email': row[8],
#             })
#         else:
#             session_id = call('edu.student', 'create', {
#                 'type_ced_ruc': row[1],
#                 'ced_ruc': row[2],
#                 'phone': row[3],
#                 'first_name': row[4],
#                 'middle_name': row[5],
#                 'last_name': row[6],
#                 'mother_name': row[7],
#             })

# 2. Read the sessions
sessions = call('edu.faculty','search_read', [], ['name','seats'])
for session in sessions:
    call('edu.faculty', 'create_student_user', session['id'])
    print("creado %s " % (session['id']))
# # 3.create a new session
