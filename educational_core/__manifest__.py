# -*- coding: utf-8 -*-
# noinspection PyStatementEffect
{
    'name': "Educational Core",

    'summary': """
        Núcleo principal para el sistema educativo""",

    'description': """
        Módulo principal que contiene todas la personalización de los módulos de usuario y de empresa para adaptarlo
        a las necesidades institucionales.
    """,

    'author': "Ing. Enrique Pérez",
    'website': "http://www.manexware.com",

    'category': 'Education',
    'version': '12.0',

    'depends': ['base', 'l10n_ec_ote', 'auth_oauth', 'report_xlsx'],

    'data': [
        'data/religion_data.xml',
        'data/faculty_data.xml',
        'data/student_data.xml',
        'security/edu_security.xml',
        'security/ir.model.access.csv',
        'views/faculty_views.xml',
        'views/student_views.xml',
        'views/location_views.xml',
        'views/career_views.xml',
        'views/classroom_views.xml',
        'views/parallel_views.xml',
        'views/subject_views.xml',
        'views/subject_parallel_views.xml',
        'views/period_views.xml',
        'views/practices_views.xml',
        'views/certification_views.xml',
        'views/manager_views.xml',
        'views/link_views.xml',
        'views/researcher_views.xml',
        'views/area_views.xml',
        'views/department_views.xml',
        'views/pedagogical_views.xml',
        'wizard/faculty_create_user_wizard_views.xml',
        'wizard/students_create_user_wizard_views.xml',
        'wizard/change_parallel_wizard_views.xml',
        'views/menu.xml',
        'report/reports.xml',
        'report/practices_documents_template.xml',
        'report/certification_documents_template.xml',
        'report/pedagogical_documents_template.xml',
        'report/manager_documents_template.xml',
        'report/link_documents_template.xml',
        'report/researcher_documents_template.xml',

    ],
    # only loaded in demonstration mode
    'demo': [

    ],
}
