# -*- coding: utf-8 -*-

from stdnum.ec import ruc, ci
from dateutil.relativedelta import relativedelta
import logging
from odoo import _, models, fields, api, tools
from odoo.exceptions import ValidationError

_logger = logging.getLogger(__name__)

TYPE_CED_RUC = [('cedula', 'CEDULA'),
                ('ruc', 'RUC'),
                ('pasaporte', 'PASAPORTE')]

STUDY_LEVEL = [('first', 'Primer Nivel'),
               ('second', 'Segundo Nivel'),
               ('third', 'Tercer Nivel'),
               ('fourth', 'Cuarto Nivel'),
               ('fifth', 'Quinto Nivel')]

MARITAL_STATUS = [('single', 'Soltero'),
                  ('married', 'Casado'),
                  ('divorced', 'Divorciado'),
                  ('widower', 'Viudo'),
                  ('free_union', 'Union Libre'),
                  ]

BLOOD_GROUP = [('A+', 'A+'),
               ('B+', 'B+'),
               ('O+', 'O+'),
               ('AB+', 'AB+'),
               ('A-', 'A-'),
               ('B-', 'B-'),
               ('O-', 'O-'),
               ('AB-', 'AB-')]

PHYSICAL_EXONERATION = [('lactation', 'Lactancia'),
                        ('discapacity', 'Discapacidad'), ]

GENDER = [
    ('male', _('Masculino')),
    ('female', _('Femenino')),
]

TYPE_ETHNICITY = [('blanco', 'BLANCO'),
                  ('indigena', 'INDIGENA'),
                  ('mestizo', 'MESTIZO'),
                  ('montubio', 'MONTUBIO'),
                  ('mulato', 'MULATO '),
                  ('afro', 'AFROECUATORIANO/AFRODESCENDIENTE'),
                  ('negro', 'NEGRO '),
                  ('otra', 'OTRA ')
                  ]

TYPE_CULTURE = [('achuar', 'ACHUAR'),
                ('andoa', 'ANDOA'),
                ('awa', 'AWA'),
                ('chachi', 'CHACHI'),
                ('cofan', 'COFAN '),
                ('epera', 'EPERA'),
                ('kichwa', 'KICHWA '),
                ('secoya', 'SECOYA '),
                ('shiwiar', 'SHIWIAR '),
                ('shuar', 'SHUAR '),
                ('siona', 'SIONA '),
                ('tsachila', 'TSACHILA '),
                ('waorani', 'WAORANI '),
                ('zapara', 'ZAPARA '),
                ('otra', 'OTRA ')
                ]


class ResPartner(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'
    _order = 'full_name'

    name = fields.Char('Nombre', compute='_compute_name', store=True)
    full_name = fields.Char(compute='_compute_name', string="Nombre Completo")
    first_name = fields.Char("Primer Nombre")
    middle_name = fields.Char("Segundo Nombre")
    last_name = fields.Char("Primer Apellido")
    mother_name = fields.Char("Segundo Apellido")
    type_ced_ruc = fields.Selection(TYPE_CED_RUC, 'Tipo ID', default='cedula', track_visibility='onchange' )
    ced_ruc = fields.Char(u'Cedula/ RUC',
                          help=u'Identificación o Registro Unico de Contribuyentes', track_visibility='onchange')
    birth_date = fields.Date('Fecha de nacimiento')
    blood_group = fields.Selection(BLOOD_GROUP, 'Tipo de sangre')
    nationality_id = fields.Many2one('res.country', string='Nacionalidad', ondelete='restrict')
    religion = fields.Many2one('edu.religion', 'Religion', ondelete='restrict')
    id_number = fields.Char('ID Card Number', size=64)
    acronym = fields.Char(string='Acronimo')
    foreign = fields.Boolean(string='Extranjero?')
    age = fields.Integer(compute='_compute_age', string=_('Edad'), store=True)
    birthplace_id = fields.Many2one('res.country', string="Lugar de Nacimiento")
    study_level = fields.Selection(STUDY_LEVEL, string=u"Eduación", default='first')
    marital_status = fields.Selection(MARITAL_STATUS, string="Estado Civil")
    gender = fields.Selection(GENDER, u'Genero')
    number_of_sons = fields.Integer(string="Numero de hijos")
    wifes_name = fields.Char(string="Nombres del Conyuge")
    fathers_name = fields.Char(string="Nombre del padre")
    mothers_name = fields.Char(string="Nombre de la madre")
    conadis = fields.Char(string="CONADIS")
    conadis_percent = fields.Float(string="Porcentaje")
    observation_physical = fields.Char(u'Observación')
    emergency_contact = fields.Many2one(
        'res.partner', 'Contacto de emergencia', ondelete='restrict')
    email = fields.Char(copy=False)
    carrer_id = fields.Many2one("edu.career", string="Carrera Ant.")
    career_id = fields.Many2one("edu.career", string="Carrera")
    carrer_academic = fields.Char('Carrera del sistema academico', help='Campo solo usado para la migracion')

    type_i = fields.Selection([('private', 'Privada'), ('public', 'Pública'), ('mixed', 'Mixta')],
                              string="Tipo de Institución")
    ethnicity = fields.Selection(TYPE_ETHNICITY, 'Etnia', default='mestizo')
    culture = fields.Selection(TYPE_CULTURE, 'Cultura')
    phone_home = fields.Char('Telefono Domicilio', size=10)
    reference_street = fields.Char('Referencia (Sector)', size=64)
    first_name_contact = fields.Char("Nombres")
    last_name_contact = fields.Char("Apellidos")
    phone_local = fields.Char('Tel. Domicilio', size=10)
    mobile_phone = fields.Char('Tel Celular', size=10)
    declaration_date = fields.Date('Fecha Declaración')
    banking_institution = fields.Char('Institución Bancaria', size=50)
    account_type = fields.Selection([('ahorro', 'CUENTA AHORROS'), ('corriente', 'CUENTA CORRIENTE')],
                                    string="Tipo de Cuenta")
    account_number = fields.Char('Número de Cuenta', size=16)
    type_relationship = fields.Selection([('conyuge', 'CONYUGE'), ('conviviente', 'CONVIVIENTE')],
                                         string="Tipo de Relación")
    wifes_last_name = fields.Char(string="Apellidos del Conyuge")
    ced_ruc_wifes = fields.Char(u'Cedula o RUC',
                                help=u'Identificación o Registro Unico de Contribuyentes', track_visibility='onchange')
    city_id_d = fields.Many2one('res.state.city', ondelete='restrict', string="Ciudad de Declaración")
    state_id_d = fields.Many2one('res.country.state', 'Provincia')
    title_id = fields.Many2one('res.partner.title', 'Titulo')

    _sql_constraints = [
        ('email_unique', 'UNIQUE(email)', u'Email debe ser único'),
        ('ced_ruc', 'UNIQUE(ced_ruc,type_ced_ruc)', u'Identificación debe ser única'),
        ('check_name', 'CHECK(1=1)', 'Contacto requiere un nombre'),
    ]

    @api.onchange('culture')
    def onchange_type_eval(self):
        for i in self:
            if i.ethnicity != 'indigena':
                i.culture = ''

    @api.multi
    @api.depends('first_name', 'last_name', 'mother_name', 'middle_name')
    def _compute_name(self):
        for record in self:
            if record.first_name and record.last_name and record.mother_name:
                if record.middle_name:
                    display_name = '%s %s %s %s' % (record.last_name, record.mother_name,
                                                    record.first_name, record.middle_name)
                    record.name = display_name.upper()
                    record.full_name = display_name.upper()
                else:
                    if record.mother_name and record.last_name and record.first_name:
                        display_name = '%s %s %s' % (record.last_name,
                                                     record.mother_name, record.first_name)
                    record.name = display_name.upper()
                    record.full_name = display_name.upper()


    @api.multi
    @api.depends('birth_date')
    def _compute_age(self):
        for record in self:
            if record.birth_date:
                birth_date = fields.Datetime.from_string(record.birth_date)
                _logger.warning("Birthday: %s" % birth_date)
                today = fields.Datetime.from_string(fields.Datetime.now())
                # _logger.warning("Hoy: %s" % today)
                if today >= birth_date:
                    age = relativedelta(today, birth_date)
                    # _logger.warning(age)
                    record.age = age.years
                    # _logger.warning("Age: %s" % self.age)

    @api.multi
    @api.constrains('birth_date')
    def _check_birthdate(self):
        for record in self:
            if record.birth_date:
                if record.birth_date > fields.Date.today():
                    raise ValidationError(_(
                        "Birth Date can't be greater than current date!"))

    @api.multi
    @api.constrains('ced_ruc', 'type_ced_ruc')
    def check_vat(self):
        for record in self:
            if record.type_ced_ruc and record.ced_ruc:
                if record.type_ced_ruc == 'cedula' and not ci.is_valid(record.ced_ruc):
                    raise ValidationError('CI [%s] no es valido !' % record.ced_ruc)
                elif record.type_ced_ruc == 'ruc' and not ruc.is_valid(record.ced_ruc):
                    raise ValidationError('RUC [%s] no es valido !' % record.ced_ruc)

    @api.multi
    @api.constrains('ced_ruc_wifes', 'type_ced_ruc')
    def check_ced(self):
        for record in self:
            if record.type_ced_ruc and record.ced_ruc_wifes:
                if record.type_ced_ruc == 'cedula' and not ci.is_valid(record.ced_ruc_wifes):
                    raise ValidationError('CI [%s] no es valido !' % record.ced_ruc_wifes)
                elif record.type_ced_ruc == 'ruc' and not ruc.is_valid(record.ced_ruc_wifes):
                    raise ValidationError('RUC [%s] no es valido !' % record.ced_ruc_wifes)

    def update_user(self):
        self.env['res.users'].update_user(self)
        return True

    # @api.multi
    # @api.depends('name')
    # def name_get(self):
    #     result = []
    #     for record in self:
    #         result.append((record.id, '%s' % record.name))
    #     return result

    @api.depends('is_company', 'parent_id.commercial_partner_id')
    def _compute_commercial_partner(self):
        pass
