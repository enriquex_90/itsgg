# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import json

import requests

from odoo import api, fields, models
from odoo.exceptions import AccessDenied, UserError
from odoo.addons.auth_signup.models.res_users import SignupError

from odoo.addons import base
base.models.res_users.USER_PRIVATE_FIELDS.append('oauth_access_token')


class ResUsers(models.Model):
    _inherit = 'res.users'

    @api.multi
    def create_user(self, records, user_group=None):
        for rec in records:
            if not rec.user_id:
                user_vals = {
                    'name': rec.name,
                    'email':rec.email or (rec.name),
                    'login': rec.email or (rec.name),
                    # 'partner_id': rec.partner_id.id,
                    'first_name' : rec.first_name,
                    'middle_name' : rec.middle_name,
                    'last_name' : rec.last_name,
                    'mother_name' : rec.mother_name,
                    'oauth_provider_id':self.env.ref('auth_oauth.provider_google').id,
                    'oauth_uid': rec.email or (rec.name)

                }
                user_id = self.create(user_vals)
                rec.user_id = user_id
                if user_group:
                    user_group.users = user_group.users + user_id


    def update_user(self, rec):
        user_vals = {
            'name': rec.name,
            'email': rec.email or (rec.name),
            'login': rec.email or (rec.name),
            # 'partner_id': rec.partner_id.id,
            'first_name': rec.first_name,
            'middle_name': rec.middle_name,
            'last_name': rec.last_name,
            'mother_name': rec.mother_name,
            'oauth_provider_id':self.env.ref('auth_oauth.provider_google').id,
            'oauth_uid': rec.email or (rec.name)

        }
        rec.user_id.write(user_vals)

    @api.model
    def _auth_oauth_signin(self, provider, validation, params):
        """ retrieve and sign in the user corresponding to provider and validated access token
            :param provider: oauth provider id (int)
            :param validation: result of validation of access token (dict)
            :param params: oauth parameters (dict)
            :return: user login (str)
            :raise: AccessDenied if signin failed

            This method can be overridden to add alternative signin methods.
        """
        oauth_uid = validation['email']
        try:
            oauth_user = self.search([("oauth_uid", "=", oauth_uid), ('oauth_provider_id', '=', provider)])
            if not oauth_user:
                raise AccessDenied()
            assert len(oauth_user) == 1
            oauth_user.write({'oauth_access_token': params['access_token']})
            return oauth_user.login
        except AccessDenied as access_denied_exception:
            if self.env.context.get('no_user_creation'):
                return None
            state = json.loads(params['state'])
            token = state.get('t')
            values = self._generate_signup_values(provider, validation, params)
            try:
                _, login, _ = self.signup(values, token)
                return login
            except (SignupError, UserError):
                raise access_denied_exception


