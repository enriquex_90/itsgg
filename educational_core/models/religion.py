# -*- coding: utf-8 -*-

from odoo import _, models, fields


class EduReligion(models.Model):
    _name = 'edu.religion'
    _description = "Religiones"

    name = fields.Char(string='Nombre', required=True)
    code = fields.Char(u'Código', size=4, required=True)

    _sql_constraints = [
        ('name_uk', 'unique(name)', u'Nombre debe ser único')
    ]
