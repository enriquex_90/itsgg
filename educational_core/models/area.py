# -*- coding: utf-8 -*-

from odoo import _, models, fields, api


class EduArea(models.Model):
    _name = 'edu.area'
    _description = "Area de conocimiento"

    name = fields.Char(string='Nombre', required=True)

    _sql_constraints = [
        ('name_unique', 'unique(name)', 'Esta área ya existe.')]




