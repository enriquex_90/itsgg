# -*- coding: utf-8 -*-

from odoo import models, fields, api

class EduParallel(models.Model):
    _name = 'edu.parallel'
    _description = "Paralelos"

    def name_get(self):
        data = []
        for rec in self:
            name = str(rec.name) + ' | ' + str(rec.carrer_id.name)
            data.append((rec.id, name))
        return data


    name = fields.Char(string="Nombre de Paralelo", compute='_compute_name', store=True)
    level = fields.Integer(string="Nivel", required=True, size=1)
    literal = fields.Char(string="Literal", required=True, size= 1,  help='Ejemplos A,B,C,D')
    classroom_id = fields.Many2one('edu.classroom', string="Aula")
    period_id = fields.Many2one('edu.period', string="Periodo", required=True)
    active = fields.Boolean(string='Estado', default=True)
    student_ids = fields.Many2many('edu.student', string="Estudiantes")
    carrer_id = fields.Many2one("edu.career", string="Carrera")
    session = fields.Char('Jornada',size=1)
    extracurricular = fields.Selection([('english','Ingles')], string='Extracurricular')
    other_id = fields.Integer()
    academic_career = fields.Char()

    @api.multi
    @api.depends('level', 'literal', 'active')
    def _compute_name(self):
        for record in self:
            if record.level and record.literal:
                record.name = str(record.level)+record.literal