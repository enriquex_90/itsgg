# -*- coding: utf-8 -*-

from odoo import models, fields


class EduCareer(models.Model):
    _name = 'edu.career'
    _description = "Carreras"

    code = fields.Char(string="Código de Carrera", required=True)
    name = fields.Char(string="Nombre de Carrera", required=True)
    level = fields.Integer(string='Niveles', help='Cantidad de niveles de la carrera', required=True)
    title = fields.Char(u"Título que Otorga")
    #FIXME Validar para que solo pueda ser un profesor y no un usuario cualquiera
    coordinator = fields.Many2one("edu.faculty", "Coordinador ant", domain=[('is_coordinator', '=', True)])

    coordinator_id = fields.Many2one("edu.faculty", "Coordinador",
                                  domain=[('is_coordinator', '=', True)])

    area = fields.Selection([('software','Software'),('design','Diseño'),('marketing','Marketing'),('offset','Offset')])
    # hacer de uno a mucho de departamento inverso
    department_id = fields.Many2one('edu.department', string="Departamento")

    description = fields.Text(string="Acerca de la Carrera")

    _sql_constraints = [('code_career_unique', 'unique(code)', 'Código de carrera ya existe.')]