# -*- coding: utf-8 -*-


from odoo import _, models, fields, api


class EduAcademicTitle(models.Model):
    _name = 'edu.academic.title'
    _description = 'Academic Title'

    name = fields.Char('Nombre', size=96, required=True)
    abbreviation = fields.Char('Abreviatura')

    _sql_constraints = [
        ('name_unique', 'unique (name)', u'Nombre debe ser único')
    ]

    @api.one
    def copy(self, default=None):
        default = dict(default or {})
        copied_count = self.search_count(
            [('name', '=like', u"Copy of {}%".format(self.name))])
        if not copied_count:
            new_name = u"Copy of {}".format(self.name)
        else:
            new_name = u"Copy of {} ({})".format(self.name, copied_count)
        default['name'] = new_name
        return super(EduAcademicTitle, self).copy(default)
