# -*- coding: utf-8 -*-

from odoo import models, fields, api

class EduDepartment(models.Model):
    _name = 'edu.department'
    _description = "Departamentos"


    name = fields.Char(string="Nombre del departamento")

# de uno a muchos prueba
    career_id = fields.One2many('edu.career', string='Carreras',
                                 inverse_name="department_id")