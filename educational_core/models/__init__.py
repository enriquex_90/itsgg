# -*- coding: utf-8 -*-

from . import res_partner
from . import faculty
from . import student
from . import academic_title
from . import religion
from . import location
from . import career
from . import classroom
from . import parallel
from . import subject
from . import period
from . import subject_parallel
from . import res_users
from . import practices
from . import certification
from . import manager
from . import link
from . import researcher
from . import area
from . import department
from . import pedagogical