# -*- coding: utf-8 -*-
from datetime import datetime

from odoo import _, models, fields, api


class EduManager(models.Model):
    _name = 'edu.manager'
    _description = "Gestor"

    name = fields.Char(string='Nombre', compute='_compute_name')
    manager_name = fields.Char(string="Nombre de la Gestoría", required=True)
    period_id = fields.Many2one('edu.period', string='Periodo', required=True)
    faculty_id = fields.Many2one('edu.faculty', string='Docente',
                                 required=True)
    student_ids = fields.Many2many('edu.student', string='Estudiantes')
    career_id = fields.Many2one("edu.career", string="Carrera",
                                related='faculty_id.career_id', store=True)

    date = 'Guayaquil, ' + datetime.now().strftime("%d de %B del %Y")

    _sql_constraints = [
        ('name_unique', 'unique (faculty_id,period_id)',
         u'Duplicado - Este docente ya tiene un registro para este periodo')
    ]

    @api.multi
    @api.depends('period_id', 'faculty_id')
    def _compute_name(self):
        for record in self:
            if record.period_id and record.faculty_id:
                record.name = str(
                    record.period_id.name) + " | " + record.faculty_id.name
