# -*- coding: utf-8 -*-

from odoo import _, models, fields

class EduSubject(models.Model):
    _name = 'edu.subject'
    _description = "Materias"

    def name_get(self):
        data = []
        for rec in self:
            # name = str(rec.level) + ' - ' + rec.name
            name = "{} - {} | {}".format(str(rec.level), rec.name, rec.career_id.name)
            data.append((rec.id, name))
        return data

    career_id = fields.Many2one("edu.career", "Carrera", required=True)
    carrer_academic = fields.Char('Carrera del sistema academico', help='Campo solo usado para la migracion')

    #FIXME Debe de existir relación con la cantidad de niveles por carrera, no se debe de asignar arbitrariamente una cantidad.
    level = fields.Integer(string="Nivel", required=True, size=1)

    code = fields.Char(string="Código de Materia", required=True)
    name = fields.Char(string="Nombre de Materia", required=True)
    hours = fields.Integer(string="Horas de duración")
    credits = fields.Integer(string="Cantidad de creditos")
    description = fields.Text(string="Acerca de la Materia")
    integrating = fields.Boolean(string="Cátedra Reto")
    active = fields.Boolean(string="Estado", default=True)
    area = fields.Many2one('edu.area', string="Área de conocimiento")

    _sql_constraints = [('code_unique', 'unique(code)', 'Código de materia ya existe.')]