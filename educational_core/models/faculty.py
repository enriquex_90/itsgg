# -*- coding: utf-8 -*-

import logging

from odoo import _, models, fields, api
from odoo.osv.expression import get_unaccent_wrapper
from odoo.exceptions import UserError, ValidationError
from stdnum.ec import ruc, ci

_logger = logging.getLogger(__name__)

TYPE_CED_RUC = [('cedula', 'CEDULA'),
                ('pasaporte', 'PASAPORTE')]


class EduFaculty(models.Model):
    _name = 'edu.faculty'
    _description = "Docentes"
    _inherit = 'res.partner'
    # _inherits = {'res.partner': 'partner_id'}

    # partner_id = fields.Many2one(
    #     'res.partner', 'Asociado', required=True, ondelete="cascade")
    # category = fields.Many2one('sie.category', u'Categoría', ondelete='restrict')
    type_ced_ruc = fields.Selection(required=True)
    ced_ruc = fields.Char(required=True)
    pan_card = fields.Char('PAN Card', size=64)
    bank_acc_num = fields.Char('Bank Acc Number', size=64)
    acronym = fields.Char('Acronimo', size=10)
    academic_title_id = fields.Many2one('edu.academic.title', string=u'Título')
    career_id = fields.Many2one("edu.career", string="Carrera")

    is_coordinator = fields.Boolean(string="Coordinador")

    is_administrative = fields.Boolean(string="Empleado Administrativo")

    is_teacher = fields.Boolean(string="Docente")

    hr_weekly = fields.Selection([('valor1', '20'), ('valor2', '40')], string="Horas semanales", required=True,
                                 default='valor1')

    is_trainer = fields.Boolean(string="Formación")
    is_tutor_integrating = fields.Boolean(string="Tutor de Cátedra")
    is_tutor_pedagogical = fields.Boolean(string="Tutor pedagógico")
    is_tutor_practices = fields.Boolean(string="Tutor practicas")
    is_tutor_certification = fields.Boolean(string="Tutor Titulacion")
    is_link = fields.Boolean(string=u"Vinculación")
    is_manager = fields.Boolean(string="Gestor")
    is_researcher = fields.Boolean(string="Investigador")

    hr_trainer = fields.Integer(string="Formación horas")
    hr_tutor_integrating = fields.Integer(string="Tutor de Cátedra horas")
    hr_tutor_pedagogical = fields.Integer(string="Tutor pedagógico horas")
    hr_tutor_practices = fields.Integer(string="Tutor practicas horas")
    hr_tutor_certification = fields.Integer(string="Tutor Titulacion horas")
    hr_link = fields.Integer(string=u"Vinculación horas")
    hr_manager = fields.Integer(string="Gestor horas")
    hr_researcher = fields.Integer(string="Investigador horas")

    hr_total = fields.Integer('Total de horas', compute="compute_hr_total", store=True)

    subject_parallel_ids = fields.One2many('edu.subject.parallel', inverse_name='faculty_id', string="Materias")

    faculty_academic_ids = fields.One2many('edu.faculty.academic',
                                           'faculty_id',
                                           ' Experiencias Académicas',
                                           help="Academic experiences")

    faculty_experience_ids = fields.One2many('edu.faculty.experience',
                                             'faculty_id',
                                             ' Experiencias Profesionales',
                                             help="Academic experiences")

    faculty_certification_ids = fields.One2many('edu.faculty.certification',
                                                'faculty_id',
                                                ' Cursos y Certificaciones',
                                                help="Academic experiences")

    faculty_information_ids = fields.One2many('edu.faculty.information',
                                              'faculty_id',
                                              ' Información de los Hijos',
                                              help="Information")
    edit = fields.Boolean('Editar Docentes', compute="_compute_edit")

    @api.model
    def open_faculty(self):
        action_faculty = self.env.ref('educational_core.act_open_edu_faculty_view_my_profile')
        result = action_faculty.read()[0]
        faculty = self.search([('user_id.id', '=', self.env.user.id)])
        if len(faculty) == 1:
            result['res_id'] = faculty.ids[0]
        return result

    #
    # @api.multi
    # def write(self, vals):
    #     for record in self:
    #         if record.user_id:
    #             if vals.get('is_link') or record.is_link:
    #                 group = self.env.ref('educational_core.group_edu_vinculacion')
    #                 group.users = [(4, self.user_id.id)]
    #             if vals.get('is_tutor_practices') or record.is_tutor_practices:
    #                 group = self.env.ref('educational_core.group_edu_practices')
    #                 group.users = [(4, self.user_id.id)]
    #             if vals.get('is_tutor_pedagogical') or record.is_tutor_pedagogical:
    #                 group = self.env.ref('educational_core.group_edu_pedagogico')
    #                 group.users = [(4, self.user_id.id)]
    #             if vals.get('is_tutor_certification') or record.is_tutor_certification:
    #                 group = self.env.ref('educational_core.group_edu_titulacion')
    #                 group.users = [(4, self.user_id.id)]
    #             if vals.get('is_manager') or record.is_manager:
    #                 group = self.env.ref('educational_core.group_edu_gestor')
    #                 group.users = [(4, self.user_id.id)]
    #             if vals.get('is_researcher') or record.is_researcher:
    #                 group = self.env.ref('educational_core.group_edu_investigacion')
    #                 group.users = [(4, self.user_id.id)]
    #     return super(EduFaculty, self).write(vals)

    @api.constrains('is_link', 'is_tutor_practices', 'is_tutor_pedagogical', 'is_tutor_certification',
                    'is_manager', 'is_researcher')
    def validation_group_user(self):
        self.update_faculty_rol()

    def update_faculty_rol(self):
        if self.user_id:
            if self.is_link:
                group = self.env.ref('educational_core.group_edu_vinculacion')
                group.sudo().users = [(4, self.user_id.id)]
            else:
                group = self.env.ref('educational_core.group_edu_vinculacion')
                group.sudo().users = [(3, self.user_id.id)]
            if self.is_tutor_practices:
                group = self.env.ref('educational_core.group_edu_practices')
                group.sudo().users = [(4, self.user_id.id)]
            else:
                group = self.env.ref('educational_core.group_edu_practices')
                group.sudo().users = [(3, self.user_id.id)]
            if self.is_tutor_pedagogical:
                group = self.env.ref('educational_core.group_edu_pedagogico')
                group.sudo().users = [(4, self.user_id.id)]
            else:
                group = self.env.ref('educational_core.group_edu_pedagogico')
                group.sudo().users = [(3, self.user_id.id)]
            if self.is_tutor_certification:
                group = self.env.ref('educational_core.group_edu_titulacion')
                group.sudo().users = [(4, self.user_id.id)]
            else:
                group = self.env.ref('educational_core.group_edu_titulacion')
                group.sudo().users = [(3, self.user_id.id)]
            if self.is_manager:
                group = self.env.ref('educational_core.group_edu_gestor')
                group.sudo().users = [(4, self.user_id.id)]
            else:
                group = self.env.ref('educational_core.group_edu_gestor')
                group.sudo().users = [(3, self.user_id.id)]
            if self.is_researcher:
                group = self.env.ref('educational_core.group_edu_investigacion')
                group.sudo().users = [(4, self.user_id.id)]
            else:
                group = self.env.ref('educational_core.group_edu_investigacion')
                group.sudo().users = [(3, self.user_id.id)]

    @api.onchange('is_trainer', 'is_tutor_integrating', 'is_tutor_pedagogical', 'is_tutor_practices',
                  'is_tutor_certification', 'is_link', 'is_manager', 'is_researcher')
    def onchange_validation_checks(self):
        if not self.is_trainer:
            self.hr_trainer = 0
        if not self.is_tutor_integrating:
            self.hr_tutor_integrating = 0
        if not self.is_tutor_pedagogical:
            self.hr_tutor_pedagogical = 0
        if not self.is_tutor_practices:
            self.hr_tutor_practices = 0
        if not self.is_tutor_certification:
            self.hr_tutor_certification = 0
        if not self.is_link:
            self.hr_link = 0
        if not self.is_manager:
            self.hr_manager = 0
        if not self.is_researcher:
            self.hr_researcher = 0

    @api.constrains('hr_total', 'hr_weekly')
    def validation_select_hour(self):
        for record in self:
            if record.hr_weekly in ["valor1", "20"]:
                if record.hr_total != 20:
                    pass
                    raise ValidationError("Valores no concuerdan con las 20 horas semanales")
            if record.hr_weekly in ["valor2", "40"]:
                if record.hr_total != 40:
                    pass
                    raise ValidationError("Valores no concuerdan con las 40 horas semanales")

    @api.constrains('is_trainer', 'is_tutor_integrating', 'is_tutor_pedagogical', 'is_tutor_practices',
                    'is_tutor_certification', 'is_link', 'is_manager', 'is_researcher', 'hr_trainer',
                    'hr_tutor_integrating', 'hr_tutor_practices', 'hr_tutor_certification',
                    'hr_tutor_pedagogical', 'hr_link', 'hr_manager', 'hr_researcher')
    def validation_checks(self):
        for record in self:
            if not record.is_trainer and record.hr_trainer != 0:
                raise ValidationError("Revisar perfil tutor formación")
            if not record.is_tutor_integrating and record.hr_tutor_integrating != 0:
                raise ValidationError("Revisar perfil tutor cátedra")
            if not record.is_tutor_pedagogical and record.hr_tutor_pedagogical != 0:
                raise ValidationError("Revisar perfil tutor pedaggico")
            if not record.is_tutor_practices and record.hr_tutor_practices != 0:
                raise ValidationError("Revisar perfil tutor de prácticas")
            if not record.is_tutor_certification and record.hr_tutor_certification != 0:
                raise ValidationError("Revisar perfil tutor de titulación")
            if not record.is_link and record.hr_link != 0:
                raise ValidationError("Revisar perfitl tutor vinculación")
            if not record.is_manager and record.hr_manager != 0:
                raise ValidationError("Revisar perfil de gestor")
            if not record.is_researcher and record.hr_researcher != 0:
                raise ValidationError("Revisar perfil de investigación")

    @api.multi
    @api.depends('hr_trainer', 'hr_tutor_integrating', 'hr_tutor_practices', 'hr_tutor_certification',
                 'hr_tutor_pedagogical', 'hr_link', 'hr_manager', 'hr_researcher')
    def compute_hr_total(self):
        for record in self:
            record.hr_total = record.hr_trainer + record.hr_tutor_integrating + record.hr_tutor_practices + record.hr_tutor_certification + \
                              record.hr_tutor_pedagogical + record.hr_link + record.hr_manager + record.hr_researcher

    @api.model
    def create_student_user(self, id):
        user_group = self.env.ref('educational_core.group_edu_faculty')
        records = self.env['edu.faculty'].browse(id)
        self.env['res.users'].create_user(records, user_group)
        return True

    def update_user(self):
        self.update_faculty_rol()
        self.env['res.users'].update_user(self)
        return True

    def find_career(self, period):
        faculty_ids = self.search([])
        for faculty_id in faculty_ids:
            if faculty_id.subject_parallel_ids:
                if faculty_id.subject_parallel_ids[0].parallel_id.carrer_id and not faculty_id.carrer_id:
                    faculty_id.write({'carrer_id': faculty_id.subject_parallel_ids[0].parallel_id.carrer_id.id})

    # sobreescito para evitar usar el respartner original
    def _name_search(self, name, args=None, operator='ilike', limit=100,
                     name_get_uid=None):
        args = args or []
        records = self._search([('name', 'ilike', name)] + args,
                               limit=limit,
                               access_rights_uid=name_get_uid)
        return self.browse(records).name_get()


# faculty_certification_ids = fields.One2many('edu.faculty.certification',
#                                     'faculty_id',
#                                     'Certifications',
#                                     help="Certifications")
# faculty_professional_ids = fields.One2many('edu.faculty.professional',
#                                  'faculty_id',
#                                  ' Professional Experiences',
#                                  help='Professional Experiences')

    @api.multi
    def _compute_edit(self):
        for record in self:
            period = record.env['edu.period'].search([('current','=',True)],limit=1)
            record.edit = period.edit


class EduFacultyAcademic(models.Model):
    _name = 'edu.faculty.academic'
    _description = "Experiencia Académica docentes"

    name = fields.Char('Nombre', required=True)
    faculty_id = fields.Many2one('edu.faculty',
                                 string='Docente',
                                 ondelete="cascade")
    area = fields.Many2one('edu.area', string="Área de conocimiento")
    start_date = fields.Date('Fecha inicio')
    end_date = fields.Date('Fecha finalización')
    description = fields.Text('Descripción')
    location = fields.Char('Lugar', help="Lugar donde realizo la actividad")
    expire = fields.Boolean('Expiró', help="Marcar en esta casilla para ignorar este registro", default=False)
    nivel = fields.Selection([('bachillerato', 'BACHILLERATO'), ('tercernivel', ' 3er NIVEL'),
                              ('cuartonivel', '4to. NIVEL')],
                             string="Nivel de Instrucción")
    registry = fields.Char('No. Registro Certificado  Senescyt')
    institution = fields.Char('Institución Educativa')
    years_approved = fields.Char('Número de Años Aprovados')
    title = fields.Char('Título Obtenido')
    country_id = fields.Many2one('res.country', ondelete='restrict', string="País")


class EduFacultyExperience(models.Model):
    _name = 'edu.faculty.experience'
    _description = "Experiencia Profesional docentes"

    name = fields.Char('Nombre', required=True)
    faculty_id = fields.Many2one('edu.faculty',
                                 string='Docente',
                                 ondelete="cascade")
    area = fields.Many2one('edu.area', string="Área de conocimiento")
    start_date = fields.Date('Fecha inicio')
    end_date = fields.Date('Fecha finalización')
    description = fields.Text('Descripción')
    location = fields.Char('Lugar', help="Lugar donde realizo la actividad")
    expire = fields.Boolean('Expiró', help="Marcar en esta casilla para ignorar este registro", default=False)
    input_mode = fields.Char('Modalidad de Ingreso', size=65)
    output_mode = fields.Char('Modalidad de Salida', size=65)


class EduFacultyCertification(models.Model):
    _name = 'edu.faculty.certification'
    _description = "Experiencia Cursos y Certificaciones docentes"

    name = fields.Char('Nombre', required=True)
    faculty_id = fields.Many2one('edu.faculty',
                                 string='Docente',
                                 ondelete="cascade")
    area = fields.Many2one('edu.area', string="Área de conocimiento")
    start_date = fields.Date('Fecha inicio')
    end_date = fields.Date('Fecha finalización')
    description = fields.Text('Descripción')
    location = fields.Char('Lugar', help="Lugar donde realizo la actividad")
    expire = fields.Boolean('Expiró', help="Marcar en esta casilla para ignorar este registro", default=False)
    company = fields.Char('Empresa')
    time = fields.Char('Duración (Horas)')
    type = fields.Selection([('aprobacion', 'Aprobación'), ('asistencia', 'Asistencia')],
                            string="Tipo de Certificado")
    certified_by = fields.Char('Certicado por')
    country_id = fields.Many2one('res.country', ondelete='restrict', string="País")


class EduFacultyInformationSons(models.Model):
    _name = 'edu.faculty.information'
    _description = "Información de los Hijos"

    name = fields.Char('Nombres')
    faculty_id = fields.Many2one('edu.faculty',
                                 string='Docente',
                                 ondelete="cascade")
    last_name = fields.Char('Apellidos')
    birth_date = fields.Date('Fecha de Nacimiento')
    ced = fields.Char(u'Cedula', help=u'Identificación o Registro Unico de Contribuyentes', track_visibility='onchange')
    type_ced_ruc = fields.Selection(TYPE_CED_RUC, 'Tipo de Documento', default='cedula', track_visibility='onchange')

    nivel = fields.Selection(
        [('escolar', 'ESCOLAR'), ('bachillerato', 'BACHILLERATO'), ('tercernivel', ' 3er NIVEL'),
         ('cuartonivel', '4to. NIVEL')],
        string="Nivel de Instrucción")

    @api.multi
    @api.constrains('ced', 'type_ced_ruc')
    def check_ced(self):
        for record in self:
            if record.type_ced_ruc and record.ced:
                if record.type_ced_ruc == 'cedula' and not ci.is_valid(record.ced):
                    raise ValidationError('CI [%s] no es valido !' % record.ced)
                elif record.type_ced_ruc == 'ruc' and not ruc.is_valid(record.ced):
                    raise ValidationError('RUC [%s] no es valido !' % record.ced)
