# -*- coding: utf-8 -*-

from dateutil.relativedelta import relativedelta
import logging

from odoo import _, models, fields, api

_logger = logging.getLogger(__name__)


class EduStudent(models.Model):
    _name = 'edu.student'
    _description = 'Estudiantes'
    _inherit = 'res.partner'

    TYPE = [('A', 'Activo'),
            ('E', 'Egresado'),
            ('G', 'Graduado'),
            ('R', 'Retirado')]

    library_card = fields.Char('Library Card', size=64)
    subject_parallel_ids = fields.Many2many('edu.subject.parallel', string="Materias")
    state = fields.Selection(TYPE, string='Estado', default='A', required=True)
    type_ced_ruc = fields.Selection(required=True)
    ced_ruc = fields.Char(required=True)

    # user_ids = fields.Char()

    @api.model
    def open_student(self):
        action_student = self.env.ref('educational_core.act_open_edu_student_view_mi_perfil')
        result = action_student.read()[0]
        student = self.search([('user_id.id', '=', self.env.user.id)])
        if len(student) == 1:
            result['res_id'] = student.ids[0]
        return result

    @api.model
    def create_student_user(self, id):
        user_group = self.env.ref('educational_core.group_edu_student')
        records = self.env['edu.student'].browse(id)
        self.env['res.users'].create_user(records, user_group)
        return True

    @api.model
    def create_student_users(self):
        students = self.search([('user_id', '=', False)], limit=5)
        user_group = self.env.ref('educational_core.group_edu_student')
        self.env['res.users'].create_user(students, user_group)
        return True

    @api.model
    def compute_name_cron(self):
        students = self.search([('name', '=', False)])
        for record in students:
            record.user_ids = None
            pass

    def update_user(self):
        self.env['res.users'].update_user(self)
        return True

    def find_career(self):
        student_ids = self.search([])
        for student_id in student_ids:
            subject_parallel_ids = student_id.subject_parallel_ids.filtered(lambda x: x.period_id.current and
                                                                                      x.parallel_id.extracurricular != 'english')
            if subject_parallel_ids:
                if subject_parallel_ids[0].parallel_id.carrer_id:
                    student_id.write({'career_id': subject_parallel_ids[0].parallel_id.carrer_id.id})

    # sobreescito para evitar usar el respartner original
    def _name_search(self, name, args=None, operator='ilike', limit=100,
                     name_get_uid=None):
        args = args or []
        records = self._search([('name', 'ilike', name)] + args,
                               limit=limit,
                               access_rights_uid=name_get_uid)
        return self.browse(records).name_get()
