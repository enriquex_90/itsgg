# -*- coding: utf-8 -*-

from odoo import _, models, fields


class EduPeriod(models.Model):
    _name = 'edu.period'
    _description = "Periodos"
    _order = 'start_date desc'

    def name_get(self):
        data = []
        for rec in self:
            name = str(rec.name) + ' | Desde: ' + str(rec.start_date) + ' | Hasta: ' + str(rec.end_date)
            data.append((rec.id, name))
        return data

    name = fields.Integer(string='Periodo', required=True)
    start_date = fields.Date(string='Fecha de Inicio', required=True)
    end_date = fields.Date(string='Fecha de Fin', required=True)
    active = fields.Boolean(string='Estado', default=True)
    current = fields.Boolean(string="Actual")

    year = fields.Char(string='Año', required=True)
    cycle = fields.Integer(string='Ciclo', required=True)
    edit = fields.Boolean('Editar Docentes', default=False)
