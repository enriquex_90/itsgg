# -*- coding: utf-8 -*-

from odoo import models, fields, api

class EduClassroom(models.Model):
    _name = 'edu.classroom'
    _description = "Aulas"

    name = fields.Char(string="Código de Aula", size=10, required=True)
    description = fields.Char(u'Descripción')
    number_student = fields.Integer(string="Cantidad de estudiantes", required=True)
    location_id = fields.Many2one("edu.location", "Ubicación", required=True)
    active = fields.Boolean(string='Estado', default=True)


