# -*- coding: utf-8 -*-

from odoo import _, models, fields, api

class EduSubjectParallel(models.Model):
    _name = 'edu.subject.parallel'
    _description = "Materias Paralelo"
    _order = 'period_id'

    name = fields.Char('Nombre', compute="_compute_name")
    subject_id = fields.Many2one('edu.subject', string='Materia', required=True)
    parallel_id = fields.Many2one('edu.parallel', string='Paralelo',domain="[('period_id.current','=',True)]", required=True)
    faculty_id = fields.Many2one('edu.faculty', string='Docente')
    period_id = fields.Many2one('edu.period', related='parallel_id.period_id')
    current = fields.Boolean(related='parallel_id.period_id.current')
    student_ids = fields.Many2many('edu.student', string="Estudiantes")
    active = fields.Boolean(string="Activar", default=True)

    _sql_constraints = [
        ('name_uniq', 'unique (subject_id,parallel_id)',
         'Esta materia ya tiene este paralelo resgistrado')
    ]

    @api.multi
    @api.depends('subject_id', 'parallel_id', 'period_id')
    def _compute_name(self):
        for record in self:
            if record.subject_id and record.parallel_id:
                record.name = record.subject_id.name + " " + record.parallel_id.name

    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100,
                     name_get_uid=None):
        args = args or []
        records = self._search([('subject_id', 'ilike', name)] + args,
                                   limit=limit,
                                   access_rights_uid=name_get_uid)
        return self.browse(records).name_get()


    def load_student(self):
        students = []
        for student in self.parallel_id.student_ids:
            students.append(student.id)
        self.student_ids = students
