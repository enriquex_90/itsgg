# -*- coding: utf-8 -*-

from odoo import models, fields, api


class EduLocation(models.Model):
    _name = 'edu.location'
    _description = "Ubicaciones"

    name = fields.Char(string="Nombre de Ubicación", required=True)
    address = fields.Text(string="Dirección", required=True)