# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from odoo import models, fields, api


class WizardEduFaculty(models.TransientModel):
    _name = 'wizard.change.parallel'
    _description = "Create User for selected Faculty(s)"

    def _get_students(self):
        if self.env.context and self.env.context.get('active_ids'):
            return self.env['edu.parallel'].browse(
                self.env.context.get('active_ids')).student_ids.ids
        return []

    parallel_id = fields.Many2one('edu.parallel', string='Nuevo Paralelo',
                                  required=True)
    student_ids = fields.Many2many(
        'edu.student', default=_get_students, string='Estudiantes',
        required=True)


    @api.multi
    def change_students(self):
        self.parallel_id.student_ids += self.student_ids
        self.env['edu.parallel'].browse(
            self.env.context.get('active_ids')).student_ids -= self.student_ids
