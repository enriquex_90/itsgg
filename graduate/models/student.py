# -*- coding: utf-8 -*-

import logging

from odoo import models, fields

_logger = logging.getLogger(__name__)


class EduStudent(models.Model):
    _inherit = 'edu.student'

    graduate_ids = fields.One2many('graduate.apply', string="Ofertas laborales", inverse_name="student_id")
