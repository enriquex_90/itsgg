from datetime import datetime

from odoo import models, fields, api, exceptions


class Graduate(models.Model):
    _name = 'graduate'
    _description = 'Oferta laboral'
    _rec_name = 'title'

    state = fields.Selection(
        [('draft', 'Borrador'), ('published', 'Publicado'), ('finally', 'Finalizado')], default="draft")
    title = fields.Text(string='Titulo del Aviso')
    profile_student = fields.Html(string='Perfil del Postulante')
    salary = fields.Float(string="Sueldo")
    start_date = fields.Date(string='Fecha de Inicio')
    end_date = fields.Date(string='Fecha de Fin')
    description = fields.Html(string='Descripción', size=500)
    area_job = fields.Char(string='Área de Empleo')
    email = fields.Char(string='Correo')
    address = fields.Char(string='Dirección')

    email_user = fields.Char(string='Correo')
    name_contact = fields.Char(string='Nombre de contacto')
    password_user = fields.Char(string='Contraseña')
    ci = fields.Char(string='RUC/C.I')
    address_user = fields.Char(string='Dirección')
    career = fields.Char(string='Carrera que estudia/o')
    phone = fields.Char(string='Teléfono')
    mobile_phone = fields.Char(string='Celular')

    company = fields.Char(string="Nombre de Empresa")
    telephone_contact = fields.Char(string="Teléfono de contacto")

    department_id = fields.Many2one('edu.department', 'Carrera')

    city_id = fields.Many2one('res.state.city', ondelete='restrict', string="Ciudad")
    state_id = fields.Many2one('res.country.state', 'Provincia', required=True)

    student_ids = fields.One2many('graduate.apply', string="Estudiantes", inverse_name='graduate_id')

    student_apply = fields.Boolean('Ya aplico', compute="_compute_student_apply")
    student_visualize = fields.Boolean('Visualización', compute="_compute_student_visualize")

    @api.multi
    def action_to_draft(self):
        for record in self:
            record.state = 'draft'

    @api.multi
    def action_to_published(self):
        for record in self:
            record.state = 'published'

    @api.multi
    def action_to_finally(self):
        for record in self:
            record.state = 'finally'

    @api.constrains('end_date', 'start_date')
    def validation_date(self):
        if self.end_date <= self.start_date:
            raise exceptions.ValidationError(
                "Error!, la finalizacion de la postulacion debe ser despues de la fecha de inicio")

    @api.multi
    def apply_work(self):
        student = self.sudo().env["edu.student"].search([("user_id", "=", self.env.user.id)])
        vals = {'student_id': student[0].id,
                'graduate_id': self.id,
                'date': datetime.now(),
                'type': 'apply'}
        self.sudo().env['graduate.apply'].create(vals)

    @api.multi
    def _compute_student_apply(self):
        for record in self:
            for apply in record.student_ids:
                if apply.student_id.user_id == self.env.user and apply.type == 'apply':
                    record.student_apply = True

    @api.multi
    def _compute_student_visualize(self):
        for record in self:
            for apply in record.student_ids:
                if apply.student_id.user_id == self.env.user and apply.type == 'views':
                    record.student_visualize = True
            if record.student_visualize == False:
                student = self.sudo().env["edu.student"].search([("user_id", "=", self.env.user.id)])
                if student:
                    self.sudo().student_ids = [(4, student.ids[0])]
                    vals = {'student_id': student[0].id,
                            'graduate_id': self.id,
                            'date': datetime.now(),
                            'type': 'views'}
                    self.sudo().env['graduate.apply'].create(vals)
