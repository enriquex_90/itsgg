from odoo import models, fields, api


class GraduateApply(models.Model):
    _name = 'graduate.apply'
    _description = 'Aplicaciones Graduados'

    name = fields.Char('Oferta/Estudiante', compute='_compute_name')
    graduate_id = fields.Many2one('graduate', 'Ofertas Laborales')
    student_id = fields.Many2one('edu.student', 'Estudiante')
    date = fields.Datetime('Fecha')
    type = fields.Selection([('apply', 'Aplicar'), ('views', 'Visualizar')], 'Tipo')

    @api.multi
    def _compute_name(self):
        for record in self:
            record.name = str(record.graduate_id.title) + " / " + str(record.student_id.name)
