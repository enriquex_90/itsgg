from odoo import models


class ReportGraduateApplyXls(models.AbstractModel):
    _name = 'report.graduate.report_graduate_apply_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, obj):
        sheet = workbook.add_worksheet()

        format_title = workbook.add_format({'font_size': 12, 'bottom': False, 'right': False, 'left': False,
                                            'top': False, 'align': 'center', 'bold': True, 'border': 1})
        format_structure = workbook.add_format({'font_size': 8, 'bottom': False, 'right': False, 'left': False,
                                                'top': False, 'align': 'left', 'bold': True, 'border': 1})
        format_content = workbook.add_format({'font_size': 6, 'bottom': False, 'right': False, 'left': False,
                                              'top': False, 'align': 'left', 'bold': False, 'border': 1})
        format_content_number = workbook.add_format({'font_size': 6, 'bottom': False, 'right': False, 'left': False,
                                                     'top': False, 'align': 'right', 'bold': False, 'border': 1})

        domain = [('graduate_id.start_date', '>=', obj.start_date), ('graduate_id.end_date', '<=', obj.end_date),
                  ('graduate_id.department_id.id', '=', obj.department_id.id)]

        graduate_apply = self.env['graduate.apply'].read_group(
            domain, ['id'], ['student_id'])

        sheet.merge_range('A1:J1', u'%s' % 'Reporte de Aplicaciones Laborales', format_title)
        sheet.merge_range('A2:E2', u'%s' % 'Desde:' + str(obj.start_date), format_title)
        sheet.merge_range('F2:J2', u'%s' % 'Hasta:' + str(obj.end_date), format_title)
        sheet.merge_range('A3:B3', u'%s' % 'Nombres', format_structure)
        sheet.merge_range('C3:D3', u'%s' % 'Correo', format_structure)
        sheet.write(2, 4, 'Telefono', format_structure)
        sheet.merge_range('F3:G3', u'%s' % 'Carrera', format_structure)
        sheet.write(2, 7, 'Estado', format_structure)
        sheet.write(2, 8, 'Visitas', format_structure)
        sheet.write(2, 9, 'Aplicaciones', format_structure)

        index = 3
        for apply in graduate_apply:
            student_apply = self.env['edu.student'].browse(apply['student_id'][0])
            if student_apply.career_id.department_id == obj.department_id:
                for student in student_apply:
                    sheet.merge_range(index, 0, index, 1, student.name, format_content)
                    sheet.merge_range(index, 2, index, 3, student.email, format_content)
                    sheet.write(index, 4, student.phone, format_content)
                    sheet.merge_range(index, 5, index, 6, student.carrer_id.name, format_content)
                    if student.state == 'A':
                        sheet.write(index, 7, 'Activo', format_content)
                    if student.state == 'E':
                        sheet.write(index, 7, 'Egresado', format_content)
                    if student.state == 'G':
                        sheet.write(index, 7, 'Graduado', format_content)
                    if student.state == 'R':
                        sheet.write(index, 7, 'Retirado', format_content)

                    num_apply = student.graduate_ids.filtered(
                        lambda
                            x: x.type == 'apply' and x.graduate_id.start_date >= obj.start_date and x.graduate_id.end_date <= obj.end_date)
                    num_views = student.graduate_ids.filtered(
                        lambda
                            y: y.type == 'views' and y.graduate_id.start_date >= obj.start_date and y.graduate_id.end_date <= obj.end_date)
                    sheet.write(index, 8, len(num_apply), format_content_number)
                    sheet.write(index, 9, len(num_views), format_content_number)
                    index += 1
