# -*- encoding: utf-8 -*-

from odoo import models, api, fields


class WizardGraduateApplyReport(models.TransientModel):
    _name = 'wizard.graduate.apply.report'
    _description = 'Reporte Postulaciones'

    start_date = fields.Date('Fecha de Inicio')
    end_date = fields.Date('Fecha de Fin')
    department_id = fields.Many2one('edu.department', 'Carrera')

    @api.multi
    def action_get_report_xls(self):
        return self.env.ref('graduate.graduate_apply_report_xlsx').report_action(self)
