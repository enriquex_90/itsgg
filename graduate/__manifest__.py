# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Graduate',
    'version': '1.1',
    'category': 'Human Resources',
    'sequence': 75,
    'summary': 'Centralize employee information',
    'description': "",
    'website': 'https://www.odoo.com/page/employees',
    'images': [
        'images/graduate_department.jpeg',
        'images/graduate_employee.jpeg',
        'images/graduate_job_position.jpeg',
        'static/src/img/default_image.png',
    ],
    'depends': [
        'educational_core',
    ],
    'data': [
        'security/graduate_security.xml',
        'security/ir.model.access.csv',
        'views/graduate_views.xml',
        'views/student_views.xml',
        'views/graduate_apply_views.xml',
        'views/menu.xml',
        'wizard/graduate_apply_report_wizard.xml',
        'report/reports.xml',
    ],
    'demo': [
        'data/graduate_demo.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'qweb': [],
}
