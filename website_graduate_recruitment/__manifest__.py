# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Oferta Laboral para Graduados',
    'category': 'Website',
    'sequence': 142,
    'version': '1.0',
    'summary': 'Manage your online hiring process',
    'description': "This module allows to publish your available job positions on your website and keep track of application submissions easily. It comes as an add-on of *Recruitment* app.",
    'depends': ['website_partner', 'graduate_recruitment', 'website_mail', 'website_form'],
    'data': [
        'security/ir.model.access.csv',
        'security/website_graduate_recruitment_security.xml',
        'data/config_data.xml',
        'views/website_graduate_recruitment_templates.xml',
        'views/graduate_recruitment_views.xml',
        'views/graduate_job_views.xml',
    ],
    'demo': [
        'data/graduate_job_demo.xml',
    ],
    'installable': True,
    'application': True,
}
