# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
{
    'name': "Contigo Brand Base",
    'version': "12.0.1.1.1",
    'summary': """Contigo Brand""",
    'description': """Contigo Brand""",
    'author': "Manexware",
    'company': "Manexware",
    'maintainer': "Manexware",
    'website': "https://manexware.com/",
    'category': 'Tools',
    "depends": [
        'base',
    ],
    "data": [
    ],
    "qweb": [
    ],
    "images": ['static/description/Banner.png'],
    "application": True,
    "installable": True,
    "auto_install": True,
}
