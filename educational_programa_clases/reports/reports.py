# -*- coding: utf-8 -*-

import logging
from odoo import models, api

_logger = logging.getLogger(__name__)


class PlanAnaliticoReport(models.TransientModel):
    _name = 'report.educational.template_plan_analitico'

    @api.model
    def render_html(self, docids, data=None):

        plan_analitico = self.env['educational.programa_analitico'].browse(docids)

        docargs = {
            'doc_ids': docids,
            'doc_model': 'educational.programa_analitico',
            'docs': plan_analitico,
            'data': data
        }

        #http://localhost:8075/report/pdf/medical.template_receta_report/

        return self.env['report'].render('educational.template_programa_analitico', docargs)


class SyllabusReport(models.TransientModel):
    _name = 'report.educational.template_syllabus'

    @api.model
    def render_html(self, docids, data=None):

        syllabus = self.env['educational.syllabus'].browse(docids)

        docargs = {
            'doc_ids': docids,
            'doc_model': 'educational.syllabus',
            'docs': syllabus,
            'data': data
        }

        #http://localhost:8075/report/pdf/medical.template_receta_report/

        return self.env['report'].render('educational.template_syllabus', docargs)