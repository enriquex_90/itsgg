# -*- coding: utf-8 -*-
{
    'name': "Educational Programas de Clases",

    'summary': """
        Gestión de los programas de clases""",

    'description': """
        Módulo construído para poder administrar los programas de clases que se llevaran a cabo.
    """,

    'author': "Ing. Cristhian Carreño Arce",
    'website': "http://www.webhosting.ml",
    'category': 'Education',
    'version': '1.0',

    'depends': ['base', 'report', 'educational_core'],

    'data': [
        'views/view_plan_analitico.xml',
        'views/view_syllabus.xml',
        'views/view_menuitem.xml',
        'data/data.xml',
        'reports/report_plan_analitico.xml',
        'reports/report_syllabus.xml',
        'reports/report_main_view.xml',
    ],
}
