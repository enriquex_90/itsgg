# -*- coding: utf-8 -*-

from odoo import models, fields, api
import logging

_logger = logging.getLogger(__name__)


class UnidadesTematicas(models.Model):
    _name = 'educational.plan_analitico_unid_tema'

    plan_analitico_id = fields.Many2one('educational.programa_analitico', required=True)

    unidad = fields.Char(string='Unidad', required=True)
    contenido = fields.Char(string='Contenido', required=True)
    titulo = fields.Boolean(string='Es título')
    horas_presenciales = fields.Float(string='H. P.', help='Horas presenciales')
    horas_practicas = fields.Float(string='H. G.P.', help='Horas de prácticas')
    horas_autonomas = fields.Float(string='H. A.', help='Horas de gestión autónoma')

    _order = "unidad"


class Bibliografia(models.Model):
    _name = 'educational.plan_analitico_bibliografia'

    plan_analitico_id = fields.Many2one('educational.programa_analitico', required=True)

    tipo = fields.Selection([('B', 'Básica'), ('C', 'Complementaria'), ('S', 'Sitios Web')], required=True)
    titulo = fields.Char(string='Título', required=True)
    autor = fields.Char(string='Autor')
    edicion = fields.Char(string='Edición')
    anioPublicacion = fields.Date(string='Año Publicación')
    existencias = fields.Integer(string='Exitencias')
    fisicoDigital = fields.Selection([('F', 'Físico'), ('D', 'Digital')], required=True)


class PlanAnalitico (models.Model):
    _name = 'educational.programa_analitico'

    name = fields.Many2one('educational.materias', string='Materia', required=True)
    nivel = fields.Integer(string='Nivel', readonly=True)
    carrera_id = fields.Many2one('educational.carreras', string='Carreras', readonly=True)
    version = fields.Char(string='Version')
    eje_formacion = fields.Selection([('B', 'Básico'), ('P', 'Profesional'), ('T', 'Titulación')], string='Eje de formación')
    prerequsitos = fields.Many2one('educational.materias', string='Pre-requisitos')
    correquisitos = fields.Many2one('educational.materias', string='Co-requisitos')
    area_conocimiento = fields.Selection([('INF', 'Informática')], string='Area de conocimiento')
    horas_presenciales = fields.Integer('Horas presenciales')
    horas_practicas = fields.Integer('Horas de prácticas')
    horas_autonomo = fields.Integer('Horas autónomas')
    aportes_teoricos = fields.Text(string='Aporte teóricos, metodológicos de la asignatura a los problemas de la profesión.')
    objetivos_generales = fields.Text(string='Objetivos Generales')
    objetivos_especificos = fields.Text(string='Objetivos Especificos')
    perfil_egreso = fields.Text(string='Perfil de Egreso de la Carrera')

    prod_acad_esp = fields.Text(string='Productos académicos esperados')
    res_est_pres = fields.Text(string='Resultados y estándares de presentación')
    int_otras_asig = fields.Text(string='Integraciones con otras asignaturas')

    unidades_line = fields.One2many('educational.plan_analitico_unid_tema',
                                    'plan_analitico_id',
                                    string='Unidades',
                                    copy=True
                                    )

    bibliografia_line = fields.One2many('educational.plan_analitico_bibliografia',
                                        'plan_analitico_id',
                                        string='Bibliografías'
                                        )

    elaboracion = fields.Many2one('res.partner', string='Elaborado por')
    revision = fields.Many2one('res.partner', string='Revisado por')
    aprobacion = fields.Many2one('res.partner', string='Aprobado por')

    @api.onchange('name')
    def _defaul_carrera(self):
        self.carrera_id = self.name.carrera.id
        self.nivel = self.name.niveles

        if self.carrera_id and self.nivel and self._origin.id:
            self._cr.execute("UPDATE  educational_programa_analitico SET carrera_id = %s, nivel = %s where id = %s ",
                         (self.name.carrera.id, self.name.niveles, self._origin.id))


class PlanAnaliticoLine(models.Model):
    _name = 'educational.planes_analiticos'

    materia_id = fields.Many2one('educational.materias')

    plan_analitico = fields.Many2one('educational.programa_analitico', string='Plan Analítico', required=True)

    observacion = fields.Text(string='Observación')

    _order = 'create_date DESC'


class Materia(models.Model):
    _inherit = 'educational.materias'

    #materia_id = fields.Many2one('educational.programa_analitico')

    plan_analitico_line = fields.One2many('educational.planes_analiticos',
                                          'materia_id',
                                          string='Plan Analítico'
                                          )


