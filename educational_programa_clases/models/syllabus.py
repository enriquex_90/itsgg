# -*- coding: utf-8 -*-

from odoo import models, fields, api


class TipoInteraccionProfesor(models.Model):
    _name = 'educational.tip_int_prof'

    syllabus_id = fields.Many2one('educational.syllabus')

    name = fields.Char(string='Tipo')
    observacion = fields.Text(string='Observación')


class ActivadesPracticas(models.Model):
    _name = 'educational.tip_act_prac'

    syllabus_id = fields.Many2one('educational.syllabus')

    name = fields.Char(string='Tipo')
    observacion = fields.Text(string='Observación')


class TrabajoAutonomo(models.Model):
    _name = 'educational.tip_trab_aut'

    syllabus_id = fields.Many2one('educational.syllabus')

    name = fields.Char(string='Tipo')
    observacion = fields.Text(string='Observación')


class UnidadesTematicas(models.Model):
    _name = 'educational.syllabus_unid_tema'

    syllabus_id = fields.Many2one('educational.syllabus', required=True)

    fecha_prog = fields.Date(string='Fecha Programada', required=True)
    contenido = fields.Text(string='Contenido', required=True)
    iter_prof_ids = fields.Many2many('educational.tip_int_prof',
                                               column1='id',
                                               column2='iteraccion_profesor_ids',
                                               string='Interacción directa con el profesor'
                                               )
    act_prac_ids = fields.Many2many('educational.tip_act_prac',
                                                 column1='id',
                                                 column2='actividades_practicas_ids',
                                                 string='Actividades prácticas'
                                                 )
    tra_aut_ids = fields.Many2many('educational.tip_trab_aut',
                                                    column1='id',
                                                    column2='trabajo_autonomo_ids',
                                                    string='Trabajo autónomo'
                                                    )
    horas_dedicacion = fields.Integer(string='Horas de dedicación por actividad')
    met_tec_inst = fields.Text(string='Métodos, técnicas e instrumentos diseñados para cada actividad')
    gestion_formativa = fields.Text(string='Gestión formativa')
    gestion_practica = fields.Text(string='Gestión práctica')
    acreditacion_validacion = fields.Text(string='Acreditación y validación')

    _order = "fecha_prog"


class Bibliografia(models.Model):
    _name = 'educational.syllabus_bibliografia'

    syllabus_id = fields.Many2one('educational.syllabus', string='Materia', required=True)

    tipo = fields.Selection([('B', 'Básica'), ('C', 'Complementaria'), ('S', 'Sitios Web')], required=True)
    titulo = fields.Char(string='Título', required=True)
    autor = fields.Char(string='Autor')
    edicion = fields.Char(string='Edición')
    anioPublicacion = fields.Date(string='Año Publicación')
    existencias = fields.Integer(string='Exitencias')
    fisicoDigital = fields.Selection([('F', 'Físico'), ('D', 'Digital')], required=True)


class Syllabus (models.Model):
    _name = 'educational.syllabus'

    name = fields.Many2one('educational.materias', string='Materia', required=True)
    version = fields.Char('Version')
    area_conocimiento = fields.Selection([('INF', 'Informática')], string='Area de conocimiento')
    horas_presenciales = fields.Integer('Horas presenciales')
    horas_practicas = fields.Integer('Horas de prácticas')
    horas_autonomo = fields.Integer('Horas autónomas')

    objetivos = fields.Text(string='Objetivos')
    prod_acad_esp = fields.Text(string='Productos académicos esperados')
    res_est_pres = fields.Text(string='Resultados y estándares de presentación')
    int_otras_asig = fields.Text(string='Integraciones con otras asignaturas')

    unidades_line = fields.One2many('educational.syllabus_unid_tema',
                                    'syllabus_id',
                                    string='Unidades',
                                    copy=True
                                    )

    bibliografia_line = fields.One2many('educational.syllabus_bibliografia',
                                        'syllabus_id',
                                        string='Bibliografías'
                                        )

    elaboracion = fields.Many2one('res.partner', string='Elaborado por')
    revision = fields.Many2one('res.partner', string='Revisado por')
    aprobacion = fields.Many2one('res.partner', string='Aprobado por')


class SyllabusLine(models.Model):
    _name = 'educational.syllabus_line'

    materia_id = fields.Many2one('educational.materias')

    syllabus = fields.Many2one('educational.syllabus', string='Syllabus', required=True)

    observacion = fields.Text(string='Observación')

    _order = 'create_date DESC'


class Materia(models.Model):
    _inherit = 'educational.materias'

    syllabus_line = fields.One2many('educational.syllabus_line',
                                    'materia_id',
                                    string='Syllabus'
                                    )